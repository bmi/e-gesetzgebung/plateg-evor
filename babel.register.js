// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// https://babeljs.io/docs/en/babel-register
// https://github.com/babel/babel/issues/9961
module.export = require('@babel/register')({
  extensions: ['.ts', '.tsx'],
  only: ['./'],
  presets: ['@babel/preset-typescript', '@babel/preset-react'],
  plugins: [
    [
      'babel-plugin-transform-import-ignore',

      {
        patterns: ['.less', '*custom-antd-components*'],
      },
    ],
  ],
});
