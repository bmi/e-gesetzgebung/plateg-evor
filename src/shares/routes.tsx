// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const ROUTES = {
  analyse: 'analyse',
  zielAnalyse: 'zielAnalyse',
  entwicklung: {
    entwicklungIdeen: 'entwicklung/entwicklungIdeen',
    entwicklungAlternativen: 'entwicklung/entwicklungAlternativen',
  },
  bewertung: {
    bewertungAuswirkungenPolitik: 'bewertung/bewertungAuswirkungenPolitik',
    bewertungAuswirkungenNachAlternative: 'bewertung/bewertungAuswirkungenNachAlternative',
    bewertung: 'bewertung/bewertungBewertung',
  },
  konsultation: {
    konsultationVorbereitung: 'konsultation/konsultationVorbereitung',
    konsultationErgebnisse: 'konsultation/konsultationErgebnisse',
  },
  fazit: 'fazit',
  ergebnisdokumentation: 'ergebnisdokumentation',
};
