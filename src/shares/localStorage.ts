// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';

import { EvorData } from '@plateg/rest-api';
import { displayMessage } from '@plateg/theme';

import { loadDraft } from '../components/startseite/controller';
import { DraftCollection } from '../types/DraftCollection';

export function saveDraftContent(values: EvorData, isContinueLater = false): void {
  const draftCollectionString: string = global.window.localStorage.getItem('draftCollection') || '{}';
  const draftCollection: DraftCollection = JSON.parse(draftCollectionString) as DraftCollection;

  const draftID = values.id;
  let currentDraft: EvorData = draftCollection[draftID];
  currentDraft = { ...currentDraft, ...values, lastmodified: new Date().toISOString() };

  draftCollection[draftID] = currentDraft;

  global.window.localStorage.setItem('draftCollection', JSON.stringify(draftCollection));
  if (isContinueLater) {
    displayMessage(i18n.t('evor.continueLater.successMsg'), 'success');
  }
}

export function saveDraft(data: string, routeChangeInitiate: (url: string) => void): void {
  const draft = JSON.parse(data) as EvorData;
  loadDraft(draft);
  routeChangeInitiate(`/evor/${draft.id}`);
}
