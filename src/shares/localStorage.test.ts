// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect, should } from 'chai';
import { v4 as uuidv4 } from 'uuid';

import { createNewRegelungsentwurf, getDraftById } from '../components/startseite/controller';
import { initLocalstorageMock } from '../localstorageMock';
import { saveDraftContent } from './localStorage';
describe('Test Save Regelungsentwurf', () => {
  beforeEach(() => {
    initLocalstorageMock();
  });
  it('Update in Storage', () => {
    const mockId1 = createNewRegelungsentwurf('Draft1');
    let mock1 = getDraftById(mockId1);
    should().equal(mock1?.regelungsalternativen, undefined);
    const teilbereichId1 = uuidv4();
    const teilbereichId2 = uuidv4();
    const teilbereichsoptionId1 = uuidv4();
    const teilbereichsoptionId2 = uuidv4();
    const teilbereichsoptionId3 = uuidv4();

    //add some details
    saveDraftContent({
      id: mockId1,
      regelungsalternativen: {
        teilbereiche: {
          teilbereichList: [
            {
              id: teilbereichId1,
              name: 'Teilbereich 1',
              optionen: [
                {
                  id: teilbereichsoptionId1,
                  name: 'Teilbereichsoption 1',
                },
                {
                  id: teilbereichsoptionId2,
                  name: 'Teilbereichsoption 2',
                },
              ],
            },
            {
              id: teilbereichId2,
              name: 'Teilbereich 2',
              optionen: [
                {
                  id: teilbereichsoptionId3,
                  name: 'Teilbereichsoption 3',
                },
              ],
            },
          ],
        },
        regelungsalternativeList: [
          {
            id: uuidv4(),
            name: 'Regelungsalternative 1',
            teilbereiche: [
              { teilbereichId: teilbereichId1, teilbereichsoptionId: teilbereichsoptionId1 },
              { teilbereichId: teilbereichId1, teilbereichsoptionId: teilbereichsoptionId2 },
            ],
            auswirkungenPolitikbereiche: [],
          },
          {
            id: uuidv4(),
            name: 'Regelungsalternative 2',
            teilbereiche: [{ teilbereichId: teilbereichId2, teilbereichsoptionId: teilbereichsoptionId3 }],
            auswirkungenPolitikbereiche: [],
          },
        ],
      },
    });
    mock1 = getDraftById(mockId1);
    expect(mock1?.regelungsalternativen?.teilbereiche?.teilbereichList).ofSize(2);
    expect(mock1?.regelungsalternativen?.teilbereiche?.teilbereichList[0].optionen).ofSize(2);
    expect(mock1?.regelungsalternativen?.teilbereiche?.teilbereichList[1].optionen).ofSize(1);
    expect(mock1?.regelungsalternativen?.regelungsalternativeList).ofSize(2);
    expect(mock1?.regelungsalternativen?.regelungsalternativeList[0].teilbereiche).ofSize(2);
    expect(mock1?.regelungsalternativen?.regelungsalternativeList[1].teilbereiche).ofSize(1);

    //remove some details
    saveDraftContent({
      id: mockId1,
      regelungsalternativen: { regelungsalternativeList: [] },
    });
    mock1 = getDraftById(mockId1);
    should().equal(mock1?.regelungsalternativen?.teilbereiche, undefined);
    expect(mock1?.regelungsalternativen?.regelungsalternativeList).ofSize(0);
  });
});
