// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './continue-later.less';

import { Button } from 'antd';
import Paragraph from 'antd/lib/typography/Paragraph';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { SaveOutlined } from '@plateg/theme';

interface ContinueLaterComponentProps {
  saveCurrentState: () => void;
  isDisabled: boolean;
}

export function ContinueLaterButton(props: ContinueLaterComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <Button
        id="evor-zwischenspeichern-btn"
        key="zwischenspeichern-button"
        disabled={props.isDisabled}
        onClick={props.saveCurrentState}
      >
        {t('evor.continueLater.btnText')}
      </Button>
    </>
  );
}

export function ContinueLaterComponent(props: ContinueLaterComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <Paragraph className={`continue-later-holder`}>
      {t(`evor.continueLater.title`)}
      &nbsp;&nbsp;&nbsp;
      <Button
        id="evor-zwischenspeichernText-btn"
        type="text"
        className="blue-text-button"
        disabled={props.isDisabled}
        onClick={props.saveCurrentState}
        icon={<SaveOutlined />}
      >
        {t('evor.continueLater.btnText')}
      </Button>
    </Paragraph>
  );
}
