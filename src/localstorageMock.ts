// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

/* eslint-disable @typescript-eslint/ban-ts-comment */
// General function to mock localstorage for unit tests
export function initLocalstorageMock(): void {
  // @ts-ignore
  global.window = {};
  // @ts-ignore
  window.localStorage = global.localStorage;
  interface KeyValueStore {
    [key: string]: string;
  }
  const storage: KeyValueStore = {};
  // @ts-ignore
  global.window.localStorage = {
    setItem(key: string, value: string) {
      storage[key] = value || '';
    },
    getItem(key: string) {
      return key in storage ? storage[key] : null;
    },
    removeItem(key: string) {
      delete storage[key];
    },
    get length() {
      return Object.keys(storage).length;
    },
    key(i: number) {
      const keys = Object.keys(storage);
      return keys[i] || null;
    },
  };
}
