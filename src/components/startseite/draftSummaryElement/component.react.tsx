// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Popover, Typography } from 'antd';
import Text from 'antd/lib/typography/Text';
import React, { RefObject, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { EvorData } from '@plateg/rest-api';
import { displayMessage, ExclamationCircleFilled, getDateTimeString } from '@plateg/theme';
import { DeleteOutlined } from '@plateg/theme/src/components/icons/DeleteOutlined';

interface DraftSummaryElementProps {
  draft: EvorData;
  deleteDraft: (id: string) => void;
}

export function DraftSummaryElement(props: DraftSummaryElementProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const deleteMainBtnRef = useRef<HTMLAnchorElement>(null);
  const cancelBtnRef = useRef<HTMLButtonElement>(null);
  const deleteBtnRef = useRef<HTMLButtonElement>(null);
  const handleDelete = () => {
    props.deleteDraft(props.draft.id);
    setOpened(false);
    displayMessage('Das Vorhaben wurde gelöscht.', 'success');
  };
  const [opened, setOpened] = useState(false);

  useEffect(() => {
    if (opened) {
      setTimeout(() => {
        deleteBtnRef.current?.focus();
      }, 100);
    }
  }, [opened]);

  const popoverTitle = (
    <>
      <span role="img" aria-label="Achtung-Icon" className="anticon anticon-exclamation-circle">
        <ExclamationCircleFilled />
      </span>
      <Text className="ant-popover-message-title">{t('startseite.deleteConfirmation.title')}</Text>
    </>
  );

  const onKeyDownHandler = (e: React.KeyboardEvent<HTMLElement>, btnRef: RefObject<HTMLButtonElement>) => {
    if (e.code === 'Tab' && !e.shiftKey) {
      e.preventDefault();
      btnRef.current?.focus();
    }
  };

  const popoverContent = (
    <div style={{ maxWidth: '300px' }}>
      <p style={{ lineHeight: '20px', fontSize: '16px' }}>{t('startseite.deleteConfirmation.warningText')}</p>
      <div className="ant-popover-buttons">
        <Button
          id={`evor-handleDelete-btn-${props.draft.id}`}
          ref={deleteBtnRef}
          onKeyDown={(e) => onKeyDownHandler(e, cancelBtnRef)}
          onClick={handleDelete}
          type="primary"
        >
          {t('startseite.deleteConfirmation.btnDelete')}
        </Button>
        <Button
          id={`evor-abortDelete-btn-${props.draft.id}`}
          ref={cancelBtnRef}
          onKeyDown={(e) => onKeyDownHandler(e, deleteBtnRef)}
          onClick={() => {
            setOpened(false);
            deleteMainBtnRef.current?.focus();
          }}
          type="default"
        >
          {t('startseite.deleteConfirmation.btnCancel')}
        </Button>
      </div>
    </div>
  );

  return (
    <div className="summary-element">
      <Title level={4} className="summary-element-title" style={{ display: 'inline-block' }}>
        <Link id={`evor-draft-link-${props.draft.id}`} to={`/evor/${props.draft.id}`}>
          {props.draft.name}
        </Link>
      </Title>
      <Popover title={popoverTitle} open={opened} placement="top" content={popoverContent}>
        <Button
          id={`evor-deleteDraft-btn-${props.draft.id}`}
          onClick={() => setOpened(true)}
          icon={<DeleteOutlined />}
          type="text"
          ref={deleteMainBtnRef}
          style={{ float: 'right', borderColor: 'white' }}
          aria-label={t('startseite.iconBtnDelete')}
        />
      </Popover>
      <p className="ant-typography p-no-style">
        {t('startseite.lastEdited', { date: getDateTimeString(props.draft.lastmodified as string) })}
      </p>
      <hr />
    </div>
  );
}
