// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './startseite.less';

import { Button, Form, Input, InputRef, Typography } from 'antd';
import { Content } from 'antd/es/layout/layout';
import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import { BASE_PATH } from '@plateg/rest-api';

import { saveDraft } from '../../shares/localStorage';
import { FileUploadComponent } from '../file-upload/component.react';
import { createNewRegelungsentwurf } from './controller';
import { RecentDraftsView } from './recentDraftsView/component.react';

export function Startseite(): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const history = useHistory();
  const [showLoadComponent, setShowLoadComponent] = useState(true);
  const [openFileDialog, setOpenFileDialog] = useState<{ trigger: () => void }>();
  const { Title } = Typography;
  const inputRef = useRef<InputRef>(null);

  const onFinish = () => {
    const id = createNewRegelungsentwurf(form.getFieldValue('draftName') as string);
    history.push(`/evor/${id}`);
  };

  const onAbort = () => {
    form.setFieldsValue({ draftName: '' });
    setShowLoadComponent(true);
  };

  const loadComponent = (
    <div className="evor-load-component">
      <Button id="evor-newProject-btn" type="primary" size={'large'} onClick={() => setShowLoadComponent(false)}>
        {t('startseite.btnNewProject')}
      </Button>
      <Button
        id="evor-loadProject-btn"
        type="default"
        size={'large'}
        style={{ marginLeft: '24px' }}
        onClick={openFileDialog?.trigger}
      >
        {t('startseite.btnLoadProject')}
      </Button>
      <FileUploadComponent
        successCallback={(file) => saveDraft(file, (url) => history.push(url))}
        setOpenUploadDialogFunction={setOpenFileDialog}
        mimeType="application/json"
      />
    </div>
  );

  const startNewComponent = (
    <div>
      <hr></hr>
      <Title level={3}>{t('startseite.newDraftTitle')}</Title>

      <Form form={form} layout="vertical" onFinish={onFinish} noValidate>
        <Form.Item
          name="draftName"
          label={<span>{t('startseite.newDraftSubtitle')}</span>}
          rules={[
            { required: true, whitespace: true, message: 'Bitte geben Sie einen Namen für den Regelungsentwurf ein!' },
          ]}
        >
          <Input autoFocus required ref={inputRef} />
        </Form.Item>
        <Form.Item>
          <Button
            id="evor-startProject-btn"
            type="primary"
            size={'large'}
            onClick={() => {
              inputRef.current?.focus();
            }}
            htmlType="submit"
          >
            {t('startseite.btnStartProject')}
          </Button>
          <Button
            id="evor-abortStartProject-btn"
            type="default"
            size={'large'}
            style={{ marginLeft: '24px' }}
            onClick={onAbort}
          >
            {t('startseite.btnAbort')}
          </Button>
        </Form.Item>
      </Form>
      <hr></hr>
    </div>
  );

  return (
    <div id="startpageContent">
      <Content className="holder">
        <Title level={1}>{t('startseite.mainTitle')}</Title>
        <section className="content-section drafts-section">
          <RecentDraftsView />
        </section>
        <section className="content-section">
          <Title level={3}>{t('startseite.subTitle')}</Title>
          <p
            dangerouslySetInnerHTML={{
              __html: t('startseite.infoText', { basePath: BASE_PATH }),
            }}
            className="ant-typography"
          ></p>
          {showLoadComponent ? loadComponent : startNewComponent}
        </section>
      </Content>
    </div>
  );
}
