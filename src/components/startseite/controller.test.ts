// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect, use } from 'chai';
import * as chaiArrays from 'chai-arrays';
import { v4 as uuidv4 } from 'uuid';

import { EvorData } from '@plateg/rest-api';

import { initLocalstorageMock } from '../../localstorageMock';
import { DraftCollection } from '../../types/DraftCollection';
import {
  collectionKey,
  createNewRegelungsentwurf,
  deleteDraft,
  getDraftById,
  getDraftList,
  loadDraft,
} from './controller';
use(chaiArrays.default);

describe('Retrieve draftList from Localstorage', () => {
  beforeEach(() => {
    initLocalstorageMock();
  });
  it('Empty Storage', () => {
    expect(getDraftList()).to.eql([]);
  });
  it('Empty Draftcollection', () => {
    global.window.localStorage.setItem(collectionKey, '{}');
    expect(getDraftList()).to.eql([]);
  });
  it('Existing draft', () => {
    global.window.localStorage.setItem(collectionKey, '{"12345":{"name":"Hello", "lastModified":"21.08.2020"}}');
    expect(getDraftList()).to.eql([{ name: 'Hello', lastModified: '21.08.2020', id: '12345' }]);
  });
});

describe('Test Create New Regelungsentwurf', () => {
  beforeEach(() => {
    initLocalstorageMock();
  });
  it('Empty Storage', () => {
    const mockId1 = createNewRegelungsentwurf('Draft1');
    const mockId2 = createNewRegelungsentwurf('');
    const draftCollectionString: string = global.window.localStorage.getItem(collectionKey) || '';
    const draftCollection: DraftCollection = JSON.parse(draftCollectionString) as DraftCollection;
    expect(Object.keys(draftCollection)).to.have.lengthOf(2);
    expect(draftCollection[mockId1]).to.have.all.keys('lastmodified', 'name', 'id');
    expect(draftCollection[mockId2]).to.have.all.keys('lastmodified', 'name', 'id');
    expect(draftCollection[mockId1]['name']).to.eql('Draft1');
    expect(draftCollection[mockId2]['name']).to.eql('');
  });
});

describe('Test delete Regelungsentwurf', () => {
  beforeEach(() => {
    initLocalstorageMock();
  });

  it('Non-Empty Storage', () => {
    const mockId1 = createNewRegelungsentwurf('Draft1');
    deleteDraft(mockId1);
    const draftCollectionString: string = global.window.localStorage.getItem(collectionKey) || '';
    const draftCollection: DraftCollection = JSON.parse(draftCollectionString) as DraftCollection;
    expect(Object.keys(draftCollection)).to.have.lengthOf(0);
  });
});

describe('Test Load Regelungsentwurf', () => {
  beforeEach(() => {
    initLocalstorageMock();
  });

  it('Add to Storage', () => {
    const mockId1 = createNewRegelungsentwurf('Draft1');
    const mock1: EvorData | null = getDraftById(mockId1);
    const mockId2 = uuidv4();
    const mockName2 = 'Draft 2';
    if (mock1 != null) {
      const mock2: EvorData = copyDraft(mock1, mockId2, mockName2);
      loadDraft(mock2);
    }
    const draftCollectionString: string = global.window.localStorage.getItem(collectionKey) || '';
    const draftCollection: DraftCollection = JSON.parse(draftCollectionString) as DraftCollection;
    expect(Object.keys(draftCollection)).to.have.lengthOf(2);
    expect(getDraftById(mockId2)).to.have.property('name', mockName2);
  });
});

const copyDraft = (sourceObj: EvorData, newId: string, newName: string): EvorData => {
  return Object.assign({}, sourceObj, { id: newId, name: newName });
};
