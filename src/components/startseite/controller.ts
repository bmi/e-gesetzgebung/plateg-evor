// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { v4 as uuidv4 } from 'uuid';

import { EvorData } from '@plateg/rest-api';

import { DraftCollection } from '../../types/DraftCollection';
export const collectionKey = 'draftCollection';

function _getDraftCollection(): DraftCollection {
  const draftCollectionString: string = global.window.localStorage.getItem(collectionKey) || '{}';
  return JSON.parse(draftCollectionString) as DraftCollection;
}

function _setDraftCollection(draftCollection: DraftCollection): void {
  global.window.localStorage.setItem(collectionKey, JSON.stringify(draftCollection));
}

export function createNewRegelungsentwurf(draftName: string): string {
  const newId = uuidv4();
  const draftCollection: DraftCollection = _getDraftCollection();
  draftCollection[newId] = { name: draftName, lastmodified: new Date().toISOString(), id: newId };
  _setDraftCollection(draftCollection);
  return newId;
}

export function getDraftList(): EvorData[] {
  const draftCollection: DraftCollection = _getDraftCollection();
  const draftList: EvorData[] = [];
  Object.keys(draftCollection).forEach((key) => {
    const tmpRegelungsentwurf: EvorData = draftCollection[key];
    tmpRegelungsentwurf['id'] = key;
    draftList.push(tmpRegelungsentwurf);
  });
  return draftList;
}

export function deleteDraft(id: string): void {
  const draftCollection: DraftCollection = _getDraftCollection();
  delete draftCollection[id];
  _setDraftCollection(draftCollection);
}

export function loadDraft(draft: EvorData): void {
  const draftCollection: DraftCollection = _getDraftCollection();
  draftCollection[draft.id] = draft;
  _setDraftCollection(draftCollection);
}

export function getDraftById(id: string): EvorData | null {
  return _getDraftCollection()[id];
}
