// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EvorData } from '@plateg/rest-api';

import { deleteDraft, getDraftList } from '../controller';
import { DraftSummaryElement } from '../draftSummaryElement/component.react';
export function RecentDraftsView(): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const [draftList, setDraftList] = useState<EvorData[]>(getDraftList());
  const deleteD = (id: string) => {
    deleteDraft(id);
    setDraftList(getDraftList());
  };
  if (draftList.length === 0) {
    return <></>;
  } else {
    const draftListComponent = draftList.map((draft) => (
      <li key={draft.id}>
        <DraftSummaryElement draft={draft} deleteDraft={deleteD} key={draft.id} />
      </li>
    ));
    return (
      <section className="content-box content-box-white draft-list">
        <Title level={3} className="draft-list-title">
          {t('startseite.lastModifiedTitle')}
        </Title>
        <ul>{draftListComponent}</ul>
      </section>
    );
  }
}
