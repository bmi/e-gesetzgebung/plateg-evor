// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../styles/plateg-evor.less';

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { CodelistenControllerApi, Configuration, EvorControllerApi, UserControllerApi } from '@plateg/rest-api';
import { CodeListenController, configureRestApi, HeaderController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { EVOR } from '../evor/component.react';
import { Startseite } from '../startseite/component.react';

export function EVorApp(): React.ReactElement {
  GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
  configureRestApi(registerRestApis);

  return (
    <Switch>
      <Route exact path="/evor">
        <Startseite />
      </Route>
      <Route path="/evor/:id">
        <EVOR />
      </Route>
    </Switch>
  );
}

function registerRestApis(configRestCalls: Configuration) {
  GlobalDI.getOrRegister('evorController', () => new EvorControllerApi(configRestCalls));
  GlobalDI.getOrRegister('userController', () => new UserControllerApi(configRestCalls));
  GlobalDI.getOrRegister('headerController', () => new HeaderController());
  GlobalDI.getOrRegister('codelistenControllerApi', () => new CodelistenControllerApi(configRestCalls));
  GlobalDI.getOrRegister('codeListenController', () => new CodeListenController());
}
