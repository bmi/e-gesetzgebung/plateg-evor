// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { saveAs } from 'file-saver';
import { Observable } from 'rxjs';
import sanitize from 'sanitize-filename';

import { EvorControllerApi, EvorData } from '@plateg/rest-api';
import { LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares/injector';
export class FileUploadController {
  private readonly loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  public saveFile(draft: EvorData): void {
    this.loadingStatusController.setLoadingStatus(true);
    const blob = new Blob([JSON.stringify(draft, null, 2)], { type: 'application/json;charset=utf-8' });
    const filename = sanitize(draft.name || '') + '.json';
    console.debug(
      "will save draft '%s' with id '%s' and a size of %i bytes to file '%s'...",
      draft.name,
      draft.id,
      blob.size,
      filename,
    );
    saveAs(blob, filename);
    this.loadingStatusController.setLoadingStatus(false);
  }

  public loadFile(file: File, success: (draft: string) => void): void {
    this.loadingStatusController.setLoadingStatus(true);
    console.debug("will load file '%s' of type '%s' with %i bytes...", file.name, file.type, file.size);
    const reader = new FileReader();
    reader.onload = (e) => {
      const text = e.target?.result as string;
      success(text);
    };
    reader.readAsText(file);
    this.loadingStatusController.setLoadingStatus(false);
  }

  public exportDocument(draft: EvorData, type: string): void {
    this.loadingStatusController.setLoadingStatus(true);
    const body = { evorData: draft };
    const evorController = GlobalDI.get<EvorControllerApi>('evorController');
    let call: Observable<Blob>;
    if (type == 'pdf') {
      call = evorController.createPdfSummary(body);
    } else {
      console.warn("type '%s' is unknown or not implemented yet!", type);
      return;
    }
    call.subscribe((data) => {
      saveAs(data, `${sanitize(draft.name || '')}.${type}`);
      this.loadingStatusController.setLoadingStatus(false);
    });
  }
}
