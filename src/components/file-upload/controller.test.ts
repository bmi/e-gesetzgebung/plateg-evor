// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { expect } from 'chai';
import FileSaver from 'file-saver';
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';
import Sinon from 'sinon';

import { EvorControllerApi, EvorData } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { setLoadingStatusStub } from '../../general.test';
import { FileUploadController } from './controller';

describe('Test FileUploadController', () => {
  const fileCtrl = new FileUploadController();
  const evorController = GlobalDI.getOrRegister('evorController', () => new EvorControllerApi());
  const consoleDebugSpy = Sinon.spy(console, 'debug');
  const saveAsSpy = Sinon.spy(FileSaver, 'saveAs');
  const createPdfSummaryStub = Sinon.stub(evorController, 'createPdfSummary');
  const callbackStub = Sinon.stub();
  window.URL.createObjectURL = Sinon.stub();

  const evorData: EvorData = { id: '0', name: 'Test0' };

  beforeEach(() => {
    consoleDebugSpy.resetHistory();
    saveAsSpy.resetHistory();
    setLoadingStatusStub.resetHistory();
    createPdfSummaryStub.resetHistory();
  });

  after(() => {
    consoleDebugSpy.restore();
    saveAsSpy.restore();
    createPdfSummaryStub.restore();
  });

  describe('Test saveFile', () => {
    it('valid input, correct funtions are called', () => {
      fileCtrl.saveFile(evorData);

      Sinon.assert.calledTwice(setLoadingStatusStub);
      const debugParameters = consoleDebugSpy.getCalls()[0].args;
      expect(debugParameters[0]).to.eql("will save draft '%s' with id '%s' and a size of %i bytes to file '%s'...");
      expect(debugParameters[1]).to.eql('Test0');
      expect(debugParameters[2]).to.eql('0');
      expect(debugParameters[4]).to.eql('Test0.json');

      const saveAsParameters = saveAsSpy.getCalls()[0].args;
      expect(Object.getPrototypeOf(saveAsParameters[0])).to.eql(Blob.prototype);
      expect(saveAsParameters[1]).to.eql('Test0.json');
    });
  });

  describe('Test exportDocument', () => {
    it('valid input, pdf, correct funtions are called', () => {
      createPdfSummaryStub.callsFake(() => {
        return new Observable<AjaxResponse<Blob>>((observer) => {
          observer.next();
        });
      });
      fileCtrl.exportDocument(evorData, 'pdf');

      Sinon.assert.calledTwice(setLoadingStatusStub);
      Sinon.assert.calledOnce(createPdfSummaryStub);
      expect(createPdfSummaryStub.getCalls()[0].args[0].evorData).to.eq(evorData);
      Sinon.assert.calledOnce(saveAsSpy);
      Sinon.assert.calledTwice(setLoadingStatusStub);
    });
  });
});
