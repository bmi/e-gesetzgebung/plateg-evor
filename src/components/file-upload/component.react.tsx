// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useRef } from 'react';

import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { FileUploadController } from './controller';
interface FileComponentProps {
  successCallback: (fileText: string) => void;
  setOpenUploadDialogFunction: (obj: { trigger: () => void }) => void;
  mimeType: string;
}

export function FileUploadComponent(props: FileComponentProps): React.ReactElement {
  const inputFileRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    const openUploadDialogFunction = {
      trigger: () => inputFileRef.current?.click(),
    };
    props.setOpenUploadDialogFunction(openUploadDialogFunction);
  }, [inputFileRef]);

  const ctrl = GlobalDI.getOrRegister('evorFileUploadController', () => new FileUploadController());

  const selectFile = (): void => {
    const file = inputFileRef.current?.files?.[0];
    if (file) {
      ctrl.loadFile(file, props.successCallback);
    }
  };

  return (
    <input style={{ display: 'none' }} type="file" ref={inputFileRef} onChange={selectFile} accept={props.mimeType} />
  );
}
