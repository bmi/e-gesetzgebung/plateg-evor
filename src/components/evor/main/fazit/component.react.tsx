// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './fazit.less';

import { Checkbox, Form, Input, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Bewertung, Konsultationen, Regelungsalternative } from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper, InfoComponent } from '@plateg/theme';

import { saveDraftContent } from '../../../../shares/localStorage';
import { ROUTES } from '../../../../shares/routes';
import { StandardEvorProps } from '../component.react';

interface FazitValues {
  bewertung: Bewertung;
  konsultationen: Konsultationen;
  regelungsalternativen: Regelungsalternative[];
}

export function FazitComponent(props: StandardEvorProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const [form] = Form.useForm();
  const { TextArea } = Input;
  const currentDraftId = props.currentDraft.id;
  const [showForm, setShowForm] = useState(false);
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    if (
      props.currentDraft.regelungsalternativen?.regelungsalternativeList !== undefined &&
      props.currentDraft.regelungsalternativen?.regelungsalternativeList?.length > 0
    ) {
      setShowForm(true);
    } else {
      setShowForm(false);
    }
    props.setIsFormDirty?.(false);
  }, []);

  const regelungsalternativenList: Regelungsalternative[] =
    props.currentDraft.regelungsalternativen?.regelungsalternativeList || [];

  const formInitialValue: FazitValues = {
    bewertung: props.currentDraft.bewertung || {
      gesamtbewertung: '',
      fazit: '',
      bewertungKriterien: [],
      bewertungZiele: [],
      bewertungPolitikbereiche: [],
    },
    konsultationen: props.currentDraft.konsultationen || {
      zusammenfassung: '',
      weitereKonsultationen: [],
    },
    regelungsalternativen: regelungsalternativenList.reduce(
      (acc: Regelungsalternative[], item: Regelungsalternative) => {
        const regelungItem: Regelungsalternative = {
          ...item,
          entschieden: item.entschieden || false,
        };
        acc.push(regelungItem);
        return acc;
      },
      [],
    ),
  };

  const onCheckboxChange = (index: number, checked: boolean) => {
    const formValues: FazitValues = form.getFieldsValue(true) as FazitValues;
    formValues.regelungsalternativen[index] = { ...formInitialValue.regelungsalternativen[index] };
    formValues.regelungsalternativen[index].entschieden = checked;
    form.setFieldsValue(formValues);
  };

  const saveDraft = () => {
    const values: FazitValues = form.getFieldsValue(true) as FazitValues;
    values.bewertung = { ...formInitialValue.bewertung, ...values.bewertung };
    values.regelungsalternativen?.forEach(function (item, key) {
      const entschiedenState = item.entschieden;
      values.regelungsalternativen[key] = { ...formInitialValue.regelungsalternativen[key] };
      values.regelungsalternativen[key].entschieden = entschiedenState;
    });
    saveDraftContent({
      id: currentDraftId,
      bewertung: {
        gesamtbewertung: values.bewertung.gesamtbewertung,
        fazit: values.bewertung.fazit,
        bewertungKriterien: values.bewertung.bewertungKriterien,
        bewertungZiele: values.bewertung.bewertungZiele,
        bewertungPolitikbereiche: values.bewertung.bewertungPolitikbereiche,
      },
      konsultationen: {
        zusammenfassung: values.konsultationen.zusammenfassung,
        weitereKonsultationen: values.konsultationen.weitereKonsultationen,
      },
      regelungsalternativen: {
        regelungsalternativeList: [...(values.regelungsalternativen ?? '')],
        teilbereiche: props?.currentDraft?.regelungsalternativen?.teilbereiche,
      },
    });
  };

  return (
    <div className="fazit-component">
      <FormWrapper
        projectName="eVoR"
        title={
          <div className="heading-holder">
            <Title level={1}>{t('fazit.title')}</Title>
          </div>
        }
        previousPage={`/evor/${currentDraftId}/${ROUTES.konsultation.konsultationErgebnisse}`}
        nextPage={`/evor/${currentDraftId}/${ROUTES.ergebnisdokumentation}`}
        saveDraft={saveDraft}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={formInitialValue}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          setIsDirty(true);
          props.setIsFormDirty?.(true);
        }}
      >
        <p>{t('fazit.goalDefinitionInfoFazit')}</p>
        <Title level={2}>{t('fazit.subtitle1')}</Title>
        {showForm === true && (
          <>
            <ul className="regelungsalternativen-list">
              {regelungsalternativenList.map((item: Regelungsalternative, i) => {
                return (
                  <li key={i}>
                    <span className="nonBreakingArea">
                      <span className="normalBreakingArea">{item.name}</span>&nbsp;
                      <span className="rang">
                        {item.rang ? 'Gesamtrang ' + item.rang.toString() : 'Gesamtrang n/a'}
                      </span>
                    </span>
                  </li>
                );
              })}
            </ul>
            <FormItemWithInfo
              name={['bewertung', 'gesamtbewertung']}
              label={
                <>
                  <span>{t('fazit.subtitle2')} &nbsp;</span>
                  <InfoComponent withLabel title={t('fazit.subtitle2')}>
                    <p>{t('fazit.infoSubitle2')}</p>
                  </InfoComponent>
                </>
              }
            >
              <TextArea rows={8} />
            </FormItemWithInfo>
            <FormItemWithInfo
              name={['konsultationen', 'zusammenfassung']}
              label={
                <>
                  <span>{t('fazit.subtitle3')} &nbsp;</span>
                  <InfoComponent withLabel title={t('fazit.subtitle3')}>
                    <p>{t('fazit.infoSubitle2')}</p>
                  </InfoComponent>
                </>
              }
            >
              <TextArea rows={8} />
            </FormItemWithInfo>
            <Title level={2}>{t('fazit.subtitle4')}</Title>
            <FormItemWithInfo
              name={['bewertung', 'fazit']}
              label={
                <>
                  <span>{t('fazit.subtitle5')} &nbsp;</span>
                  <InfoComponent withLabel title={t('fazit.drawerFazitTitleSmall')}>
                    <p>{t('fazit.drawerFazitContentSmall')}</p>
                  </InfoComponent>
                </>
              }
            >
              <TextArea rows={8} />
            </FormItemWithInfo>
            <Title level={3}>{t('fazit.subtitle6')}</Title>
            <p>{t('fazit.hintGesamtFazit')}</p>
            <ul>
              {regelungsalternativenList.map((item: Regelungsalternative, i) => {
                return (
                  <li key={i}>
                    <Form.Item
                      name={['regelungsalternativen', i, 'entschieden']}
                      className="title-holder"
                      valuePropName={item.entschieden ? 'checked' : ''}
                    >
                      <Checkbox
                        id={`evor-regelungsalternativenBewertungEntschieden-${item?.id}-chk`}
                        onChange={(e) => onCheckboxChange(i, e.target.checked)}
                      >
                        {item.name}
                      </Checkbox>
                    </Form.Item>
                  </li>
                );
              })}
            </ul>
          </>
        )}
        {showForm === false && <p className="ant-typography p-no-style">{t('fazit.noRegelungsalternativenText')}</p>}
      </FormWrapper>
    </div>
  );
}
