// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, FormWrapper, InfoComponent } from '@plateg/theme';

import { saveDraftContent } from '../../../../shares/localStorage';
import { ROUTES } from '../../../../shares/routes';
import { StandardEvorProps } from '../component.react';

interface AnalyseValues {
  problemanalyse?: string;
  akteursSystemanalyse?: string;
}

export function AnalyseComponent(props: StandardEvorProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const [form] = Form.useForm();
  const { TextArea } = Input;
  const [isDirty, setIsDirty] = useState(false);

  const formInitialValue: AnalyseValues = {
    problemanalyse: props.currentDraft.problemanalyse || '',
    akteursSystemanalyse: props.currentDraft.akteursSystemanalyse || '',
  };

  useEffect(() => {
    props.setIsFormDirty?.(false);
  }, []);

  const saveDraft = () => {
    const values: AnalyseValues = form.getFieldsValue(true) as AnalyseValues;
    saveDraftContent({
      ...values,
      id: props.currentDraft.id,
    });
  };

  const title = (
    <div className="heading-holder">
      <span style={{ whiteSpace: 'nowrap' }}>
        <Title level={1}>{t('analyseRegelungsfeldes.title')}</Title>
        <InfoComponent title={t('analyseRegelungsfeldes.drawerAnalyseTitle')}>
          <p className="ant-typography p-no-style">{t('analyseRegelungsfeldes.drawerAnalyseIntro')}</p>
          <p className="ant-typography p-no-style">
            {t('analyseRegelungsfeldes.drawerAnalyseProblem1')} <br />
            {t('analyseRegelungsfeldes.drawerAnalyseProblem2')}
          </p>
          <p className="ant-typography p-no-style">{t('analyseRegelungsfeldes.drawerAnalyseText')}</p>
        </InfoComponent>
      </span>
    </div>
  );

  return (
    <div>
      <FormWrapper
        projectName="eVoR"
        title={title}
        previousPage={''}
        nextPage={`/evor/${props.currentDraft.id}/${ROUTES.zielAnalyse}`}
        saveDraft={saveDraft}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={formInitialValue}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          setIsDirty(true);
          props.setIsFormDirty?.(true);
        }}
      >
        <p className="ant-typography p-no-style">{t('analyseRegelungsfeldes.info')}</p>
        <FormItemWithInfo
          name="problemanalyse"
          label={
            <span>
              {t('analyseRegelungsfeldes.fromProblemanalyseLable')}
              <InfoComponent withLabel title={t('analyseRegelungsfeldes.drawerProblemAnalyseTitle')}>
                <p className="ant-typography p-no-style">{t('analyseRegelungsfeldes.drawerProblemAnalyseText')}</p>
                <p className="ant-typography p-no-style">{t('analyseRegelungsfeldes.drawerProblemAnalyseText2')}</p>
              </InfoComponent>
            </span>
          }
        >
          <TextArea rows={9} />
        </FormItemWithInfo>
        <FormItemWithInfo
          name="akteursSystemanalyse"
          label={
            <span>
              {t('analyseRegelungsfeldes.formSystemanalyseLable')}
              <InfoComponent withLabel title={t('analyseRegelungsfeldes.drawerSystemAnalyseTitle')}>
                <p className="ant-typography p-no-style">{t('analyseRegelungsfeldes.drawerSystemAnalyseText')}</p>
                <p className="ant-typography p-no-style">{t('analyseRegelungsfeldes.drawerSystemAnalyseText2')}</p>
              </InfoComponent>
            </span>
          }
        >
          <TextArea rows={9} />
        </FormItemWithInfo>
      </FormWrapper>
    </div>
  );
}
