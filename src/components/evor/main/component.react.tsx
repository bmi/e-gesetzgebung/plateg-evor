// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Col, Row } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import { EvorData } from '@plateg/rest-api';
import { BreadcrumbComponent, DropdownMenu, HeaderController } from '@plateg/theme';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { ContinueLaterButton, ContinueLaterComponent } from '../../../shares/continue-later/component.react';
import { saveDraftContent } from '../../../shares/localStorage';
import { ROUTES } from '../../../shares/routes';
import { FileUploadController } from '../../file-upload/controller';
import { AnalyseComponent } from './analyse-regelungsfeld/component.react';
import { IdeenComponent } from './entwicklung-alternativen/entwicklung-ideen/component.react';
import { EntwicklungAlternativenComponent } from './entwicklung-alternativen/zusammenstellung-alternativen/component.react';
import { ErgebnisdokumentationComponent } from './ergebnisdokumentation/component.react';
import { FazitComponent } from './fazit/component.react';
import { ErgebnisseComponent } from './konsultation/ergebnisse/component.react';
import { KonsultationVorbereitungComponent } from './konsultation/vorbereitung/component.react';
import { PolitikbereichenComponent } from './regelungsalternativen-pruefung/auswirkungen-politikbereiche/component.react';
import { AuswirkungenNachRegelungsalternativenComponent } from './regelungsalternativen-pruefung/auswirkungen-regelungsalternativen/component.react';
import { BewertungComponent } from './regelungsalternativen-pruefung/bewertung/component.react';
import { ZielanalyseComponent } from './zielanalyse/component.react';

export interface StandardEvorProps {
  currentDraft: EvorData;
  setFormInstance: (form: FormInstance | undefined) => void;
  setIsFormDirty?: (isDirty: boolean) => void;
  formInstance?: FormInstance;
}

export function MainComponent(props: StandardEvorProps): React.ReactElement {
  const headerController = GlobalDI.get<HeaderController>('headerController');
  const fileCtrl = GlobalDI.getOrRegister('evorFileUploadController', () => new FileUploadController());
  const { t } = useTranslation();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; pageName: string }>('/evor/:id/:pageName');
  const [isFormDirty, setIsFormDirty] = useState(false);
  const [isSaveLaterAvailable, setIsSaveLaterAvailable] = useState(true);

  useEffect(() => {
    const pageName = routeMatcherVorbereitung?.params?.pageName as string;
    const hasPageContinueLater = !pagesWithoutSaveLater.some((item) => item === pageName);
    setIsSaveLaterAvailable(hasPageContinueLater);

    const dropdownMenuItems: DropdownMenuItem[] = [
      {
        element: t('evor.header.linkPDF'),
        onClick: () => {
          fileCtrl.exportDocument(props.currentDraft, 'pdf');
        },
      },
      {
        element: t('evor.header.linkSave'),
        onClick: () => {
          fileCtrl.saveFile(props.currentDraft);
        },
      },
    ];
    if (hasPageContinueLater) {
      dropdownMenuItems.push({
        element: t('evor.continueLater.btnText'),
        disabled: () => !isFormDirty,
        onClick: () => {
          setIsFormDirty(false);
          saveDraftContent({ ...props.currentDraft, ...props.formInstance?.getFieldsValue(true) } as EvorData, true);
        },
      });
    }
    headerController.setHeaderProps({
      headerRight: [
        <Button
          id="evor-exportPDFDocument-btn"
          key="evor-pdf"
          type="default"
          size="small"
          onClick={() => fileCtrl.exportDocument(props.currentDraft, 'pdf')}
        >
          {t('evor.header.linkPDF')}
        </Button>,
        <Button
          id="evor-saveFile-btn"
          key="evor-save"
          type="default"
          size="small"
          onClick={() => fileCtrl.saveFile(props.currentDraft)}
        >
          {t('evor.header.linkSave')}
        </Button>,
        hasPageContinueLater ? (
          <ContinueLaterButton
            key="continue-later-btn"
            isDisabled={!isFormDirty}
            saveCurrentState={() => {
              setIsFormDirty(false);
              saveDraftContent(
                { ...props.currentDraft, ...props.formInstance?.getFieldsValue(true) } as EvorData,
                true,
              );
            }}
          />
        ) : (
          <></>
        ),
        <DropdownMenu
          items={dropdownMenuItems}
          elementId="headerRightAlternative"
          overlayClass="headerRightAlternative-overlay"
        />,
      ],
      headerLast: [],
    });
  }, [props.currentDraft, isFormDirty, routeMatcherVorbereitung]);

  const pagesWithoutSaveLater = ['ergebnisdokumentation'];
  useEffect(() => {
    const pageName = routeMatcherVorbereitung?.params?.pageName as string;

    headerController.setHeaderProps({
      headerLeft: [
        <BreadcrumbComponent
          key="breadcrumb"
          items={[
            <Link id="evor-home-link" key="evor-home" to="/evor/">
              {t('evor.header.linkHome')}
            </Link>,
            <span key={`evor-${pageName}`}>{props.currentDraft.name}</span>,
          ]}
        />,
      ],
    });
  }, [routeMatcherVorbereitung]);

  return (
    <Row>
      <Col
        xs={{ span: 22, offset: 1 }}
        md={{ span: 14, offset: 2 }}
        lg={{ span: 16, offset: 3 }}
        xl={{ span: 12, offset: 3 }}
        xxl={{ span: 10, offset: 4 }}
      >
        <Switch>
          <Route exact path={`/evor/:id/${ROUTES.analyse}`}>
            <AnalyseComponent
              currentDraft={props.currentDraft}
              setFormInstance={props.setFormInstance}
              setIsFormDirty={setIsFormDirty}
            />
          </Route>
          <Route exact path={`/evor/:id/${ROUTES.zielAnalyse}`}>
            <ZielanalyseComponent
              currentDraft={props.currentDraft}
              setFormInstance={props.setFormInstance}
              setIsFormDirty={setIsFormDirty}
            />
          </Route>
          <Route exact path={`/evor/:id/${ROUTES.entwicklung.entwicklungIdeen}`}>
            <IdeenComponent
              currentDraft={props.currentDraft}
              setFormInstance={props.setFormInstance}
              setIsFormDirty={setIsFormDirty}
            />
          </Route>
          <Route exact path={`/evor/:id/${ROUTES.entwicklung.entwicklungAlternativen}`}>
            <EntwicklungAlternativenComponent
              currentDraft={props.currentDraft}
              setFormInstance={props.setFormInstance}
              setIsFormDirty={setIsFormDirty}
            />
          </Route>
          <Route exact path={`/evor/:id/${ROUTES.bewertung.bewertungAuswirkungenPolitik}`}>
            <PolitikbereichenComponent
              currentDraft={props.currentDraft}
              setFormInstance={props.setFormInstance}
              setIsFormDirty={setIsFormDirty}
            />
          </Route>
          <Route exact path={`/evor/:id/${ROUTES.bewertung.bewertungAuswirkungenNachAlternative}`}>
            <AuswirkungenNachRegelungsalternativenComponent
              currentDraft={props.currentDraft}
              setFormInstance={props.setFormInstance}
              setIsFormDirty={setIsFormDirty}
            />
          </Route>
          <Route exact path={`/evor/:id/${ROUTES.bewertung.bewertung}`}>
            <BewertungComponent
              currentDraft={props.currentDraft}
              setFormInstance={props.setFormInstance}
              setIsFormDirty={setIsFormDirty}
            />
          </Route>
          <Route exact path={`/evor/:id/${ROUTES.konsultation.konsultationVorbereitung}`}>
            <KonsultationVorbereitungComponent
              currentDraft={props.currentDraft}
              setFormInstance={props.setFormInstance}
              setIsFormDirty={setIsFormDirty}
            />
          </Route>
          <Route exact path={`/evor/:id/${ROUTES.konsultation.konsultationErgebnisse}`}>
            <ErgebnisseComponent
              currentDraft={props.currentDraft}
              setFormInstance={props.setFormInstance}
              setIsFormDirty={setIsFormDirty}
            />
          </Route>
          <Route exact path={`/evor/:id/${ROUTES.fazit}`}>
            <FazitComponent
              currentDraft={props.currentDraft}
              setFormInstance={props.setFormInstance}
              setIsFormDirty={setIsFormDirty}
            />
          </Route>
          <Route exact path={`/evor/:id/${ROUTES.ergebnisdokumentation}`}>
            <ErgebnisdokumentationComponent currentDraft={props.currentDraft} setFormInstance={props.setFormInstance} />
          </Route>
          <Route
            path="/evor/:id"
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect to={`/evor/${match.params.id}/analyse`} />
            )}
          />
        </Switch>
        {isSaveLaterAvailable && (
          <ContinueLaterComponent
            saveCurrentState={() => {
              setIsFormDirty(false);
              saveDraftContent(
                { ...props.currentDraft, ...props.formInstance?.getFieldsValue(true) } as EvorData,
                true,
              );
            }}
            isDisabled={!isFormDirty}
          />
        )}
      </Col>
    </Row>
  );
}
