// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { BASE_PATH, Regelungsalternative } from '@plateg/rest-api';
import { FormWrapper, InfoComponent, RepeatableFormComponent } from '@plateg/theme';

import { saveDraftContent } from '../../../../../shares/localStorage';
import { ROUTES } from '../../../../../shares/routes';
import { StandardEvorProps } from '../../component.react';
import { AlternativeComponent } from './alternative-component/component.react';
import { removeDeletedAlternativesFromBewertung } from './controller';

interface EntwicklungAlternativenValues {
  regelungsalternativeList: Regelungsalternative[];
}

export function EntwicklungAlternativenComponent(props: StandardEvorProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const [form] = Form.useForm();
  const currentDraftId = props.currentDraft.id;
  const [isDirty, setIsDirty] = useState(false);

  const setDirtyFlag = () => {
    setIsDirty(true);
  };

  useEffect(() => {
    props.setIsFormDirty?.(false);
  }, []);

  // First call current draft is not yet filled, so we give defaults
  const formInitialValue: EntwicklungAlternativenValues = {
    regelungsalternativeList: props.currentDraft.regelungsalternativen?.regelungsalternativeList || [
      { id: uuidv4(), name: '', text: '', teilbereichList: [], auswirkungenPolitikbereiche: [] },
    ],
  };

  const getRepeatableItem = () => {
    return (
      (form.getFieldsValue(true) as EntwicklungAlternativenValues).regelungsalternativeList ||
      formInitialValue.regelungsalternativeList
    );
  };

  const setRepeatableItem = (regelungsalternativeList: Regelungsalternative[]) => {
    const currentValues = form.getFieldsValue(true) as EntwicklungAlternativenValues;
    currentValues.regelungsalternativeList = regelungsalternativeList;
    form.setFieldsValue(currentValues);
  };

  const saveDraft = () => {
    const values = form.getFieldsValue(true) as EntwicklungAlternativenValues;
    values?.regelungsalternativeList?.forEach((alternative, i) => {
      alternative.teilbereichList.forEach((teilbereich, ii) => {
        if (teilbereich?.teilbereichsoptionId !== undefined && teilbereich?.teilbereichsoptionId === 'empty') {
          delete values.regelungsalternativeList[i].teilbereichList[ii].teilbereichsoptionId;
        }
      });
    });
    saveDraftContent({
      id: currentDraftId,
      regelungsalternativen: {
        ...props.currentDraft.regelungsalternativen,
        regelungsalternativeList: values.regelungsalternativeList,
      },
      bewertung: removeDeletedAlternativesFromBewertung(
        props.currentDraft.bewertung || { bewertungKriterien: [], bewertungPolitikbereiche: [], bewertungZiele: [] },
        values.regelungsalternativeList,
      ),
    });
  };

  const title = (
    <div className="heading-holder">
      <span style={{ whiteSpace: 'nowrap' }}>
        <Title level={1}> {t('alternativesCollection.title')}</Title>
        <InfoComponent title={t('alternativesCollection.drawerHeadingTitle')}>
          <p className="ant-typography p-no-style">{t('alternativesCollection.drawerHeadingContent1')}</p>
          <p className="ant-typography p-no-style">{t('alternativesCollection.drawerHeadingContent2')}</p>
          <p
            className="ant-typography p-no-style"
            dangerouslySetInnerHTML={{
              __html: t('alternativesCollection.drawerHeadingContent3', {
                linkGGO: `${BASE_PATH}/arbeitshilfen/download/34#page=67`,
              }),
            }}
          />
        </InfoComponent>
      </span>
    </div>
  );

  return (
    <div>
      <FormWrapper
        projectName="eVoR"
        title={title}
        previousPage={`/evor/${currentDraftId}/${ROUTES.entwicklung.entwicklungIdeen}`}
        nextPage={`/evor/${currentDraftId}/${ROUTES.bewertung.bewertungAuswirkungenPolitik}`}
        saveDraft={saveDraft}
        isDirty={() => form.isFieldsTouched() || isDirty}
        form={form}
        formInitialValue={formInitialValue}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          props.setIsFormDirty?.(true);
        }}
      >
        <Title level={2}>{t('alternativesCollection.subtitle')}</Title>
        <p className="ant-typography p-no-style">{t('alternativesCollection.infoText')}</p>
        <RepeatableFormComponent
          getRepeatableItem={getRepeatableItem}
          setRepeatableItem={setRepeatableItem}
          emptyItem={() => {
            return { id: uuidv4(), name: '', text: '', teilbereiche: [], auswirkungenPolitikbereiche: [] };
          }}
          textAddItem="Regelungsalternative hinzufügen"
          setDirtyFlag={setDirtyFlag}
          repeatableComponent={(item: Regelungsalternative, index: number, deleteItem: (index: number) => void) => (
            <AlternativeComponent
              key={item.id}
              index={index}
              deleteAlternative={deleteItem}
              teilbereichList={props.currentDraft.regelungsalternativen?.teilbereiche?.teilbereichList}
              setDirtyFlag={setDirtyFlag}
              autoFocus={getRepeatableItem().length > 1 && isDirty}
            />
          )}
        />
      </FormWrapper>
    </div>
  );
}
