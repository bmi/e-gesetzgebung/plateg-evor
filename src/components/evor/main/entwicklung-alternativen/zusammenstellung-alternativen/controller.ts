// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Bewertung, Regelungsalternative } from '@plateg/rest-api';
export function removeDeletedAlternativesFromBewertung(
  bewertung: Bewertung,
  alternativen: Regelungsalternative[],
): Bewertung {
  const alternativenIds = alternativen.map((item) => item.id);
  const cleanedBewertung = { ...bewertung };
  cleanedBewertung.bewertungZiele = bewertung?.bewertungZiele?.filter(
    (item) => item.regelungsalternativeId && alternativenIds.includes(item.regelungsalternativeId),
  );
  cleanedBewertung.bewertungKriterien = bewertung?.bewertungKriterien?.filter(
    (item) => item.regelungsalternativeId && alternativenIds.includes(item.regelungsalternativeId),
  );
  cleanedBewertung.bewertungPolitikbereiche = bewertung?.bewertungPolitikbereiche?.filter(
    (item) => item.regelungsalternativeId && alternativenIds.includes(item.regelungsalternativeId),
  );
  return cleanedBewertung;
}
