// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form, Input, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { Teilbereich } from '@plateg/rest-api';
import { DeleteOutlined } from '@plateg/theme/src/components/icons/DeleteOutlined';

import { TeilbereichDropdownComponent } from './teilbereich-dropdown-component/component.react';
interface AlternativeComponentProps {
  index: number;
  deleteAlternative: (index: number) => void;
  teilbereichList?: Teilbereich[];
  setDirtyFlag: Function;
  autoFocus: boolean;
}

export function AlternativeComponent(props: AlternativeComponentProps): React.ReactElement {
  const { TextArea } = Input;
  const { t } = useTranslation();
  const { Title } = Typography;
  let teilbereichList;

  const deleteAlternativeAndSetDirty = (index: number) => {
    props.setDirtyFlag();
    props.deleteAlternative(index);
  };

  if (props.teilbereichList && props.teilbereichList.length !== 0) {
    teilbereichList = props.teilbereichList.map((item, _index) => {
      return (
        <TeilbereichDropdownComponent
          key={item.id}
          teilbereich={item}
          alternativenIndex={props.index}
          teilbereichIndex={_index}
        />
      );
    });
  } else {
    teilbereichList = [<p className="ant-typography p-no-style">{t('alternativesCollection.noTeilbereicheText')}</p>];
  }

  return (
    <>
      <Title level={3}>{`${t('alternativesCollection.elementTitle')} ${(props.index + 1).toString()}`}</Title>
      <div className="content-box content-box-blue double-margin-validation">
        <Form.Item
          name={['regelungsalternativeList', props.index, 'name']}
          label={<span>{`Titel der Regelungsalternative ${(props.index + 1).toString()}`}</span>}
          rules={[
            {
              required: true,
              whitespace: true,
              message: t('alternativesCollection.validationMessage', { index: props.index + 1 }),
            },
          ]}
        >
          <Input autoFocus={props.autoFocus} required />
        </Form.Item>
        <Form.Item
          name={['regelungsalternativeList', props.index, 'text']}
          label={<span>{`Kurzbeschreibung der Regelungsalternative ${(props.index + 1).toString()}`}</span>}
        >
          <TextArea rows={4} />
        </Form.Item>
        <h4>{`${t('alternativesCollection.elementSubtitle3Part1')} für Regelungsalternative ${(
          props.index + 1
        ).toString()} ${t('alternativesCollection.elementSubtitle3Part2')}`}</h4>
        <section className="content-box content-box-white">{teilbereichList}</section>
      </div>

      <div style={{ textAlign: 'right', marginTop: '8px' }}>
        <Button
          id="evor-deleteAlternative-btn"
          size={'small'}
          icon={<DeleteOutlined />}
          type="link"
          onClick={() => deleteAlternativeAndSetDirty(props.index)}
          className="blue-text-button"
        >
          {t('alternativesCollection.deleteAlternative', { index: props.index + 1 })}
        </Button>
      </div>
    </>
  );
}
