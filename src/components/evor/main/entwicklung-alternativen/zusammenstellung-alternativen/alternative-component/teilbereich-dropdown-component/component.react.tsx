// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import { DefaultOptionType } from 'antd/lib/select';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { Teilbereich, Teilbereichsoption } from '@plateg/rest-api';
import { MainContentSelectWrapper } from '@plateg/theme';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

interface TeilbereichDropdownProps {
  teilbereich?: Teilbereich;
  teilbereichIndex: number;
  alternativenIndex: number;
}

export function TeilbereichDropdownComponent(props: TeilbereichDropdownProps): React.ReactElement {
  const teilbereich = props.teilbereich || { optionen: [] as Teilbereichsoption[], name: '' };
  const { t } = useTranslation();

  const options: DefaultOptionType[] = teilbereich.optionen.map((item) => ({
    label: (
      <span key={item.id} aria-label={item.name}>
        {item.name}
      </span>
    ),
    value: item.id,
    title: item.name,
  }));
  options.push({
    label: (
      <span
        key={`${1}-${t('ergebnisdokumentation.pruefungBewertung.gesamtrangKeine')}`}
        aria-label={t('ergebnisdokumentation.pruefungBewertung.gesamtrangKeine')}
      >
        {t('ergebnisdokumentation.pruefungBewertung.gesamtrangKeine')}
      </span>
    ),
    value: 'empty',
    title: t('ergebnisdokumentation.pruefungBewertung.gesamtrangKeine').toString(),
  });

  return (
    <>
      {options.length > 0 && (
        <>
          <Form.Item
            name={[
              'regelungsalternativeList',
              props.alternativenIndex,
              'teilbereichList',
              props.teilbereichIndex,
              'teilbereichsoptionId',
            ]}
            label={<span>{teilbereich.name}</span>}
          >
            <MainContentSelectWrapper
              options={options}
              suffixIcon={<SelectDown />}
              placeholder={t('alternativesCollection.selectDefaultText')}
            />
          </Form.Item>
          <Form.Item
            hidden={true}
            initialValue={props.teilbereich?.id}
            name={[
              'regelungsalternativeList',
              props.alternativenIndex,
              'teilbereichList',
              props.teilbereichIndex,
              'teilbereichId',
            ]}
          >
            <input type="hidden" />
          </Form.Item>
        </>
      )}
      {options.length === 0 && (
        <p className="ant-typography p-no-style">
          <label>{teilbereich.name}</label>
          {t('alternativesCollection.noOptionenText')}
        </p>
      )}
    </>
  );
}
