// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect, use } from 'chai';
import * as chaiArrays from 'chai-arrays';

import { Bewertung, Regelungsalternative } from '@plateg/rest-api';

import { removeDeletedAlternativesFromBewertung } from './controller';
use(chaiArrays.default);

describe('Test Regelungsentwurf clean references to Regelungsalternativen', () => {
  it('Clean Bewertung standard use case', () => {
    const bewertung: Bewertung = {
      bewertungZiele: [
        {
          regelungsalternativeId: '1',
          zielId: '3',
        },
        {
          regelungsalternativeId: '2',
          zielId: '3',
        },
      ],
      bewertungPolitikbereiche: [
        {
          regelungsalternativeId: '1',
          politikbereichId: '4',
        },
        {
          regelungsalternativeId: '1',
          politikbereichId: '5',
        },
      ],
      bewertungKriterien: [
        {
          regelungsalternativeId: '2',
          pruefkriteriumId: '5',
        },
        {
          regelungsalternativeId: '2',
          pruefkriteriumId: '6',
        },
      ],
    };
    const expectedOutput: Bewertung = {
      bewertungZiele: [
        {
          regelungsalternativeId: '2',
          zielId: '3',
        },
      ],
      bewertungPolitikbereiche: [],
      bewertungKriterien: [
        {
          regelungsalternativeId: '2',
          pruefkriteriumId: '5',
        },
        {
          regelungsalternativeId: '2',
          pruefkriteriumId: '6',
        },
      ],
    };
    const alternativen: Regelungsalternative[] = [
      {
        id: '2',
        name: '',
        text: '',
        teilbereiche: [],
        auswirkungenPolitikbereiche: [''],
      },
    ];
    const cleanedBewertung = removeDeletedAlternativesFromBewertung(bewertung, alternativen);
    expect(cleanedBewertung).to.eql(expectedOutput);
  });

  it('Nothing to clean', () => {
    const bewertung: Bewertung = {
      fazit: 'Hello',
      bewertungPolitikbereiche: [],
      bewertungZiele: [],
      bewertungKriterien: [],
    };
    const alternativen: Regelungsalternative[] = [];
    const cleanedBewertung = removeDeletedAlternativesFromBewertung(bewertung, alternativen);
    expect(cleanedBewertung).to.eql(bewertung);
  });
});
