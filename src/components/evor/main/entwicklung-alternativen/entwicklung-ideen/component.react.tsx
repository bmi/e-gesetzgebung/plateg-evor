// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { BASE_PATH, Teilbereich } from '@plateg/rest-api';
import { FormWrapper, InfoComponent, RepeatableFormComponent } from '@plateg/theme';

import { saveDraftContent } from '../../../../../shares/localStorage';
import { ROUTES } from '../../../../../shares/routes';
import { StandardEvorProps } from '../../component.react';
import { getRegelungsalternativenWithCleanTeilbereichReferences } from '../../regelungsalternativen-pruefung/controller';
import { TeilbereichComponent } from './teilbereich-component/component.react';

export interface IdeenValues {
  teilbereichList: Teilbereich[];
}

export function IdeenComponent(props: StandardEvorProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const [form] = Form.useForm();
  const currentDraftId = props.currentDraft.id;
  const regelungsalternativen = props.currentDraft.regelungsalternativen;
  const [isDirty, setIsDirty] = useState(false);

  const setDirtyFlag = () => {
    setIsDirty(true);
  };

  useEffect(() => {
    props.setIsFormDirty?.(false);
  }, []);

  // First call current draft is not yet filled, so we give defaults
  const formInitialValue: IdeenValues = {
    teilbereichList: props.currentDraft.regelungsalternativen?.teilbereiche?.teilbereichList || [
      { id: uuidv4(), name: '', optionen: [{ id: uuidv4(), name: '', text: '' }] },
    ],
  };

  const getRepeatableItem = () => {
    return (form.getFieldsValue(true) as IdeenValues).teilbereichList || formInitialValue.teilbereichList;
  };

  const setRepeatableItem = (teilbereichList: Teilbereich[]) => {
    const currentValues = form.getFieldsValue(true) as IdeenValues;
    currentValues.teilbereichList = teilbereichList;
    form.setFieldsValue(currentValues);
  };

  const title = (
    <div className="heading-holder">
      <span style={{ whiteSpace: 'nowrap' }}>
        <Title level={1}>{t('ideasCollection.title')}</Title>
        <InfoComponent title={t('ideasCollection.drawerTitle')}>
          <p className="ant-typography p-no-style">{t('ideasCollection.drawerContent1')}</p>
          <p className="ant-typography p-no-style">{t('ideasCollection.drawerContent2')}</p>
          <p
            className="ant-typography p-no-style"
            dangerouslySetInnerHTML={{
              __html: t('ideasCollection.drawerContent3', {
                linkGGO: `${BASE_PATH}/arbeitshilfen/download/34#page=67`,
              }),
            }}
          />
        </InfoComponent>
      </span>
    </div>
  );

  const saveDraft = () => {
    const values = form.getFieldsValue(true) as IdeenValues;
    const cleanRegelungsalternativen = getRegelungsalternativenWithCleanTeilbereichReferences(
      values,
      regelungsalternativen,
    );
    saveDraftContent({
      id: currentDraftId,
      regelungsalternativen: {
        ...cleanRegelungsalternativen,
        teilbereiche: { teilbereichList: values.teilbereichList },
      },
    });
  };

  return (
    <div>
      <FormWrapper
        projectName="eVoR"
        title={title}
        previousPage={`/evor/${currentDraftId}/${ROUTES.zielAnalyse}`}
        nextPage={`/evor/${currentDraftId}/${ROUTES.entwicklung.entwicklungAlternativen}`}
        saveDraft={saveDraft}
        isDirty={() => form.isFieldsTouched() || isDirty}
        form={form}
        formInitialValue={formInitialValue}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          props.setIsFormDirty?.(true);
        }}
      >
        <Title level={2}>{t('ideasCollection.subtitle1')}</Title>
        <p
          className="ant-typography p-no-style"
          dangerouslySetInnerHTML={{
            __html: t('ideasCollection.infoText1', {
              basePath: BASE_PATH,
            }),
          }}
        />

        <Title level={2}>{t('ideasCollection.subtitle2')}</Title>
        <p className="ant-typography p-no-style">{t('ideasCollection.infoText2')}</p>
        <RepeatableFormComponent
          getRepeatableItem={getRepeatableItem}
          setRepeatableItem={setRepeatableItem}
          emptyItem={() => {
            return { id: uuidv4(), name: '', optionen: [{ id: uuidv4(), name: '', text: '' }] };
          }}
          setDirtyFlag={setDirtyFlag}
          textAddItem="Teilbereich hinzufügen"
          repeatableComponent={(item: Teilbereich, index: number, deleteItem: (index: number) => void) => (
            <TeilbereichComponent
              key={item.id}
              teilbereich={item}
              teilbereichIndex={index}
              deleteTeilbereich={deleteItem}
              getRepeatableItem={getRepeatableItem}
              setDirtyFlag={setDirtyFlag}
              autoFocus={getRepeatableItem().length > 1 && isDirty}
              isDirty={isDirty}
            />
          )}
        />
      </FormWrapper>
    </div>
  );
}
