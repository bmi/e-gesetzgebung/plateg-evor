// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { DeleteOutlined } from '@plateg/theme/src/components/icons/DeleteOutlined';
interface OptionComponentProps {
  teilbereichIndex: number;
  optionIndex: number;
  deleteOption: (index: number) => void;
  setDirtyFlag: Function;
  autoFocus: boolean;
}

export function OptionComponent(props: OptionComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const { TextArea } = Input;
  const deleteOptionAndSetDirty = (index: number) => {
    props.setDirtyFlag();
    props.deleteOption(index);
  };
  return (
    <>
      <div className="content-box content-box-blue triple-margin-validation">
        <Form.Item
          name={['teilbereichList', props.teilbereichIndex, 'optionen', props.optionIndex, 'name']}
          label={
            <span>{`${t('ideasCollection.subpartOption')} ${(props.teilbereichIndex + 1).toString()}.${(
              props.optionIndex + 1
            ).toString()}`}</span>
          }
          style={{ marginBottom: '16px' }}
          rules={[
            {
              required: true,
              whitespace: true,
              message: t('ideasCollection.validationMessageOption', {
                index: `${props.teilbereichIndex + 1}.${props.optionIndex + 1}`,
              }),
            },
          ]}
        >
          <Input autoFocus={props.autoFocus} required />
        </Form.Item>
        <Form.Item
          name={['teilbereichList', props.teilbereichIndex, 'optionen', props.optionIndex, 'text']}
          label={
            <span>{`${t('ideasCollection.subpartDescription')} ${(props.teilbereichIndex + 1).toString()}.${(
              props.optionIndex + 1
            ).toString()}`}</span>
          }
        >
          <TextArea rows={3} />
        </Form.Item>
      </div>
      <div style={{ overflow: 'hidden', width: '100%' }}>
        {' '}
        <Button
          id="evor-deleteOption-btn"
          size={'small'}
          icon={<DeleteOutlined />}
          type="link"
          style={{ float: 'right' }}
          onClick={() => deleteOptionAndSetDirty(props.optionIndex)}
          className="blue-text-button"
        >
          {`Option ${props.teilbereichIndex + 1}.${props.optionIndex + 1} entfernen`}
        </Button>
      </div>
    </>
  );
}
