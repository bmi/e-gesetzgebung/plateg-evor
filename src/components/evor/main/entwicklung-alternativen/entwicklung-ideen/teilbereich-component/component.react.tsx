// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { Teilbereich, Teilbereichsoption } from '@plateg/rest-api';
import { FormItemWithInfo, InfoComponent, RepeatableFormComponent } from '@plateg/theme';
import { DeleteOutlined } from '@plateg/theme/src/components/icons/DeleteOutlined';

import { OptionComponent } from './option-component/component.react';

interface TeilbereichComponentProps {
  teilbereich: Teilbereich;
  teilbereichIndex: number;
  deleteTeilbereich: (index: number) => void;
  getRepeatableItem: () => Teilbereich[];
  setDirtyFlag: Function;
  autoFocus: boolean;
  isDirty: boolean;
}

export function TeilbereichComponent(props: TeilbereichComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const deleteTeilbereichAndSetDirty = (index: number) => {
    props.setDirtyFlag();
    props.deleteTeilbereich(index);
  };

  const getRepeatableItem = () => {
    return props.getRepeatableItem()[props.teilbereichIndex].optionen || [];
  };

  const setRepeatableItem = (_optionen: Teilbereichsoption[]) => {
    // Not needed as repeatable form will write directly into the Teilbereich Object (no new link)
  };
  return (
    <>
      <section className="content-box content-box-white double-margin-validation">
        <FormItemWithInfo
          name={['teilbereichList', props.teilbereichIndex, 'name']}
          label={
            <span>
              {`${t('ideasCollection.subpartTitle')} ${(props.teilbereichIndex + 1).toString()}`}
              <InfoComponent
                withLabelRequired
                title={t('ideasCollection.drawerSubpartTitle')}
                id={`${t('ideasCollection.drawerSubpartTitle')} ${props.teilbereichIndex}`}
              >
                <p className="ant-typography p-no-style">{t('ideasCollection.drawerSubpartText')}</p>
                <ul>
                  <li>{t('ideasCollection.drawerSubpartListItem1')}</li>
                  <li>{t('ideasCollection.drawerSubpartListItem2')}</li>
                  <li>{t('ideasCollection.drawerSubpartListItem3')}</li>
                  <li>{t('ideasCollection.drawerSubpartListItem4')}</li>
                </ul>
              </InfoComponent>
            </span>
          }
          rules={[
            {
              required: true,
              whitespace: true,
              message: t('ideasCollection.validationMessageTeilbereich', { index: props.teilbereichIndex + 1 }),
            },
          ]}
        >
          <Input required />
        </FormItemWithInfo>
        <RepeatableFormComponent
          getRepeatableItem={getRepeatableItem}
          setRepeatableItem={setRepeatableItem}
          emptyItem={() => {
            return { id: uuidv4(), name: '', text: '' };
          }}
          textAddItem="Option hinzufügen"
          setDirtyFlag={props.setDirtyFlag}
          repeatableComponent={(item: Teilbereichsoption, index: number, deleteItem: (index: number) => void) => (
            <OptionComponent
              key={item.id}
              optionIndex={index}
              teilbereichIndex={props.teilbereichIndex}
              deleteOption={deleteItem}
              setDirtyFlag={props.setDirtyFlag}
              autoFocus={getRepeatableItem().length > 1 && props.isDirty}
            />
          )}
        />
      </section>
      <div style={{ overflow: 'hidden', width: '100%' }}>
        <Button
          id="evor-deleteTeilbereich-btn"
          size={'small'}
          icon={<DeleteOutlined />}
          type="link"
          style={{ float: 'right' }}
          onClick={() => deleteTeilbereichAndSetDirty(props.teilbereichIndex)}
          className="blue-text-button"
        >
          {`Teilbereich ${props.teilbereichIndex + 1} entfernen`}
        </Button>
      </div>
    </>
  );
}
