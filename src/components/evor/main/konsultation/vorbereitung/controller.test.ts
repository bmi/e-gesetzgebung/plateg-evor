// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect, use } from 'chai';
import * as chaiArrays from 'chai-arrays';
import { TFunction } from 'i18next';

import { Konsultationen, KonsultationType, Politikbereich } from '@plateg/rest-api';

import {
  getBewertungWithCleanPolitikbereiche,
  getBewertungWithDisabledPolitikbereiche,
  getKonsultationenWithCleanRessortReferences,
} from './controller';
use(chaiArrays.default);

describe('Test clean bewertung after politikbereiche changed', () => {
  it('Removed unused politikbereiche from bewertungs object', () => {
    const bewertungPolitikbereiche = [
      { politikbereichId: 'a' },
      { politikbereichId: 'a' },
      { politikbereichId: 'b' },
      { politikbereichId: 'c' },
    ];
    const politikbereiche = [{ id: 'x', unterbereiche: [{ id: 'b' }, { id: 'c' }] }];
    const expectedResult = [{ politikbereichId: 'b' }, { politikbereichId: 'c' }];
    const cleanedBewertung = getBewertungWithCleanPolitikbereiche(bewertungPolitikbereiche, politikbereiche);
    expect(cleanedBewertung).to.eql(expectedResult);
  });
});

describe('Test clean bewertung after auswirkungen von regelungsalternativen changed', () => {
  it('Set disabled politikbereiche for bewertung to undefined', () => {
    const regelungsalternativen = [
      { id: '1', auswirkungenPolitikbereiche: ['a'], teilbereiche: [] },
      { id: '2', auswirkungenPolitikbereiche: ['b'], teilbereiche: [] },
    ];
    const bewertungPolitikbereiche = [
      { politikbereichId: 'a', regelungsalternativeId: '1', bewertung: -1 },
      { politikbereichId: 'a', regelungsalternativeId: '2', bewertung: 1 },
      { politikbereichId: 'b', regelungsalternativeId: '2', bewertung: 1 },
      { politikbereichId: 'c', regelungsalternativeId: '1', bewertung: 0 },
    ];
    const expectedResult = [
      { politikbereichId: 'a', regelungsalternativeId: '1', bewertung: -1 },
      { politikbereichId: 'a', regelungsalternativeId: '2', bewertung: undefined },
      { politikbereichId: 'b', regelungsalternativeId: '2', bewertung: 1 },
      { politikbereichId: 'c', regelungsalternativeId: '1', bewertung: undefined },
    ];
    getBewertungWithDisabledPolitikbereiche(bewertungPolitikbereiche, regelungsalternativen);
    expect(bewertungPolitikbereiche).to.eql(expectedResult);
  });
});

describe('Test Konsultationen clean references to Ressort', () => {
  const t: TFunction = (key: string) => {
    return key;
  };
  it('Clean Ressort, Konsultationen contains non existing reference to Ressort', () => {
    const politikbereiche: Politikbereich[] = [
      {
        id: '565656',
        name: 'Test',
        alias: 'Test',
        unterbereiche: [
          { id: '15e93da3-b8a9-4846-9895-616636dd1777', name: 'finanzmarktpolitik', alias: 'finanzmarktpolitik' },
        ],
      },
      {
        id: '565657',
        name: 'Test2',
        alias: 'Test2',
        unterbereiche: [{ id: '15e93da3-b8a9-4846-9895-616636dd1778', name: 'steuern', alias: 'steuern' }],
      },
    ];
    const konsultationen: Konsultationen = {
      weitereKonsultationen: [
        {
          id: '565658',
          name: 'Test3',
          text: 'Test3',
          type: KonsultationType.Ressort,
          autoAdded: true,
        },
      ],
    };
    const cleanedPolitikbereiche = getKonsultationenWithCleanRessortReferences(politikbereiche, konsultationen, t);
    expect(cleanedPolitikbereiche.weitereKonsultationen).to.have.lengthOf(0);
  });

  it('Clean Ressort, Konsultationen contains existing reference to Ressort', () => {
    const politikbereiche: Politikbereich[] = [
      {
        id: '565656',
        name: 'Test',
        alias: 'Test',
        unterbereiche: [
          { id: '15e93da3-b8a9-4846-9895-616636dd1777', name: 'finanzmarktpolitik', alias: 'finanzmarktpolitik' },
        ],
      },
      {
        id: '565657',
        name: 'Test2',
        alias: 'Test2',
        unterbereiche: [{ id: '15e93da3-b8a9-4846-9895-616636dd1778', name: 'steuern', alias: 'steuern' }],
      },
    ];
    const konsultationen: Konsultationen = {
      weitereKonsultationen: [
        {
          id: '565657',
          name: 'Test2',
          text: 'Test2',
          type: KonsultationType.Ressort,
          autoAdded: false,
        },
      ],
    };
    const cleanedPolitikbereiche = getKonsultationenWithCleanRessortReferences(politikbereiche, konsultationen, t);
    expect(cleanedPolitikbereiche.weitereKonsultationen).to.have.lengthOf(1);
  });
});
