// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { TFunction } from 'i18next';
import React from 'react';
import { v4 as uuidv4 } from 'uuid';

import { Politikbereich, Regelungsalternative } from '@plateg/rest-api';
import { CheckOutlined } from '@plateg/theme/src/components/icons/CheckOutlined';

import { getMinisterium, MinisteriumInfo } from './controller';

interface TableContent {
  [key: string]: { [key: string]: string[] };
}
export interface TableRow {
  key: string;
  ressort?: React.ReactElement;
  regelungsentwurf?: string;
  themenbereiche?: React.ReactElement;
}

export function computeTableContent(
  politikbereiche: Politikbereich[],
  selectedAlternativen: Regelungsalternative[],
  t: TFunction,
): TableRow[] {
  const tableContent: TableContent = {};
  politikbereiche.forEach((politikbereich) => {
    const ministerium = getMinisterium(politikbereich.alias, t);
    selectedAlternativen.forEach((alternative) => {
      const alternativeName = alternative.name || '';
      if (alternative.auswirkungenPolitikbereiche.indexOf(politikbereich.id) >= 0) {
        createEntryForAlternative(tableContent, ministerium, alternativeName);
        tableContent[ministerium.abbreviation][alternativeName].push(politikbereich.name || '');
      }
    });
  });
  return createRowsFromTableContent(tableContent);
}

function createEntryForAlternative(
  tableContent: TableContent,
  ministerium: MinisteriumInfo,
  alternativeName: string,
): void {
  if (!(ministerium.abbreviation in tableContent)) {
    tableContent[ministerium.abbreviation] = { name: ministerium.name };
  }
  if (!(alternativeName in tableContent[ministerium.abbreviation])) {
    tableContent[ministerium.abbreviation][alternativeName] = [];
  }
}

function createRowsFromTableContent(tableContent: TableContent): TableRow[] {
  const tableRows: TableRow[] = [];
  for (const [ressortKey, ressortValue] of Object.entries(tableContent)) {
    let firstTimeRessort = true;
    const ressortString = computeRessortElement(ressortKey, ressortValue.name);
    pushEmptyRow(tableRows);
    for (const [alternativeKey, alternativeValue] of Object.entries(ressortValue)) {
      if (alternativeKey === 'name') {
        continue;
      }
      const ressort = firstTimeRessort ? ressortString : <></>;
      pushDataRow(tableRows, ressort, alternativeKey, alternativeValue);
      firstTimeRessort = false;
    }
  }
  pushEmptyRow(tableRows);
  return tableRows;
}

function computeRessortElement(abbreviation: string, name: string): React.ReactElement {
  return (
    <>
      <span>{abbreviation}</span>
      <span style={{ display: 'block' }}>{name}</span>
    </>
  );
}

function computeThemenbereicheElement(alternativeValue: string[]): React.ReactElement {
  return (
    <div style={{ overflow: 'hidden', whiteSpace: 'nowrap' }}>
      {alternativeValue.map((item: string, index: number) => (
        <p className="ant-typography p-no-style" key={index}>
          <CheckOutlined />
          &nbsp;
          {item}
        </p>
      ))}
    </div>
  );
}

function pushEmptyRow(tableRows: TableRow[]): void {
  tableRows.push({ key: uuidv4() + '-empty' });
}

function pushDataRow(
  tableRows: TableRow[],
  ressort: React.ReactElement,
  regelungsentwurf: string,
  alternativeValue: string[],
): void {
  tableRows.push({
    key: uuidv4(),
    ressort,
    regelungsentwurf,
    themenbereiche: computeThemenbereicheElement(alternativeValue),
  });
}
