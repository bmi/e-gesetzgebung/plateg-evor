// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { TFunction } from 'i18next';

import { BewertungPolitikbereich, Konsultationen, Politikbereich, Regelungsalternative } from '@plateg/rest-api';

import { getMinisterien } from '../../../../../data/ministerien';
import { extractPolitikbereiche } from '../../regelungsalternativen-pruefung/bewertung/controller';
export interface MinisteriumInfo {
  abbreviation: string;
  name: string;
}

export function getMinisterium(politikbereichAlias: string | undefined, t: TFunction): MinisteriumInfo {
  if (!politikbereichAlias) {
    return { abbreviation: '', name: '' };
  }
  const ministerienList = getMinisterien(t);
  return ministerienList[politikbereichAlias];
}

export function getKonsultationenWithCleanRessortReferences(
  politikbereiche: Politikbereich[],
  konsultationen: Konsultationen,
  t: TFunction,
): Konsultationen {
  const extractedPolitikbereiche = extractPolitikbereiche(politikbereiche);
  const ressortNamesList = extractedPolitikbereiche.map(
    (politikbereich) => getMinisterium(politikbereich.alias, t).abbreviation,
  );
  const ressortNames = new Set(ressortNamesList);
  konsultationen.weitereKonsultationen.forEach((konsultation, i) => {
    if (konsultation.autoAdded && konsultation?.name && !ressortNames.has(konsultation.name)) {
      konsultationen.weitereKonsultationen.splice(i, 1);
    }
  });
  return konsultationen;
}

/* Entfernt Referenzen zu deselektierten Politikbereichen im Bewertungsfeld
 */
export function getBewertungWithCleanPolitikbereiche(
  bewertungPolitikbereiche: BewertungPolitikbereich[],
  nestedPolitikbereiche: Politikbereich[],
): BewertungPolitikbereich[] {
  const politikbereiche = extractPolitikbereiche(nestedPolitikbereiche).map((item) => item.id);
  return bewertungPolitikbereiche.filter((item) => politikbereiche.includes(item.politikbereichId || ''));
}

/* Setzt alle Bewertungen von den Politikbereichen, die disabled wurden, auf undefined zurück
 */
export function getBewertungWithDisabledPolitikbereiche(
  bewertungPolitikbereiche: BewertungPolitikbereich[],
  regelungsalternativen: Regelungsalternative[],
): void {
  regelungsalternativen.forEach((alternative) => {
    bewertungPolitikbereiche.forEach((bereich, index) => {
      if (
        bereich.regelungsalternativeId === alternative.id &&
        !alternative.auswirkungenPolitikbereiche.includes(bereich.politikbereichId || '')
      ) {
        bewertungPolitikbereiche[index].bewertung = undefined;
      }
    });
  });
}
