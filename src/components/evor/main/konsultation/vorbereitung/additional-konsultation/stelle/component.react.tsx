// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { DeleteOutlined } from '@plateg/theme/src/components/icons/DeleteOutlined';
interface AdditionalStelleProps {
  index: number;
  deleteStelle: (index: number) => void;
  deleteButtonText: string;
  propertyName: string;
  setDirtyFlag: Function;
  autoFocus: boolean;
}

export function AdditionalStellenComponent(props: AdditionalStelleProps): React.ReactElement {
  const { t } = useTranslation();

  const deleteStellenAndSetDirty = (index: number) => {
    props.setDirtyFlag();
    props.deleteStelle(index);
  };
  return (
    <Form.Item>
      <Form.Item
        label={<span>{`${t('konsultation.titleKosultStelle')} ${props.index + 1}`}</span>}
        name={[props.propertyName, props.index, 'name']}
        rules={[{ required: true, message: t('konsultation.validationMessage.stelle', { index: props.index + 1 }) }]}
      >
        <Input autoFocus={props.autoFocus} type="text" required />
      </Form.Item>
      <div style={{ textAlign: 'right', marginTop: '8px' }}>
        <Button
          id="evor-deleteStellen-btn"
          type="link"
          className="blue-text-button"
          icon={<DeleteOutlined />}
          size={'small'}
          onClick={() => deleteStellenAndSetDirty(props.index)}
        >
          {props.deleteButtonText}
        </Button>
      </div>
    </Form.Item>
  );
}
