// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { RessortDTO } from '@plateg/rest-api';
import { CheckOutlined, MainContentSelectWrapper } from '@plateg/theme';
import { DeleteOutlined } from '@plateg/theme/src/components/icons/DeleteOutlined';

interface AdditionalRessortProps {
  index: number;
  deleteRessort: (index: number) => void;
  deleteButtonText: string;
  propertyName: string;
  setDirtyFlag: Function;
  autoFocus: boolean;
  ressorts: RessortDTO[];
}

export function AdditionalRessortComponent(props: AdditionalRessortProps): React.ReactElement {
  const { t } = useTranslation();

  const deleteKonsultationAndSetDirty = (index: number) => {
    props.setDirtyFlag();
    props.deleteRessort(index);
  };
  return (
    <Form.Item>
      <Form.Item
        label={<span>{`${t('konsultation.titleKosultRessort')} ${props.index + 1}`}</span>}
        name={[props.propertyName, props.index, 'name']}
        rules={[{ required: true, message: t('konsultation.validationMessage.ressort', { index: props.index + 1 }) }]}
      >
        <MainContentSelectWrapper
          id="evor-konsultationTitleKosultRessort-select"
          placeholder={t('bewertung.selectDefaultValue')}
          listHeight={256}
          autoFocus={props.autoFocus}
          menuItemSelectedIcon={<CheckOutlined />}
          optionFilterProp="title"
          options={props.ressorts.map((item) => {
            const elementTitle = `${item.bezeichnung} (${item.kurzbezeichnung})`;
            return {
              label: (
                <span title={elementTitle} key={`beteiligteRessorts-${item.bezeichnung}-${item.id.toString()}`}>
                  {elementTitle}
                </span>
              ),
              title: elementTitle,
              value: elementTitle,
            };
          })}
        />
      </Form.Item>
      <div style={{ textAlign: 'right', marginTop: '8px' }}>
        <Button
          id="evor-deleteKonsultation-btn"
          type="link"
          className="blue-text-button"
          icon={<DeleteOutlined />}
          size={'small'}
          onClick={() => deleteKonsultationAndSetDirty(props.index)}
        >
          {props.deleteButtonText}
        </Button>
      </div>
    </Form.Item>
  );
}
