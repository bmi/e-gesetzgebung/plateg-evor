// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './konsultationTable.less';

import { Table } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EvorData, Regelungsalternative } from '@plateg/rest-api';
import { TableScroll } from '@plateg/theme';

import { extractPolitikbereiche } from '../../../regelungsalternativen-pruefung/bewertung/controller';
import { computeTableContent } from '../controller_table';

import type { TableRow } from '../controller_table';
import type { ColumnsType } from 'antd/es/table';

interface KonsultationTableProps {
  currentDraft: EvorData;
  selectedAlternativen: Regelungsalternative[];
}

export function KonsultationTabelle(props: KonsultationTableProps): React.ReactElement {
  const { t } = useTranslation();
  const politikbereicheList = extractPolitikbereiche(props.currentDraft?.politikbereiche?.politikbereichList || []);
  const tableContent: TableRow[] = computeTableContent(politikbereicheList, props.selectedAlternativen, t);

  const columns: ColumnsType<TableRow> = [
    { title: 'Ressort', dataIndex: 'ressort', fixed: true, rowScope: 'row' },
    { title: 'Regelungsentwurf', dataIndex: 'regelungsentwurf' },
    { title: 'Themenbereiche', dataIndex: 'themenbereiche' },
  ];

  return (
    <div>
      {tableContent.length > 1 && (
        <TableScroll>
          <Table
            columns={columns}
            dataSource={tableContent}
            pagination={false}
            rowClassName={(record) => {
              if (record.key.indexOf('-empty') !== -1) {
                return 'empty-row';
              }
              return '';
            }}
            className="konsultation-table"
          />
        </TableScroll>
      )}
      {tableContent.length <= 1 && <p className="ant-typography p-no-style">{t('konsultation.noAutomaticRessorts')}</p>}
    </div>
  );
}
