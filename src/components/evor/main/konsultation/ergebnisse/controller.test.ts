// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { TFunction } from 'i18next';
import { v4 as uuidv4 } from 'uuid';

import { Konsultation, KonsultationType } from '@plateg/rest-api';

import { getMinisterien } from '../../../../../data/ministerien';
import { getKonsultationen } from './controller';
describe('Test Berechnung der Konsultationselemente', () => {
  const t: TFunction = (key: string) => {
    return key;
  };
  it('Keine alten Elemente vorhanden, zwei automatisch generierte', () => {
    const currentDraft = {
      id: 'Test id',
      regelungsalternativen: {
        regelungsalternativeList: [
          { id: '1', weiterverfolgen: true, teilbereichList: [], auswirkungenPolitikbereiche: ['steuern', 'klima'] },
        ],
      },
      konsultationen: { weitereKonsultationen: [] },
    };
    const expectedValue: Konsultation[] = [
      {
        id: uuidv4(),
        name: 'BMF',
        text: '',
        type: KonsultationType.Ressort,
        autoAdded: true,
      },
      {
        id: uuidv4(),
        name: 'BMWK',
        text: '',
        type: KonsultationType.Ressort,
        autoAdded: true,
      },
    ];
    /*we need to stub uuidv4() later to make this work
    should().equal(getKonsultationen(currentDraft, getMinisterien(t)), expectedValue);
    temporary "alternative" ->
    */
    const konsultationen = getKonsultationen(currentDraft, getMinisterien(t));
    expect(konsultationen.length).is.eq(expectedValue.length);
    konsultationen.forEach((obj, index) => {
      expect(obj).to.have.property('name', expectedValue[index].name);
      expect(obj).to.have.property('text', expectedValue[index].text);
      expect(obj).to.have.property('type', expectedValue[index].type);
      expect(obj).to.have.property('autoAdded', expectedValue[index].autoAdded);
    });
  });
});
