// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Konsultationen, KonsultationType } from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { getMinisterien } from '../../../../../data/ministerien';
import { saveDraftContent } from '../../../../../shares/localStorage';
import { ROUTES } from '../../../../../shares/routes';
import { StandardEvorProps } from '../../component.react';
import { getKonsultationen } from './controller';
import { KonsultationsComponent } from './konsultations-component/component.react';

interface ErgebnisseValues {
  konsultationen: Konsultationen;
}

export function ErgebnisseComponent(props: StandardEvorProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const [form] = Form.useForm();
  const { TextArea } = Input;
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    props.setIsFormDirty?.(false);
  }, []);

  const formInitialValue: ErgebnisseValues = {
    konsultationen: {
      zusammenfassung: props.currentDraft?.konsultationen?.zusammenfassung || '',
      weitereKonsultationen: getKonsultationen(props.currentDraft, getMinisterien(t)) || [],
    },
  };

  const saveDraft = () => {
    const values = form.getFieldsValue(true) as ErgebnisseValues;
    const preparedValues = {
      id: props.currentDraft.id,
      konsultationen: values.konsultationen,
    };
    saveDraftContent(preparedValues);
  };

  return (
    <div>
      <FormWrapper
        projectName="eVoR"
        title={
          <div className="heading-holder">
            <Title level={1}>{t('ergebnisse.title')}</Title>
          </div>
        }
        previousPage={`/evor/${props.currentDraft.id}/${ROUTES.konsultation.konsultationVorbereitung}`}
        nextPage={`/evor/${props.currentDraft.id}/${ROUTES.fazit}`}
        saveDraft={saveDraft}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={formInitialValue}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          setIsDirty(true);
          props.setIsFormDirty?.(true);
        }}
      >
        <Title level={2}>{t('ergebnisse.subtitle1')}</Title>
        <p className="ant-typography p-no-style">{t('ergebnisse.infoText1')}</p>
        <Title level={3}>{t('ergebnisse.subsubTitle1')}</Title>
        <KonsultationsComponent konsultationen={formInitialValue.konsultationen} type={KonsultationType.Ressort} />
        <Title level={3}>{t('ergebnisse.subsubTitle2')}</Title>
        <KonsultationsComponent konsultationen={formInitialValue.konsultationen} type={KonsultationType.Stelle} />
        <Title level={2}>{t('ergebnisse.subtitle2')}</Title>
        <p className="ant-typography p-no-style">{t('ergebnisse.infoText2')}</p>
        <Form.Item label={<span>{t('ergebnisse.subsubTitle3')}</span>} name={['konsultationen', 'zusammenfassung']}>
          <TextArea rows={8} />
        </Form.Item>
      </FormWrapper>
    </div>
  );
}
