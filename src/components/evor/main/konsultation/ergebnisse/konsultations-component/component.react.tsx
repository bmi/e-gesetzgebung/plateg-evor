// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { Konsultation, Konsultationen, KonsultationTypeEnum } from '@plateg/rest-api';
interface KonsultationsComponentProps {
  konsultationen?: Konsultationen;
  type: KonsultationTypeEnum;
}
export function KonsultationsComponent(props: KonsultationsComponentProps): React.ReactElement {
  const { TextArea } = Input;
  const { t } = useTranslation();
  const konsultationsStellen =
    props.konsultationen?.weitereKonsultationen.filter((item) => item.type === props.type) || [];
  const componentList = konsultationsStellen.map((item: Konsultation) => {
    const index = props.konsultationen?.weitereKonsultationen.findIndex((item2: Konsultation) => item.id === item2.id);
    const label = <span>{`Ergebnisse der Konsultation mit: ${item.name || ''}`}</span>;
    return (
      <Form.Item key={item.id} label={label} name={['konsultationen', 'weitereKonsultationen', index ?? -1, 'text']}>
        <TextArea rows={5} />
      </Form.Item>
    );
  });
  if (componentList.length > 0) {
    return <div>{componentList}</div>;
  }
  return <p className="ant-typography p-no-style">{t('ergebnisse.noStellenText')}</p>;
}
