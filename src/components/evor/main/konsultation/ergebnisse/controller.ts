// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { v4 as uuidv4 } from 'uuid';

import { EvorData, Konsultation, KonsultationType } from '@plateg/rest-api';

import { MinisterienMap } from '../../../../../data/ministerien';
/**
 * Berechnung der neuen Konsultationsobjekte aus dem Localstorage und den Eingaben des Benutzers
 * @param currentDraft
 * @param ministerienMap
 */
export function getKonsultationen(currentDraft: EvorData, ministerienMap: MinisterienMap): Konsultation[] {
  const currentKonsultationen = currentDraft?.konsultationen?.weitereKonsultationen || [];
  const generatedRessortNames = generateRessortNames(currentDraft, ministerienMap);
  let newKonsultationen = removeDuplicateRessorts(currentKonsultationen, generatedRessortNames);
  addGeneratedRessorts(newKonsultationen, generatedRessortNames);
  newKonsultationen = newKonsultationen.concat(currentKonsultationen.filter((ressort) => !ressort.autoAdded));
  return newKonsultationen;
}

/**
 * Erzeugt eine Liste mit allen Ressortnamen, die automatisch aufgrund der Auswahl des Benutzers (z.b. auf der Konsultation/Vorbereitungsseite)
 * berechnet werden. Manuell gesetzte Ressorts und Stellen werden hier nicht berücksichtigt.
 * @param currentDraft
 * @param ministerienMap
 */
function generateRessortNames(currentDraft: EvorData, ministerienMap: MinisterienMap): string[] {
  let generatedRessortNames: string[] = [];
  currentDraft.regelungsalternativen?.regelungsalternativeList?.forEach((alternative) => {
    if (alternative.weiterverfolgen) {
      alternative.auswirkungenPolitikbereiche.forEach((politikbereich) => {
        const ressort = ministerienMap[politikbereich];
        generatedRessortNames.push(ressort.abbreviation);
      });
    }
  });
  generatedRessortNames = [...new Set(generatedRessortNames)];
  return generatedRessortNames;
}

/**
 * Diese Funktion filtert zum einen doppelte Ressorts aus, also Ressorts, die sowohl automatisch erzeugt wurden als auch schon im Localstorage vorliegen.
 * Desweiteren gibt sie eine Liste mit allen alten automatisch generierten Ressorts zurück, die noch weiterverwendet werden sollen. Wenn z.B. das BMI
 * nicht mehr konsultiert werden soll, dann muss der Eintrag auch aus dem Localstorage entfernt werden.
 * @param oldRessorts
 * @param generatedRessortNames
 */
function removeDuplicateRessorts(oldRessorts: Konsultation[], generatedRessortNames: string[]): Konsultation[] {
  const newRessorts: Konsultation[] = [];
  oldRessorts.forEach((ressort) => {
    const searchIndex = generatedRessortNames.findIndex((name) => ressort.name === name);
    if (searchIndex >= 0) {
      generatedRessortNames.splice(searchIndex, 1);
      newRessorts.push(ressort);
    }
  });
  return newRessorts;
}

/**
 * An dieser Stelle werden aus den Ressortabkürzungen, die automatisch extrahiert wurden, komplette Ressortelemente (mit id, name, text etc) erzeugt.
 * @param newKonsultationen
 * @param generatedRessortNames
 */
function addGeneratedRessorts(newKonsultationen: Konsultation[], generatedRessortNames: string[]): void {
  generatedRessortNames.forEach((name) => {
    newKonsultationen.push({
      id: uuidv4(),
      name,
      text: '',
      type: KonsultationType.Ressort,
      autoAdded: true,
    });
  });
}
