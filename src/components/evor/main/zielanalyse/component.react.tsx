// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { Ziel, Zielanalyse } from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper, InfoComponent, RepeatableFormComponent } from '@plateg/theme';

import { saveDraftContent } from '../../../../shares/localStorage';
import { ROUTES } from '../../../../shares/routes';
import { StandardEvorProps } from '../component.react';
import { getBewertungWithCleanZielReferences } from '../regelungsalternativen-pruefung/bewertung/controller';
import { GoalComponent } from './ziel/component.react';

interface ZielanalyseValues {
  zielanalyse: Zielanalyse;
}

export function ZielanalyseComponent(props: StandardEvorProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const [form] = Form.useForm();
  const { TextArea } = Input;
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    props.setIsFormDirty?.(false);
  }, []);
  // First call current draft is not yet filled, so we give defaults
  const formInitialValue: ZielanalyseValues = {
    zielanalyse: props.currentDraft.zielanalyse || { text: '', ziele: [{ id: uuidv4(), name: '', text: '' }] },
  };

  const getRepeatableItem = () => {
    return (
      (form.getFieldsValue(true) as ZielanalyseValues)?.zielanalyse?.ziele || formInitialValue?.zielanalyse?.ziele || []
    );
  };

  const setRepeatableItem = (ziele: Ziel[]) => {
    const currentValues = form.getFieldsValue(true) as ZielanalyseValues;
    currentValues.zielanalyse.ziele = ziele;
    form.setFieldsValue(currentValues);
  };

  const saveDraft = () => {
    const values = form.getFieldsValue(true) as ZielanalyseValues;
    const preparedValues = {
      id: props.currentDraft.id,
      zielanalyse: {
        text: values.zielanalyse.text,
        ziele: values.zielanalyse.ziele,
      },
      bewertung: getBewertungWithCleanZielReferences(values.zielanalyse, props.currentDraft.bewertung),
    };
    saveDraftContent(preparedValues);
  };

  return (
    <div>
      <FormWrapper
        projectName="eVoR"
        title={
          <div className="heading-holder">
            <Title level={1}>{t('zielanalyse.title')}</Title>
          </div>
        }
        previousPage={`/evor/${props.currentDraft.id}/${ROUTES.analyse}`}
        nextPage={`/evor/${props.currentDraft.id}/${ROUTES.entwicklung.entwicklungIdeen}`}
        saveDraft={saveDraft}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={formInitialValue}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          setIsDirty(true);
          props.setIsFormDirty?.(true);
        }}
      >
        <p className="ant-typography p-no-style">{t('zielanalyse.info')}</p>
        <div className="heading-holder">
          <Title level={2}>{t('zielanalyse.subtitle1')}</Title>
          <InfoComponent title={t('zielanalyse.drawerZielanalyseTitle')}>
            <p className="ant-typography p-no-style">{t('zielanalyse.drawerZielanalyseContent1')}</p>
            <p className="ant-typography p-no-style">{t('zielanalyse.drawerZielanalyseContent2')}</p>
          </InfoComponent>
        </div>
        <FormItemWithInfo
          name={['zielanalyse', 'text']}
          label={
            <span>
              {t('zielanalyse.goalRegelungsvorhaben')}
              <InfoComponent withLabel title={t('zielanalyse.drawerZieleTitle')}>
                <ul>
                  <li>{t('zielanalyse.drawerZieleContent1')}</li>
                  <li>{t('zielanalyse.drawerZieleContent2')}</li>
                  <li>{t('zielanalyse.drawerZieleContent3')}</li>
                  <li>{t('zielanalyse.drawerZieleContent4')}</li>
                  <li>{t('zielanalyse.drawerZieleContent5')}</li>
                </ul>
              </InfoComponent>
            </span>
          }
        >
          <TextArea rows={8} />
        </FormItemWithInfo>
        <div className="heading-holder">
          <Title level={2}>{t('zielanalyse.subtitle2')}</Title>
          <InfoComponent title={t('zielanalyse.drawerZieldefinitionTitle')}>
            <p className="ant-typography p-no-style">{t('zielanalyse.drawerZieldefinitionContent1')}</p>
            <ul>
              <li>{t('zielanalyse.drawerZieldefinitionContent2')}</li>
              <li>{t('zielanalyse.drawerZieldefinitionContent3')}</li>
            </ul>
          </InfoComponent>
        </div>
        <p className="ant-typography p-no-style">{t('zielanalyse.goalDefinitionInfo')}</p>
        <RepeatableFormComponent
          getRepeatableItem={getRepeatableItem}
          setRepeatableItem={setRepeatableItem}
          emptyItem={() => {
            return { id: uuidv4(), name: '', text: '' };
          }}
          textAddItem="Ziel hinzufügen"
          setDirtyFlag={() => setIsDirty(true)}
          repeatableComponent={(item: Ziel, index: number, deleteItem: (index: number) => void) => (
            <GoalComponent
              key={item.id}
              index={index}
              deleteGoal={deleteItem}
              setDirtyFlag={() => setIsDirty(true)}
              autoFocus={getRepeatableItem().length > 1 && isDirty}
            />
          )}
        />
      </FormWrapper>
    </div>
  );
}
