// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form, Input, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { DeleteOutlined } from '@plateg/theme/src/components/icons/DeleteOutlined';
interface GoalComponentProps {
  index: number;
  deleteGoal: (index: number) => void;
  setDirtyFlag: Function;
  autoFocus: boolean;
}

export function GoalComponent(props: GoalComponentProps): React.ReactElement {
  const { TextArea } = Input;
  const { Title } = Typography;
  const { t } = useTranslation();
  const deleteGoalAndSetDirty = (index: number) => {
    props.setDirtyFlag();
    props.deleteGoal(index);
  };

  return (
    <>
      <Title level={4}>{`Ziel ${(props.index + 1).toString()}`}</Title>

      <Form.Item
        name={['zielanalyse', 'ziele', props.index, 'name']}
        label={<span>{`Titel des Ziels ${(props.index + 1).toString()}`}</span>}
        rules={[
          { required: true, whitespace: true, message: t('zielanalyse.validationMessage', { index: props.index + 1 }) },
        ]}
      >
        <Input autoFocus={props.autoFocus} required />
      </Form.Item>
      <Form.Item
        name={['zielanalyse', 'ziele', props.index, 'text']}
        label={<span>{`Kurzbeschreibung des Ziels ${(props.index + 1).toString()}`}</span>}
      >
        <TextArea rows={4} />
      </Form.Item>
      <div style={{ textAlign: 'right', marginTop: '8px' }}>
        <Button
          id="evor-deleteGoal-btn"
          size={'small'}
          icon={<DeleteOutlined />}
          type="text"
          onClick={() => deleteGoalAndSetDirty(props.index)}
          className="blue-text-button"
        >
          {`Ziel ${props.index + 1} entfernen`}
        </Button>
      </div>
    </>
  );
}
