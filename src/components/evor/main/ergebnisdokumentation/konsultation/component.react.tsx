// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React, { Fragment } from 'react';
import { useTranslation } from 'react-i18next';

import { EvorData, KonsultationType } from '@plateg/rest-api';

import { KonsultationTabelle } from '../../konsultation/vorbereitung/konsultation-tabelle/component.react';

export interface ErgebnisdokumentationItemProps {
  currentDraft: EvorData;
}

export function KonsultationErgebnisdokumentationComponent(props: ErgebnisdokumentationItemProps): React.ReactElement {
  const { t } = useTranslation();

  const ressortsList =
    props.currentDraft.konsultationen?.weitereKonsultationen.filter(
      (item) => item.type === KonsultationType.Ressort && !item.autoAdded,
    ) || [];
  const stellenList =
    props.currentDraft.konsultationen?.weitereKonsultationen.filter(
      (item) => item.type === KonsultationType.Stelle && !item.autoAdded,
    ) || [];
  return (
    <>
      <Title level={3}>{t('konsultation.title')}</Title>
      <KonsultationTabelle
        currentDraft={props.currentDraft}
        selectedAlternativen={props.currentDraft.regelungsalternativen?.regelungsalternativeList || []}
      />
      <Title level={4}>{t('ergebnisdokumentation.konsultation.ressort.title')}</Title>
      <p>{t('ergebnisdokumentation.konsultation.ressort.text')}</p>
      <ul>
        {ressortsList.map((ressort) => (
          <li key={ressort.id}>{ressort.name}</li>
        ))}
      </ul>
      <Title level={4}>{t('ergebnisdokumentation.konsultation.stellen.title')}</Title>
      <p>{t('ergebnisdokumentation.konsultation.stellen.text')}</p>
      <ul>
        {stellenList.map((stelle) => (
          <li key={stelle.id}>{stelle.name}</li>
        ))}
      </ul>
      <Title level={4}>{t('ergebnisdokumentation.konsultation.ergebnisseDerKonsultation')}</Title>
      <Title level={4}>{t('ergebnisdokumentation.konsultation.betroffeneRessorts')}</Title>
      {ressortsList.map((ressort) => (
        <>
          <strong className="subtitle">{ressort.name}</strong>
          <p>{ressort.text}</p>
        </>
      ))}
      <Title level={4}>{t('ergebnisdokumentation.konsultation.weitereStellen')}</Title>
      {stellenList.map((stelle) => (
        <Fragment key={stelle.id}>
          <strong className="subtitle">{stelle.name}</strong>
          <p>{stelle.text}</p>
        </Fragment>
      ))}
      <Title level={4}>{t('ergebnisdokumentation.konsultation.zusammenfassung')}</Title>
      {props.currentDraft.konsultationen?.zusammenfassung}
    </>
  );
}
