// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ErgebnisdokumentationItemProps } from '../analyse-regelungsfeld/component.react';
import { PruefungBewertungAuswirkungenErgebnisdokumentationComponent } from './auswirkungen/component.react';
import { BewertungSectionErgebnisdokumentationComponent } from './bewertung-section/component.react';
import { GesamtrangErgebnisdokumentationComponent } from './gesamtrang/component.react';
export function PruefungBewertungErgebnisdokumentationComponent(
  props: ErgebnisdokumentationItemProps,
): React.ReactElement {
  const { t } = useTranslation();
  const regelungsalternativeList = props.currentDraft.regelungsalternativen?.regelungsalternativeList || [];
  const politikbereicheList =
    props.currentDraft.politikbereiche?.politikbereichList
      .map((politikbereich) => politikbereich.unterbereiche)
      .flatMap((unterbereiche) => unterbereiche) || [];
  return (
    <>
      <Title level={3}>{t('bewertung.title')}</Title>
      <PruefungBewertungAuswirkungenErgebnisdokumentationComponent
        regelungsalternativeList={regelungsalternativeList}
        politikbereicheList={politikbereicheList}
      />

      <Title level={4}>{t('ergebnisdokumentation.pruefungBewertung.zielerreichung.title')}</Title>
      <BewertungSectionErgebnisdokumentationComponent
        regelungsalternativeList={regelungsalternativeList}
        sectionMark={t('ergebnisdokumentation.pruefungBewertung.zielerreichung.mark')}
        listItems={props.currentDraft.zielanalyse?.ziele || []}
        listItemsValues={props.currentDraft.bewertung?.bewertungZiele || []}
        valueId={'zielId'}
      />

      <Title level={4}>{t('ergebnisdokumentation.pruefungBewertung.pruefkriterien.title')}</Title>
      <BewertungSectionErgebnisdokumentationComponent
        regelungsalternativeList={regelungsalternativeList}
        sectionMark={t('ergebnisdokumentation.pruefungBewertung.pruefkriterien.mark')}
        listItems={props.currentDraft.pruefkriterien?.pruefkriteriumList || []}
        listItemsValues={props.currentDraft.bewertung?.bewertungKriterien || []}
        valueId={'pruefkriteriumId'}
      />

      <Title level={4}>{t('ergebnisdokumentation.pruefungBewertung.politikbereich.title')}</Title>
      <BewertungSectionErgebnisdokumentationComponent
        regelungsalternativeList={regelungsalternativeList}
        sectionMark={t('ergebnisdokumentation.pruefungBewertung.politikbereich.mark')}
        listItems={politikbereicheList || []}
        listItemsValues={props.currentDraft.bewertung?.bewertungPolitikbereiche || []}
        valueId={'politikbereichId'}
      />

      <Title level={4}>{t('ergebnisdokumentation.pruefungBewertung.gesamtrang')}</Title>
      <GesamtrangErgebnisdokumentationComponent regelungsalternativeList={regelungsalternativeList} />

      <Title level={4}>{t('ergebnisdokumentation.pruefungBewertung.gesamtbewertung')}</Title>
      <p>{props.currentDraft.bewertung?.gesamtbewertung}</p>
    </>
  );
}
