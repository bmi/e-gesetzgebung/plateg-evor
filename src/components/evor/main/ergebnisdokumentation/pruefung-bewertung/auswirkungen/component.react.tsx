// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Table } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { Politikunterbereich, Regelungsalternative } from '@plateg/rest-api';

import type { ColumnsType } from 'antd/es/table';

export interface PruefungBewertungProps {
  regelungsalternativeList: Regelungsalternative[];
  politikbereicheList: Politikunterbereich[];
}

export interface GenericColumnType {
  [k: string]: string;
}

export function PruefungBewertungAuswirkungenErgebnisdokumentationComponent(
  props: PruefungBewertungProps,
): React.ReactElement {
  const { t } = useTranslation();

  let columns: ColumnsType<GenericColumnType> = props.regelungsalternativeList.map((alternative, index) => {
    return { title: alternative.name, dataIndex: `alternative-${index}` };
  });
  columns = [
    {
      title: 'Politikbereich',
      dataIndex: 'politikbereich',

      rowScope: 'row',
    },
    ...columns,
  ];

  const data: GenericColumnType[] = props.politikbereicheList.map((politikbereich) => {
    // Set Politikbereich name for each row
    const row: { [k: string]: string } = {
      key: uuidv4() + '-cell',
      politikbereich: politikbereich.name || '',
    };

    props.regelungsalternativeList.forEach((alternative, index) => {
      const selectedPolitikbereich = alternative.auswirkungenPolitikbereiche.filter(
        (bereich) => bereich === politikbereich.id,
      );
      if (selectedPolitikbereich.length) {
        row[`alternative-${index}`] = 'X';
      }
    });
    return row;
  });

  return (
    <>
      <Title level={4}>{t('ergebnisdokumentation.pruefungBewertung.auswirkungen.title')}</Title>
      <p>{t('ergebnisdokumentation.pruefungBewertung.auswirkungen.text')}</p>
      <Table<GenericColumnType>
        columns={columns}
        dataSource={data}
        pagination={false}
        rowClassName={(record) => {
          if (record.key.indexOf('-title') !== -1) {
            return 'title-row';
          }
          if (record.key.indexOf('-cell') !== -1) {
            return 'cell-row';
          }
          return '';
        }}
      />
    </>
  );
}
