// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Table } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import {
  BewertungKriterium,
  BewertungPolitikbereich,
  BewertungZiel,
  Politikunterbereich,
  Pruefkriterium,
  Regelungsalternative,
  Ziel,
} from '@plateg/rest-api';

import type { ColumnsType } from 'antd/es/table';
import type { GenericColumnType } from '../auswirkungen/component.react';

export interface BewertungSectionProps {
  regelungsalternativeList: Regelungsalternative[];
  sectionMark: string;
  listItems: Ziel[] | Pruefkriterium[] | Politikunterbereich[];
  listItemsValues: BewertungZiel[] & BewertungKriterium[] & BewertungPolitikbereich[];
  valueId: 'zielId' | 'pruefkriteriumId' | 'politikbereichId';
}
type IItemvalue = BewertungZiel & BewertungKriterium & BewertungPolitikbereich;

export function BewertungSectionErgebnisdokumentationComponent(props: BewertungSectionProps): React.ReactElement {
  const { t } = useTranslation();
  const sectionId = props.sectionMark.toLowerCase();

  const getZeilValue = (value?: number) => {
    let valueText = '';
    switch (value) {
      case 0:
        valueText = t('ergebnisdokumentation.pruefungBewertung.zeilValues.neutral');
        break;
      case -1:
        valueText = t('ergebnisdokumentation.pruefungBewertung.zeilValues.negativ');
        break;
      case 1:
        valueText = t('ergebnisdokumentation.pruefungBewertung.zeilValues.positiv');
        break;
      default:
        valueText = t('ergebnisdokumentation.pruefungBewertung.zeilValues.keineAngabe');
    }
    return valueText;
  };

  let columns: ColumnsType<GenericColumnType> = props.regelungsalternativeList.map((alternative, index) => {
    return { title: alternative.name, dataIndex: `alternative-${index}` };
  });
  columns = [{ title: props.sectionMark, dataIndex: sectionId, rowScope: 'row' }, ...columns];

  const data = props.listItems.map((item) => {
    // Set Item name for each row
    const row: { [k: string]: string } = {
      key: uuidv4() + '-cell',
    };
    row[`${sectionId}`] = item.name || '';

    props.regelungsalternativeList.forEach((alternative, index) => {
      const selectedValue = props.listItemsValues
        .filter((item) => item.regelungsalternativeId === alternative.id)
        .filter((value: IItemvalue) => value[props.valueId] === item.id);
      if (selectedValue.length) {
        row[`alternative-${index}`] = getZeilValue(selectedValue[0].bewertung);
      }
    });

    return row;
  });

  return (
    <>
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        rowClassName={(record) => {
          if (record.key.indexOf('-title') !== -1) {
            return 'title-row';
          }
          if (record.key.indexOf('-cell') !== -1) {
            return 'cell-row';
          }
          return '';
        }}
      />
    </>
  );
}
