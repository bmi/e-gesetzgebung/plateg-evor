// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Table } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { Regelungsalternative } from '@plateg/rest-api';

import type { ColumnsType } from 'antd/es/table';
import type { GenericColumnType } from '../auswirkungen/component.react';

export interface GesamtrangErgebnisdokumentationProps {
  regelungsalternativeList: Regelungsalternative[];
}

export function GesamtrangErgebnisdokumentationComponent(
  props: GesamtrangErgebnisdokumentationProps,
): React.ReactElement {
  const { t } = useTranslation();
  const rangObj: { [k: string]: string | number } = {};

  let columns: ColumnsType<GenericColumnType> = props.regelungsalternativeList.map((alternative, index) => {
    rangObj[`alternative-${index}`] = alternative.rang || t('ergebnisdokumentation.pruefungBewertung.gesamtrangKeine');
    return { title: alternative.name, dataIndex: `alternative-${index}` };
  });
  columns = [{ title: '', dataIndex: 'gesamtrang', rowScope: 'row' }, ...columns];

  const data = [
    {
      key: 'gesamtrang-cell',
      gesamtrang: t('ergebnisdokumentation.pruefungBewertung.gesamtrangRangLabel'),
      ...rangObj,
    },
  ];

  return (
    <>
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        rowClassName={(record) => {
          if (record.key.indexOf('-title') !== -1) {
            return 'title-row';
          }
          if (record.key.indexOf('-cell') !== -1) {
            return 'cell-row';
          }
          return '';
        }}
      />
    </>
  );
}
