// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React, { Fragment } from 'react';
import { useTranslation } from 'react-i18next';

import { ErgebnisdokumentationItemProps } from '../analyse-regelungsfeld/component.react';

export function ZielanalyseErgebnisdokumentationComponent(props: ErgebnisdokumentationItemProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <Title level={3}>{t('zielanalyse.title')}</Title>
      <p>{props.currentDraft.zielanalyse?.text}</p>
      <Title level={4}>{t('zielanalyse.subtitle2')}</Title>
      {props.currentDraft.zielanalyse?.ziele.map((ziel, index) => {
        return (
          <Fragment key={ziel.id}>
            <Title level={5}>
              {t('ergebnisdokumentation.zielanalyse.ziel')} {index + 1}: {ziel.name}
            </Title>
            <p>{ziel.text}</p>
          </Fragment>
        );
      })}
    </>
  );
}
