// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EvorData } from '@plateg/rest-api';
export interface ErgebnisdokumentationItemProps {
  currentDraft: EvorData;
}

export function AnalyseErgebnisdokumentationComponent(props: ErgebnisdokumentationItemProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <Title level={3}>{t('analyseRegelungsfeldes.title')}</Title>
      <Title level={4}>{t('analyseRegelungsfeldes.fromProblemanalyseLable')}</Title>
      <p>{props.currentDraft.problemanalyse}</p>
      <Title level={4}>{t('analyseRegelungsfeldes.formSystemanalyseLable')}</Title>
      <p>{props.currentDraft.akteursSystemanalyse}</p>
    </>
  );
}
