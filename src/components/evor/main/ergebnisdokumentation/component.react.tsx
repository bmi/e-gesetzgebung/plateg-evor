// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './ergebnisdokumentation.less';

import { Button } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';

import { LeftOutlined } from '@plateg/theme/src/components/icons/LeftOutlined';

import { ROUTES } from '../../../../shares/routes';
import { StandardEvorProps } from '../component.react';
import { AnalyseErgebnisdokumentationComponent } from './analyse-regelungsfeld/component.react';
import { EntwicklungAlternativenErgebnisdokumentationComponent } from './entwicklung-alternativen/component.react';
import { FazitErgebnisdokumentationComponent } from './fazit/component.react';
import { KonsultationErgebnisdokumentationComponent } from './konsultation/component.react';
import { PruefungBewertungErgebnisdokumentationComponent } from './pruefung-bewertung/component.react';
import { ZielanalyseErgebnisdokumentationComponent } from './zielanalyse/component.react';

export function ErgebnisdokumentationComponent(props: StandardEvorProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const currentDraftId = props.currentDraft.id;

  return (
    <div className="ergebnisdokumentation">
      <div className="heading-holder">
        <Title level={1}>{t('ergebnisdokumentation.title')}</Title>
      </div>
      <Title level={2}>{props.currentDraft.name}</Title>
      {/* Analyse des Regelungsfeldes */}
      <AnalyseErgebnisdokumentationComponent currentDraft={props.currentDraft} />
      {/* Zielanalyse */}
      <ZielanalyseErgebnisdokumentationComponent currentDraft={props.currentDraft} />
      {/* Entwicklung Alternativen */}
      <EntwicklungAlternativenErgebnisdokumentationComponent currentDraft={props.currentDraft} />
      {/* Prüfung und Bewertung von Regelungsalternativen */}
      <PruefungBewertungErgebnisdokumentationComponent currentDraft={props.currentDraft} />
      {/* Konsultation */}
      <KonsultationErgebnisdokumentationComponent currentDraft={props.currentDraft} />
      {/* Fazit */}
      <FazitErgebnisdokumentationComponent currentDraft={props.currentDraft} />
      <div className="form-control-buttons">
        <Button
          id="evor-currentDraftFazit-btn"
          type="text"
          className="btn-prev"
          onClick={() => history.push(`/evor/${currentDraftId}/${ROUTES.fazit}`)}
        >
          <LeftOutlined /> {t('form.btnPrev')}
        </Button>
        <Link className="home-text-link" to="/evor">
          {t('ergebnisdokumentation.zurStartseite')}
        </Link>
      </div>
    </div>
  );
}
