// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Table } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { ErgebnisdokumentationItemProps } from '../analyse-regelungsfeld/component.react';

import type { ColumnsType } from 'antd/es/table';
import type { GenericColumnType } from '../pruefung-bewertung/auswirkungen/component.react';

export function EntwicklungAlternativenErgebnisdokumentationComponent(
  props: ErgebnisdokumentationItemProps,
): React.ReactElement {
  const { t } = useTranslation();
  const regelungsalternativeList = props.currentDraft.regelungsalternativen?.regelungsalternativeList || [];
  const teilbereichList = props.currentDraft.regelungsalternativen?.teilbereiche?.teilbereichList || [];

  let columns: ColumnsType<GenericColumnType> = regelungsalternativeList.map((alternative, index) => {
    return { title: alternative.name, dataIndex: `alternative-${index}` };
  });
  columns = [{ title: 'Teilbereiche', dataIndex: 'teilbereiche', rowScope: 'row' }, ...columns];

  const data = teilbereichList.map((teilbereich) => {
    const selectedTeilbereichId = teilbereich.id;

    // Set Teilbereich name for each row
    const row: { [k: string]: string } = {
      key: uuidv4() + '-cell',
      teilbereiche: teilbereich.name || '',
    };

    regelungsalternativeList.forEach((alternative, index) => {
      const selectedTeilbereich = alternative.teilbereichList.filter(
        (teilbereich) => teilbereich.teilbereichId === selectedTeilbereichId,
      );
      const selectedOptionId = selectedTeilbereich[0].teilbereichsoptionId;
      if (selectedOptionId) {
        const teilbereichOptionName =
          teilbereich.optionen.filter((option) => option.id === selectedOptionId)[0]?.name || '';
        // Set selected teilbereich option to each regelungsalternative
        row[`alternative-${index}`] = teilbereichOptionName;
      }
    });
    return row;
  });

  return (
    <>
      <Title level={3}>{t('ideasCollection.title')}</Title>
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        rowClassName={(record) => {
          if (record.key.indexOf('-title') !== -1) {
            return 'title-row';
          }
          if (record.key.indexOf('-cell') !== -1) {
            return 'cell-row';
          }
          return '';
        }}
      />
    </>
  );
}
