// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EvorData } from '@plateg/rest-api';
export interface ErgebnisdokumentationItemProps {
  currentDraft: EvorData;
}

export function FazitErgebnisdokumentationComponent(props: ErgebnisdokumentationItemProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <Title level={3}>{t('fazit.title')}</Title>
      <Title level={4}>{t('ergebnisdokumentation.fazit.eigeneBewertung')}</Title>
      <p>{props.currentDraft.bewertung?.gesamtbewertung}</p>
      <Title level={4}>{t('ergebnisdokumentation.fazit.konsultation')}</Title>
      <p>{props.currentDraft.konsultationen?.zusammenfassung}</p>
      <Title level={4}>{t('ergebnisdokumentation.fazit.gesamtfazit')}</Title>
      <p>{props.currentDraft.bewertung?.fazit}</p>
      <Title level={4}>{t('ergebnisdokumentation.fazit.entscheidung')}</Title>
      <ul>
        {props.currentDraft.regelungsalternativen?.regelungsalternativeList
          .filter((alternative) => alternative.entschieden)
          .map((item) => <li>{item.name}</li>)}
      </ul>
    </>
  );
}
