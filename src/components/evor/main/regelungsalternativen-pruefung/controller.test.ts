// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect, should, use } from 'chai';
import * as chaiArrays from 'chai-arrays';

import { Politikbereich, Regelungsalternativen } from '@plateg/rest-api';

import { IdeenValues } from '../entwicklung-alternativen/entwicklung-ideen/component.react';
import {
  getActivatedCategories,
  getRegelungsalternativenWithCleanOptionReferences,
  getRegelungsalternativenWithCleanPolitikbereicheReferences,
  getRegelungsalternativenWithCleanTeilbereichReferences,
  getSelectedPolitikbereichen,
  prepareInfoText,
  prepareInitialFormPolitikbereichenValues,
  prepareResonseData,
} from './controller';
use(chaiArrays.default);

describe('Test prepareInfoText', () => {
  it("return 3 info texts for 'finanzmarktpolitik'", () => {
    expect(prepareInfoText('finance.finanzmarktpolitik').length).to.eql(3);
  });
  it("return 2 info texts for 'versicherungenundfinanzen'", () => {
    expect(prepareInfoText('finance.versicherungenundfinanzen').length).to.eql(2);
  });
  it("return 0 info texts for non existing messageKey 'blabla'", () => {
    expect(prepareInfoText('blabla').length).to.eql(0);
  });
});

describe('Test prepareInitialFormPolitikbereichenValues', () => {
  it('return correct structure, contains unterbereiche', () => {
    const data = [
      {
        id: 'finance',
        name: 'Finanzen, Steuern und Zölle',
        alias: 'finance',
        unterbereiche: [
          {
            id: 'finanzmarktpolitik',
            name: 'Finanzmarktpolitik',
            alias: 'finanzmarktpolitik',
          },
          {
            id: 'versicherungenundfinanzen',
            name: 'Versicherungen und Finanzen',
            alias: 'versicherungenundfinanzen',
          },
        ],
      },
      {
        id: 'innereskategorie',
        name: 'Inneres',
        alias: 'innereskategorie',
        unterbereiche: [
          {
            id: 'statistik',
            name: 'Statistik',
            alias: 'statistik',
          },
        ],
      },
    ];
    const result = prepareInitialFormPolitikbereichenValues(data);
    expect(result.finance[0]).to.eql('finanzmarktpolitik');
    expect(result.finance[1]).to.eql('versicherungenundfinanzen');
    expect(result.innereskategorie[0]).to.eql('statistik');
  });
  it('return correct structure, no unterbereiche', () => {
    const data = [
      {
        id: 'finance',
        name: 'Finanzen, Steuern und Zölle',
        alias: 'finance',
        unterbereiche: [],
      },
      {
        id: 'innereskategorie',
        name: 'Inneres',
        alias: 'innereskategorie',
        unterbereiche: [],
      },
    ];
    const result = prepareInitialFormPolitikbereichenValues(data);
    expect(result.finance[0]).to.eql('finance');
    expect(result.innereskategorie[0]).to.eql('innereskategorie');
  });
});

describe('Test getActivatedCategories', () => {
  it('return correct category aliases', () => {
    const data = { test0: ['0,1'], test1: ['2,3,4'] };
    const result = getActivatedCategories(data);
    expect(result[0].alias).to.eql('test0');
    expect(result[1].alias).to.eql('test1');
    expect(result[0].unterbereiche[0]).to.eql('0,1');
    expect(result[1].unterbereiche[0]).to.eql('2,3,4');
  });
  it('return empty array, no values provided', () => {
    const data = {};
    const result = getActivatedCategories(data);
    expect(result.length).to.eql(0);
  });
});

describe('Test prepareResonseData', () => {
  const initialStructure = [
    {
      id: 'finance',
      name: 'Finanzen, Steuern und Zölle',
      alias: 'finance',
      unterbereiche: [
        {
          id: 'finanzmarktpolitik',
          name: 'Finanzmarktpolitik',
          alias: 'finanzmarktpolitik',
        },
        {
          id: 'versicherungenundfinanzen',
          name: 'Versicherungen und Finanzen',
          alias: 'versicherungenundfinanzen',
        },
      ],
    },
  ];
  const activatedCategories = [{ alias: 'finance', unterbereiche: ['finanzmarktpolitik'] }];

  it('return empty array, empty arrays passed in', () => {
    expect(prepareResonseData([], []).length).to.eql(0);
  });

  it('return empty array, no activated category', () => {
    const response = prepareResonseData([], initialStructure);
    expect(response.length).to.eql(0);
  });
  it('return correct politikbereich, one activated category', () => {
    const response = prepareResonseData(activatedCategories, initialStructure);
    expect(response.length).to.eql(1);
    expect(response[0].alias).to.eql('finance');
    expect(response[0].id).to.eql('finance');
    expect(response[0].name).to.eql('Finanzen, Steuern und Zölle');
    expect(response[0].unterbereiche[0].alias).to.eql('finanzmarktpolitik');
    expect(response[0].unterbereiche[0].id).to.eql('finanzmarktpolitik');
    expect(response[0].unterbereiche[0].name).to.eql('Finanzmarktpolitik');
  });
});

describe('Test getSelectedPolitikbereichen', () => {
  const politikbereichenList = [
    {
      id: 'finance',
      name: 'Finanzen, Steuern und Zölle',
      alias: 'finance',
      unterbereiche: [
        {
          id: 'finanzmarktpolitik',
          name: 'Finanzmarktpolitik',
          alias: 'finanzmarktpolitik',
        },
        {
          id: 'versicherungenundfinanzen',
          name: 'Versicherungen und Finanzen',
          alias: 'versicherungenundfinanzen',
        },
      ],
    },
  ];

  it('return empty array, empty arrays passed in', () => {
    expect(getSelectedPolitikbereichen([]).length).to.eql(0);
  });
  it('return correct categories, two activated categories', () => {
    const response = getSelectedPolitikbereichen(politikbereichenList);
    expect(response.length).to.eql(2);
    expect(response[0].alias).to.eql('finance.finanzmarktpolitik');
    expect(response[0].id).to.eql('finanzmarktpolitik');
    expect(response[0].name).to.eql('Finanzmarktpolitik');
    expect(response[1].alias).to.eql('finance.versicherungenundfinanzen');
    expect(response[1].id).to.eql('versicherungenundfinanzen');
    expect(response[1].name).to.eql('Versicherungen und Finanzen');
  });
});

describe('Test Regelungsentwurf clean references to Teilbereichsoptionen', () => {
  it('Clean Teilbereichsoptionen, Regelungsalternative contains non existing reference to teilbereichsoption', () => {
    const ideenValues: IdeenValues = {
      teilbereichList: [
        {
          id: '565656',
          name: 'Test',
          optionen: [{ id: '1', name: 'Option1', text: '' }],
        },
        {
          id: '565653',
          name: 'Test2',
          optionen: [{ id: '2', name: 'Option2', text: '' }],
        },
      ],
    };
    const regAlts: Regelungsalternativen = {
      regelungsalternativeList: [
        {
          id: '123456',
          name: '',
          text: '',
          teilbereichList: [{ teilbereichsoptionId: '3', teilbereichId: '565653' }],
          auswirkungenPolitikbereiche: [''],
        },
      ],
    };
    const cleanedRegAlts = getRegelungsalternativenWithCleanOptionReferences(ideenValues, regAlts);
    should().equal(cleanedRegAlts.regelungsalternativeList[0].teilbereichList[0].teilbereichsoptionId, undefined);
  });

  it('Clean Teilbereichsoptionen, Regelungsalternative contains existing reference to teilbereichsoption', () => {
    const ideenValues: IdeenValues = {
      teilbereichList: [
        {
          id: '565656',
          name: 'Test',
          optionen: [{ id: '1', name: 'Option1', text: '' }],
        },
        {
          id: '565653',
          name: 'Test2',
          optionen: [{ id: '2', name: 'Option2', text: '' }],
        },
      ],
    };
    const regAlts: Regelungsalternativen = {
      regelungsalternativeList: [
        {
          id: '123456',
          name: '',
          text: '',
          teilbereichList: [{ teilbereichsoptionId: '2', teilbereichId: '565653' }],
          auswirkungenPolitikbereiche: [''],
        },
      ],
    };
    const cleanedRegAlts = getRegelungsalternativenWithCleanOptionReferences(ideenValues, regAlts);
    expect(cleanedRegAlts.regelungsalternativeList[0].teilbereichList[0].teilbereichsoptionId).equals('2');
  });
});

describe('Test Regelungsentwurf clean references to Politikunterbereiche', () => {
  it('Clean Politikunterbereiche, Regelungsalternative contains non existing reference to Politikunterbereich', () => {
    const poltikbereiche: Politikbereich[] = [
      {
        id: 'testbereich',
        unterbereiche: [{ id: 'testunterbereich1', alias: 'testunterbereich1', name: 'testunterbereich1' }],
      },
    ];
    const regAlts: Regelungsalternativen = {
      regelungsalternativeList: [
        {
          id: '123456',
          name: '',
          text: '',
          teilbereichList: [],
          auswirkungenPolitikbereiche: ['testunterbereich'],
        },
      ],
    };
    const cleanedRegAlts = getRegelungsalternativenWithCleanPolitikbereicheReferences(poltikbereiche, regAlts);
    expect(cleanedRegAlts.regelungsalternativeList[0].auswirkungenPolitikbereiche).to.have.lengthOf(0);
  });

  it('Clean Politikunterbereiche, Regelungsalternative contains existing reference to Politikunterbereich', () => {
    const poltikbereiche: Politikbereich[] = [
      {
        id: 'testbereich',
        unterbereiche: [{ id: 'testunterbereich1', alias: 'testunterbereich1', name: 'testunterbereich1' }],
      },
    ];
    const regAlts: Regelungsalternativen = {
      regelungsalternativeList: [
        {
          id: '123456',
          name: '',
          text: '',
          teilbereichList: [],
          auswirkungenPolitikbereiche: ['testunterbereich1'],
        },
      ],
    };
    const cleanedRegAlts = getRegelungsalternativenWithCleanPolitikbereicheReferences(poltikbereiche, regAlts);
    expect(cleanedRegAlts.regelungsalternativeList[0].auswirkungenPolitikbereiche).to.have.lengthOf(1);
  });
});

describe('Test Regelungsentwurf clean references to Teilbereiche', () => {
  it('Clean Teilbereiche, Regelungsalternative contains non existing reference to Teilbereich', () => {
    const ideenValues: IdeenValues = {
      teilbereichList: [
        {
          id: '565656',
          name: 'Test',
          optionen: [{ id: '1', name: 'Option1', text: '' }],
        },
        {
          id: '565653',
          name: 'Test2',
          optionen: [{ id: '2', name: 'Option2', text: '' }],
        },
      ],
    };
    const regAlts: Regelungsalternativen = {
      regelungsalternativeList: [
        {
          id: '123454',
          name: '',
          text: '',
          teilbereichList: [{ teilbereichsoptionId: '3', teilbereichId: '565654' }],
          auswirkungenPolitikbereiche: [''],
        },
      ],
    };
    const cleanedRegAlts = getRegelungsalternativenWithCleanTeilbereichReferences(ideenValues, regAlts);
    should().equal(cleanedRegAlts.regelungsalternativeList[0].teilbereichList[0], undefined);
  });

  it('Clean Teilbereiche, Regelungsalternative contains existing reference to Teilbereich', () => {
    const ideenValues: IdeenValues = {
      teilbereichList: [
        {
          id: '565656',
          name: 'Test',
          optionen: [{ id: '1', name: 'Option1', text: '' }],
        },
        {
          id: '565653',
          name: 'Test2',
          optionen: [{ id: '2', name: 'Option2', text: '' }],
        },
      ],
    };
    const regAlts: Regelungsalternativen = {
      regelungsalternativeList: [
        {
          id: '123453',
          name: '',
          text: '',
          teilbereichList: [{ teilbereichsoptionId: '3', teilbereichId: '565653' }],
          auswirkungenPolitikbereiche: [''],
        },
      ],
    };
    const cleanedRegAlts = getRegelungsalternativenWithCleanTeilbereichReferences(ideenValues, regAlts);
    expect(cleanedRegAlts.regelungsalternativeList[0].teilbereichList[0].teilbereichId).equals('565653');
  });
});
