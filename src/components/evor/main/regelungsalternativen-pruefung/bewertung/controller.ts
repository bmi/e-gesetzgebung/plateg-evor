// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Bewertung, Politikbereich, Politikunterbereich, Pruefkriterien, Zielanalyse } from '@plateg/rest-api';
export function extractPolitikbereiche(politikbereiche: Politikbereich[]): Politikbereich[] {
  let selectedPolitikbereiche: Politikbereich[] = [];
  politikbereiche.forEach((item: Politikbereich) => {
    if (item.unterbereiche.length !== 0) {
      selectedPolitikbereiche = selectedPolitikbereiche.concat(extractPolitikunterbereiche(item.unterbereiche));
    } else {
      selectedPolitikbereiche.push({ id: item.id, name: item.name, alias: item.alias, unterbereiche: [] });
    }
  });
  return selectedPolitikbereiche;
}

function extractPolitikunterbereiche(politikunterbereiche: Politikunterbereich[]): Politikbereich[] {
  const selectedPolitikbereiche: Politikbereich[] = [];
  politikunterbereiche.forEach((item: Politikunterbereich) => {
    selectedPolitikbereiche.push({ id: item.id, alias: item.alias, name: item.name, unterbereiche: [] });
  });
  return selectedPolitikbereiche;
}

export function getBewertungWithCleanZielReferences(zielanalyse: Zielanalyse, bewertung: Bewertung): Bewertung {
  if (bewertung) {
    const ids = zielanalyse.ziele.map((ziel) => ziel.id);
    bewertung.bewertungZiele = bewertung.bewertungZiele?.filter(function (el) {
      return ids.includes(el.zielId);
    });
  }
  return bewertung;
}

export function getBewertungWithCleanKriterienReferences(
  pruefkriterien: Pruefkriterien,
  bewertung: Bewertung,
): Bewertung {
  if (bewertung) {
    const ids = pruefkriterien?.pruefkriteriumList?.map((kriterium) => kriterium.id);
    bewertung.bewertungKriterien = bewertung.bewertungKriterien?.filter(function (el) {
      return ids.includes(el.pruefkriteriumId);
    });
  }
  return bewertung;
}
