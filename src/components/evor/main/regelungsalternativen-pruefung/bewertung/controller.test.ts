// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { Bewertung, Pruefkriterien, Zielanalyse } from '@plateg/rest-api';

import {
  extractPolitikbereiche,
  getBewertungWithCleanKriterienReferences,
  getBewertungWithCleanZielReferences,
} from './controller';
describe('Extract list with Politikbereiche', () => {
  it('Empty Input', () => {
    expect(extractPolitikbereiche([])).to.eql([]);
  });
  it('No nested structure', () => {
    expect(
      extractPolitikbereiche([
        {
          id: '7',
          name: 'Test1',
          alias: undefined,
          unterbereiche: [],
        },
        {
          id: '8',
          name: 'Test2',
          unterbereiche: [],
        },
      ]),
    ).to.eql([
      {
        id: '7',
        name: 'Test1',
        alias: undefined,
        unterbereiche: [],
      },
      {
        id: '8',
        name: 'Test2',
        alias: undefined,
        unterbereiche: [],
      },
    ]);
  });
  it('Nested structure with Unterbereiche', () => {
    expect(
      extractPolitikbereiche([
        {
          id: '1',
          name: 'unused',
          alias: undefined,
          unterbereiche: [
            {
              id: '2',
              name: 'Test1',
              alias: undefined,
            },
            {
              id: '3',
              name: 'Test2',
              alias: undefined,
            },
          ],
        },
        {
          id: '7',
          name: 'Test3',
          alias: undefined,
          unterbereiche: [],
        },
      ]),
    ).to.eql([
      {
        id: '2',
        name: 'Test1',
        alias: undefined,
        unterbereiche: [],
      },
      {
        id: '3',
        name: 'Test2',
        alias: undefined,
        unterbereiche: [],
      },
      {
        id: '7',
        name: 'Test3',
        alias: undefined,
        unterbereiche: [],
      },
    ]);
  });
});

describe('Test Bewertung clean references to Ziele', () => {
  it('Clean Ziele reference, Bewertung contains non existing reference to Ziel', () => {
    const zielanalyse: Zielanalyse = {
      text: '',
      ziele: [
        {
          id: '565656',
          name: 'Test',
          text: '',
        },
        {
          id: '565653',
          name: 'Test2',
          text: '',
        },
      ],
    };
    const bewertung: Bewertung = {
      bewertungPolitikbereiche: [],
      bewertungKriterien: [],
      bewertungZiele: [
        {
          bewertung: 0,
          regelungsalternativeId: '123',
          zielId: '565654',
        },
      ],
    };
    const cleanedBewertung = getBewertungWithCleanZielReferences(zielanalyse, bewertung);
    expect(cleanedBewertung.bewertungZiele).to.have.lengthOf(0);
  });

  it('Clean Ziele reference, Bewertung contains existing reference to Ziel', () => {
    const zielanalyse: Zielanalyse = {
      text: '',
      ziele: [
        {
          id: '565656',
          name: 'Test',
          text: '',
        },
        {
          id: '565653',
          name: 'Test2',
          text: '',
        },
      ],
    };
    const bewertung: Bewertung = {
      bewertungPolitikbereiche: [],
      bewertungKriterien: [],
      bewertungZiele: [
        {
          bewertung: 0,
          regelungsalternativeId: '123',
          zielId: '565653',
        },
      ],
    };
    const cleanedBewertung = getBewertungWithCleanZielReferences(zielanalyse, bewertung);
    expect(cleanedBewertung.bewertungZiele).to.have.lengthOf(1);
  });
});

describe('Test Bewertung clean references to Kriterien', () => {
  it('Clean Kriterien reference, Bewertung contains non existing reference to Kriterien', () => {
    const pruefkriterien: Pruefkriterien = {
      pruefkriteriumList: [
        {
          id: '20fe1b0d-914e-4c7c-b873-045f02994e9',
          name: 'Test',
        },
        {
          id: '20fe1b0d-914e-4c7c-b873-045f02994e10',
          name: 'Test2',
        },
      ],
    };
    const bewertung: Bewertung = {
      bewertungPolitikbereiche: [],
      bewertungKriterien: [
        {
          pruefkriteriumId: '20fe1b0d-914e-4c7c-b873-045f02994e12',
          regelungsalternativeId: '2',
        },
        {
          pruefkriteriumId: '20fe1b0d-914e-4c7c-b873-045f02994e11',
          regelungsalternativeId: '2',
        },
      ],
      bewertungZiele: [],
    };
    const cleanedBewertung = getBewertungWithCleanKriterienReferences(pruefkriterien, bewertung);
    expect(cleanedBewertung.bewertungKriterien).to.have.lengthOf(0);
  });

  it('Clean Kriterien reference, Bewertung contains existing reference to Kriterien', () => {
    const pruefkriterien: Pruefkriterien = {
      pruefkriteriumList: [
        {
          id: '20fe1b0d-914e-4c7c-b873-045f02994e9',
          name: 'Test',
        },
        {
          id: '1',
          name: 'Test2',
        },
      ],
    };
    const bewertung: Bewertung = {
      bewertungPolitikbereiche: [],
      bewertungKriterien: [
        {
          pruefkriteriumId: '1',
          regelungsalternativeId: '3',
        },
        {
          pruefkriteriumId: '20fe1b0d-914e-4c7c-b873-045f02994e11',
          regelungsalternativeId: '3',
        },
      ],
      bewertungZiele: [],
    };
    const cleanedBewertung = getBewertungWithCleanKriterienReferences(pruefkriterien, bewertung);
    expect(cleanedBewertung.bewertungKriterien).to.have.lengthOf(1);
  });
});
