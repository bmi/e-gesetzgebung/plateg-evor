// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form/hooks/useForm';
import React from 'react';
import { v4 as uuidv4 } from 'uuid';

import { BewertungZiel, EvorData, Regelungsalternative } from '@plateg/rest-api';

import { BewertungValues } from '../../component.react';
import { Datarow } from '../component.react';
import { TableRadioGroup } from '../table-radio-group/component.react';
export function generateZieleRows(
  currentDraft: EvorData,
  selectedAlternativen: Regelungsalternative[],
  form: FormInstance,
): Datarow[] {
  return (currentDraft?.zielanalyse?.ziele || []).map((item) => {
    return {
      key: uuidv4() + '-cell',
      description: item.name || '',
      ...generateRadioGroupEntriesForZiele(item.id, selectedAlternativen, form, item.name),
    };
  });
}

function generateRadioGroupEntriesForZiele(
  elementId: string,
  selectedAlternativen: Regelungsalternative[],
  form: FormInstance,
  itemName: string,
): { [key: string]: React.ReactElement } {
  const radioGroupEntries: { [key: string]: React.ReactElement } = {};
  selectedAlternativen.forEach((alternative: Regelungsalternative) => {
    const index = findBewertungZielIndex(alternative.id, elementId, form);
    const initialValue: number | undefined = (form.getFieldsValue(true) as EvorData).bewertung?.bewertungZiele[index]
      ?.bewertung;
    if (alternative.name) {
      radioGroupEntries[alternative.name] = (
        <fieldset>
          <legend>{itemName}</legend>
          <TableRadioGroup
            formItemName={['bewertung', 'bewertungZiele', index, 'bewertung']}
            initialValue={initialValue}
          />
        </fieldset>
      );
    }
  });
  return radioGroupEntries;
}

function findBewertungZielIndex(alternativenId: string, elementId: string, form: FormInstance): number {
  let bewertungsIndex = -1;
  const currentValues = form.getFieldsValue(true) as BewertungValues;
  currentValues?.bewertung?.bewertungZiele?.forEach((item: BewertungZiel, index: number) => {
    if (item?.regelungsalternativeId === alternativenId && item?.zielId === elementId) {
      bewertungsIndex = index;
    }
  });
  if (bewertungsIndex === -1) {
    const count =
      currentValues?.bewertung?.bewertungZiele?.push({
        regelungsalternativeId: alternativenId,
        zielId: elementId,
        bewertung: undefined,
      }) || 0;
    bewertungsIndex = count - 1;
    form.setFieldsValue({ bewertung: currentValues.bewertung });
  }
  return bewertungsIndex;
}
