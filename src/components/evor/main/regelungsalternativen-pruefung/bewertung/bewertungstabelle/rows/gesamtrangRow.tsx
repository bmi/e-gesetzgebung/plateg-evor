// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import { DefaultOptionType } from 'antd/lib/select';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import { Regelungsalternative } from '@plateg/rest-api';
import { MainContentSelectWrapper } from '@plateg/theme';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

import { Datarow } from '../component.react';

export function GenerateGesamtrangRow(selectedAlternativen: Regelungsalternative[]): Datarow {
  const { t } = useTranslation();
  const options: DefaultOptionType[] = selectedAlternativen.map((_item, index) => {
    return {
      value: index + 1,
      label: <span key={`${_item.id}`}>{index + 1}</span>,
      title: index + 1,
    };
  });
  options.push({
    value: 0,
    label: (
      <span aria-label={t('ergebnisdokumentation.pruefungBewertung.gesamtrangKeine')}>
        {t('ergebnisdokumentation.pruefungBewertung.gesamtrangKeine')}
      </span>
    ),
    title: t('ergebnisdokumentation.pruefungBewertung.gesamtrangKeine').toString(),
  });

  const gesamtrangRow: Datarow & { [key: string]: string | ReactElement } = {
    key: 'gesamtrangRow',
    description: t('bewertung.tableInfoText2'),
  };

  selectedAlternativen.forEach((item: Regelungsalternative) => {
    if (item.name) {
      gesamtrangRow[item.name] = (
        <div>
          <Form.Item
            name={[
              'regelungsalternativen',
              'regelungsalternativeList',
              selectedAlternativen.findIndex((alternative) => item.id === alternative.id),
              'rang',
            ]}
            label={<span>{t('bewertung.rankLabel')}</span>}
          >
            <MainContentSelectWrapper
              id="evor-regelungsalternativenBewertung-select"
              suffixIcon={<SelectDown />}
              defaultValue={t('bewertung.selectDefaultValue').toString()}
              options={options}
            />
          </Form.Item>
        </div>
      );
    }
  });
  return gesamtrangRow;
}
