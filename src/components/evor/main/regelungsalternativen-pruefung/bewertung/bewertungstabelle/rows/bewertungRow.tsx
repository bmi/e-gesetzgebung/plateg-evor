// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { Regelungsalternative } from '@plateg/rest-api';

import { Datarow } from '../component.react';
export function GenerateBewertungRow(selectedAlternativen: Regelungsalternative[]): Datarow {
  const { TextArea } = Input;
  const { t } = useTranslation();

  const bewertungRow = { key: 'bewertungRow', description: t('bewertung.tableInfoText1') };
  selectedAlternativen.forEach((item: Regelungsalternative) => {
    bewertungRow[item.name] = (
      <div>
        <Form.Item
          name={[
            'regelungsalternativen',
            'regelungsalternativeList',
            selectedAlternativen.findIndex((alternative) => item.id === alternative.id),
            'bewertungQualitativ',
          ]}
          label={<span>{t('bewertung.bewertungLabel')}</span>}
        >
          <TextArea rows={5} />
        </Form.Item>
      </div>
    );
  });
  return bewertungRow;
}
