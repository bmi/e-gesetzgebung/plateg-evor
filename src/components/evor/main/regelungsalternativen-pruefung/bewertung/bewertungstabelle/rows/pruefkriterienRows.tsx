// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form, Input } from 'antd';
import { FormInstance } from 'antd/lib/form/hooks/useForm';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { BewertungKriterium, EvorData, Pruefkriterium, Regelungsalternative } from '@plateg/rest-api';
import { DeleteOutlined } from '@plateg/theme/src/components/icons/DeleteOutlined';

import { BewertungValues } from '../../component.react';
import { Datarow } from '../component.react';
import { TableRadioGroup } from '../table-radio-group/component.react';
export function GeneratePruefkriterienRows(
  pruefkriterien: Pruefkriterium[],
  selectedAlternativen: Regelungsalternative[],
  form: FormInstance,
  deleteElement: (index: number) => void,
  pruefkriterienFocusIndex: number,
): Datarow[] {
  const { t } = useTranslation();

  return pruefkriterien.map((kriterium: Pruefkriterium, index) => {
    const labelText = `${t('bewertung.pruefkriteriumLabel')} ${index + 1}`;
    return {
      key: uuidv4() + '-cell',
      description: (
        <div className="table-margin-validation">
          <Form.Item
            name={['pruefkriterien', 'pruefkriteriumList', index, 'name']}
            rules={[
              { required: true, whitespace: true, message: t('bewertung.validationMessage', { index: index + 1 }) },
            ]}
            label={<span>{`${labelText}`}</span>}
          >
            <Input autoFocus={pruefkriterienFocusIndex === index} required />
          </Form.Item>
          <Button
            id={`evor-deleteCriteria-btn-${index}`}
            type="link"
            style={{ float: 'right' }}
            onClick={() => deleteElement(index)}
            icon={<DeleteOutlined />}
            className="blue-text-button"
          >
            {t('bewertung.deleteCriteria', { index: index + 1 })}
          </Button>
        </div>
      ),
      ...generateRadioGroupEntriesForPruefkriterien(kriterium.id || '', selectedAlternativen, form, labelText),
    };
  });
}

function generateRadioGroupEntriesForPruefkriterien(
  elementId: string,
  selectedAlternativen: Regelungsalternative[],
  form: FormInstance,
  itemName: string,
): { [key: string]: React.ReactElement } {
  const radioGroupEntries: { [key: string]: React.ReactElement } = {};
  selectedAlternativen.forEach((alternative: Regelungsalternative) => {
    const index = findBewertungPruefkriterienIndex(alternative.id, elementId, form);
    const initialValue: number | undefined = (form.getFieldsValue(true) as EvorData).bewertung?.bewertungKriterien[
      index
    ]?.bewertung;
    if (alternative.name) {
      radioGroupEntries[alternative.name] = (
        <fieldset>
          <legend>{itemName}</legend>
          <TableRadioGroup
            formItemName={['bewertung', 'bewertungKriterien', index, 'bewertung']}
            initialValue={initialValue}
            disabled={false}
          />
        </fieldset>
      );
    }
  });
  return radioGroupEntries;
}

function findBewertungPruefkriterienIndex(alternativenId: string, elementId: string, form: FormInstance): number {
  let bewertungsIndex = -1;
  const currentValues = form.getFieldsValue(true) as BewertungValues;
  currentValues?.bewertung?.bewertungKriterien?.forEach((item: BewertungKriterium, index: number) => {
    if (item?.regelungsalternativeId === alternativenId && item?.pruefkriteriumId === elementId) {
      bewertungsIndex = index;
    }
  });
  if (bewertungsIndex === -1) {
    const count: number =
      currentValues?.bewertung?.bewertungKriterien?.push({
        regelungsalternativeId: alternativenId,
        pruefkriteriumId: elementId,
        bewertung: undefined,
      }) || 0;
    bewertungsIndex = count - 1;
    form.setFieldsValue({ bewertung: currentValues.bewertung });
  }
  return bewertungsIndex;
}
