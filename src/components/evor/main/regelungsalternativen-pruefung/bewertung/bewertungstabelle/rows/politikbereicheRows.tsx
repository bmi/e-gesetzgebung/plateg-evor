// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form/hooks/useForm';
import React from 'react';
import { v4 as uuidv4 } from 'uuid';

import { BewertungPolitikbereich, EvorData, Politikbereich, Regelungsalternative } from '@plateg/rest-api';

import { BewertungValues } from '../../component.react';
import { Datarow } from '../component.react';
import { TableRadioGroup } from '../table-radio-group/component.react';
export function GeneratePolitikbereicheRows(
  selectedPolitikbereiche: Politikbereich[],
  selectedAlternativen: Regelungsalternative[],
  form: FormInstance,
): Datarow[] {
  return selectedPolitikbereiche.map((item: Politikbereich) => {
    return {
      key: uuidv4() + '-cell',
      description: item.name as string,
      ...generateRadioGroupEntriesForPolitikbereiche(item.id, selectedAlternativen, form, item.name as string),
    };
  });
}

function generateRadioGroupEntriesForPolitikbereiche(
  elementId: string,
  selectedAlternativen: Regelungsalternative[],
  form: FormInstance,
  itemName: string,
): { [key: string]: React.ReactElement } {
  const radioGroupEntries: { [key: string]: React.ReactElement } = {};
  selectedAlternativen.forEach((alternative: Regelungsalternative) => {
    const index = findBewertungPolitikbereicheIndex(alternative.id, elementId, form);
    const initialValue: number | undefined = (form.getFieldsValue(true) as EvorData).bewertung
      ?.bewertungPolitikbereiche[index]?.bewertung;
    const disabled: boolean = alternative?.auswirkungenPolitikbereiche?.indexOf(elementId) === -1;
    if (alternative.name) {
      radioGroupEntries[alternative.name] = (
        <fieldset>
          <legend>{itemName}</legend>
          <TableRadioGroup
            formItemName={['bewertung', 'bewertungPolitikbereiche', index, 'bewertung']}
            initialValue={initialValue}
            disabled={disabled}
          />
        </fieldset>
      );
    }
  });
  return radioGroupEntries;
}

function findBewertungPolitikbereicheIndex(alternativenId: string, elementId: string, form: FormInstance): number {
  let bewertungsIndex = -1;
  const currentValues = form.getFieldsValue(true) as BewertungValues;
  currentValues?.bewertung?.bewertungPolitikbereiche?.forEach((item: BewertungPolitikbereich, index: number) => {
    if (item?.regelungsalternativeId === alternativenId && item?.politikbereichId === elementId) {
      bewertungsIndex = index;
    }
  });
  if (bewertungsIndex === -1) {
    const count =
      currentValues?.bewertung?.bewertungPolitikbereiche?.push({
        regelungsalternativeId: alternativenId,
        politikbereichId: elementId,
        bewertung: undefined,
      }) || 0;
    bewertungsIndex = count - 1;
    form.setFieldsValue({ bewertung: currentValues.bewertung });
  }
  return bewertungsIndex;
}
