// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Radio } from 'antd';
import { RadioChangeEvent } from 'antd/lib/radio';
import React, { useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
interface TableRadioGroupProps {
  formItemName: any[];
  initialValue?: number;
  disabled: boolean;
}

export function TableRadioGroup(props: TableRadioGroupProps): React.ReactElement {
  const [selectedValue, setSelectedValue] = useState<undefined | number>(undefined);
  const [backgroundColor, setBackgroundColor] = useState('');

  const updateBackgroundColor = (value: number | undefined) => {
    switch (value) {
      case -1:
        setBackgroundColor('#FFE6E6');
        break;
      case 0:
        setBackgroundColor('#EDEDED');
        break;
      case 1:
        setBackgroundColor('#F0FFEB');
        break;
    }
  };
  useEffect(() => {
    updateBackgroundColor(selectedValue ?? props.initialValue);
  });
  const onChange = (e: RadioChangeEvent) => {
    setSelectedValue(e.target.value as number);
    updateBackgroundColor(e.target.value as number);
  };
  return (
    <>
      <Form.Item name={props.formItemName} className="radio-group" style={{ backgroundColor }}>
        <Radio.Group
          onChange={onChange}
          name={'radiogroup-' + uuidv4()}
          value={selectedValue}
          disabled={props.disabled}
        >
          <Radio value={-1}>negativ</Radio>
          <Radio value={0}>neutral</Radio>
          <Radio value={1}>positiv</Radio>
        </Radio.Group>
      </Form.Item>
    </>
  );
}
