// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Table, Typography } from 'antd';
import { FormInstance } from 'antd/lib/form/hooks/useForm';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { EvorData, Regelungsalternative } from '@plateg/rest-api';
import { TableScroll } from '@plateg/theme';
import { PlusOutlined } from '@plateg/theme/src/components/icons/PlusOutlined';

import { BewertungValues } from '../component.react';
import { extractPolitikbereiche } from '../controller';
import { GenerateBewertungRow } from './rows/bewertungRow';
import { GenerateGesamtrangRow } from './rows/gesamtrangRow';
import { GeneratePolitikbereicheRows } from './rows/politikbereicheRows';
import { GeneratePruefkriterienRows } from './rows/pruefkriterienRows';
import { generateZieleRows } from './rows/zieleRows';

interface BewertungstabelleProps {
  selectedAlternativen: Regelungsalternative[];
  currentDraft: EvorData;
  form: FormInstance;
  formInitialValue: BewertungValues;
  setDirtyFlag: Function;
}

export interface Datarow {
  key: string;
  description: string | React.ReactElement;
}

export function BewertungsTabelle(props: BewertungstabelleProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;

  const selectedPolitikbereiche = extractPolitikbereiche(props.currentDraft?.politikbereiche?.politikbereichList || []);
  const [pruefkriterien, setPruefkriterien] = useState(
    props.currentDraft?.pruefkriterien?.pruefkriteriumList ||
      props.formInitialValue?.pruefkriterien?.pruefkriteriumList ||
      [],
  );
  const [pruefkriterienFocusIndex, setPruefkriterienFocusIndes] = useState(-1);

  const addPruefkriterium = () => {
    const currentValues = props.form.getFieldsValue(true) as BewertungValues;
    currentValues?.pruefkriterien?.pruefkriteriumList?.push({ id: uuidv4(), name: '' });
    setPruefkriterien(currentValues?.pruefkriterien?.pruefkriteriumList?.slice() || []);
    setPruefkriterienFocusIndes(currentValues?.pruefkriterien?.pruefkriteriumList?.length - 1);
    props.form.setFieldsValue(currentValues);
    props.setDirtyFlag();
  };

  const deletePruefkriterium = (index: number) => {
    const currentValues = props.form.getFieldsValue(true) as BewertungValues;
    currentValues?.pruefkriterien?.pruefkriteriumList?.splice(index, 1);
    setPruefkriterien(currentValues?.pruefkriterien?.pruefkriteriumList?.slice() || []);
    setPruefkriterienFocusIndes(index - 1);
    props.form.setFieldsValue(currentValues);
    props.setDirtyFlag();
  };

  let columns = props.selectedAlternativen.map((alternative) => {
    return { title: alternative.name, dataIndex: alternative.name, fixed: false };
  });
  columns = [{ title: '', dataIndex: 'description', fixed: true, width: 170 }, ...columns];

  const zieleRows: Datarow[] = generateZieleRows(props.currentDraft, props.selectedAlternativen, props.form);

  const pruefkriterienRows: Datarow[] = GeneratePruefkriterienRows(
    pruefkriterien,
    props.selectedAlternativen,
    props.form,
    deletePruefkriterium,
    pruefkriterienFocusIndex,
  );

  const politikbereicheRows: Datarow[] = GeneratePolitikbereicheRows(
    selectedPolitikbereiche,
    props.selectedAlternativen,
    props.form,
  );

  const bewertungRow: Datarow = GenerateBewertungRow(props.selectedAlternativen);

  const gesamtrangRow: Datarow = GenerateGesamtrangRow(props.selectedAlternativen);
  const data = [
    {
      key: uuidv4() + '-title',
      description: <Title level={4}>{t('bewertung.goalTitle')}</Title>,
    },
    ...zieleRows,
    {
      key: uuidv4() + '-title',
      description: <Title level={4}>{t('bewertung.criteriaTitle')}</Title>,
    },
    ...pruefkriterienRows,
    {
      key: uuidv4() + '-title',
      description: (
        <Button id="evor-addPruefkriterium-btn" type="link" onClick={addPruefkriterium} className="blue-text-button">
          <PlusOutlined />
          {t('bewertung.addCriteria')}
        </Button>
      ),
    },
    {
      key: uuidv4() + '-title',
      description: <Title level={4}>{t('bewertung.politikbereichTitle')}</Title>,
    },
    ...politikbereicheRows,
    {
      key: uuidv4() + '-title',
      description: <Title level={4}>{t('bewertung.ratingTitle')}</Title>,
    },
    bewertungRow,
    {
      key: uuidv4() + '-title',
      description: <Title level={4}>{t('bewertung.rankTitle')}</Title>,
    },
    gesamtrangRow,
  ];

  return (
    <div className="bewertung-table">
      <TableScroll>
        <Table
          columns={columns}
          dataSource={data}
          pagination={false}
          rowClassName={(record) => {
            if (record.key.indexOf('-title') !== -1) {
              return 'title-row';
            }
            if (record.key.indexOf('-cell') !== -1) {
              return 'cell-row';
            }
            return '';
          }}
        />
      </TableScroll>
    </div>
  );
}
