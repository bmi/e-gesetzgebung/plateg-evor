// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Checkbox, Form } from 'antd';
import React, { useEffect } from 'react';

import { Regelungsalternative } from '@plateg/rest-api';
interface AlternativenBannerProps {
  regelungsalternativen: Regelungsalternative[];
  isRankDisplayed: boolean;
  setSelectedAlternativen: React.Dispatch<
    React.SetStateAction<{
      alternativen: Regelungsalternative[];
    }>
  >;
  preIndexName: string[];
  postIndexName: string[];
}

export function AlternativenCheckboxList(props: AlternativenBannerProps): React.ReactElement {
  useEffect(() => {
    const tmpRegelungsalternativen = props.regelungsalternativen;
    if (tmpRegelungsalternativen !== undefined && tmpRegelungsalternativen.length > 0) {
      // Checkboxen auf true setzen, falls bisher noch nicht auf dieser Seite gewesen
      // Zudem Klick "simulieren", damit Tabelle angepasst wird
      tmpRegelungsalternativen.forEach((alternative) => {
        alternative.bewertungVornehmen =
          alternative.bewertungVornehmen === undefined ? true : alternative.bewertungVornehmen;
        onCheckboxChange(alternative, alternative.bewertungVornehmen);
      });
    }
  }, []);

  const onCheckboxChange = (alternative: Regelungsalternative, checked: boolean) => {
    if (checked) {
      props.setSelectedAlternativen((prev) => {
        prev.alternativen.push(alternative);
        return { ...prev };
      });
    } else {
      props.setSelectedAlternativen((prev) => {
        const itemIndex = prev.alternativen.findIndex((item) => item.id === alternative.id);
        if (itemIndex > -1) {
          prev.alternativen.splice(itemIndex, 1);
        }
        return { ...prev };
      });
    }
  };

  const alternativenCheckboxList = props.regelungsalternativen.map((item: Regelungsalternative, index) => {
    return (
      <li key={index}>
        <div className="bewertung-checkbox">
          <Form.Item valuePropName="checked" name={[...props.preIndexName, index, ...props.postIndexName]} noStyle>
            <Checkbox
              id={`evor-politikbereiche-${item?.id}-chk`}
              onChange={(e) => onCheckboxChange(item, e.target.checked)}
              className="bewertung-checkbox"
              defaultChecked={true}
            >
              {item.name}
            </Checkbox>
          </Form.Item>
        </div>
      </li>
    );
  });
  return <ul className="regelungsalternativen-list">{alternativenCheckboxList}</ul>;
}
