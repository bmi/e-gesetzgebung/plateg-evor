// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './bewertung.less';

import { Form, Input, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import {
  BewertungKriterium,
  BewertungPolitikbereich,
  BewertungZiel,
  Politikbereich,
  Pruefkriterien,
  Regelungsalternative,
} from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { saveDraftContent } from '../../../../../shares/localStorage';
import { ROUTES } from '../../../../../shares/routes';
import { StandardEvorProps } from '../../component.react';
import { AlternativenCheckboxList } from './alternativen-checkbox-list/component.react';
import { BewertungsTabelle } from './bewertungstabelle/component.react';
import { getBewertungWithCleanKriterienReferences } from './controller';

export interface BewertungValues {
  regelungsalternativen: { regelungsalternativeList: Regelungsalternative[] };
  pruefkriterien: Pruefkriterien;
  politikbereiche?: { politikbereichList: Politikbereich[] };
  bewertung: {
    bewertungZiele: BewertungZiel[];
    bewertungPolitikbereiche: BewertungPolitikbereich[];
    bewertungKriterien: BewertungKriterium[];
  };
}

export function BewertungComponent(props: StandardEvorProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const [form] = Form.useForm();
  let dirty = false;
  const { TextArea } = Input;
  const [selectedAlternativen, setSelectedAlternativen] = useState<{ alternativen: Regelungsalternative[] }>({
    alternativen: [],
  });

  const currentDraftId: string = props.currentDraft.id || '';

  const setDirtyFlag = () => {
    dirty = true;
  };

  const [formInitialValue, setFormInitialValue] = useState<BewertungValues>();
  const [showForm, setShowForm] = useState(false);

  useEffect(() => {
    const initial = {
      bewertung: props.currentDraft?.bewertung || {
        bewertungZiele: [],
        bewertungPolitikbereiche: [],
        bewertungKriterien: [],
      },
      pruefkriterien: props.currentDraft.pruefkriterien || {
        pruefkriteriumList: [
          { id: uuidv4(), name: 'Umsetzung/Vollzug' },
          { id: uuidv4(), name: 'Effizienz' },
        ],
      },
      regelungsalternativen: props.currentDraft?.regelungsalternativen || { regelungsalternativeList: [] },
    };
    initial.regelungsalternativen.regelungsalternativeList?.forEach((item) => {
      if (item.bewertungVornehmen === undefined) {
        item.bewertungVornehmen = true;
      }
    });
    initial.bewertung.bewertungZiele = initial.bewertung.bewertungZiele || [];
    initial.bewertung.bewertungPolitikbereiche = initial.bewertung.bewertungPolitikbereiche || [];
    initial.bewertung.bewertungKriterien = initial.bewertung.bewertungKriterien || [];
    setFormInitialValue(initial);
    form.setFieldsValue(initial);
  }, []);

  useEffect(() => {
    const tmpRegelungsalternativen = props.currentDraft.regelungsalternativen?.regelungsalternativeList;
    if (tmpRegelungsalternativen !== undefined && tmpRegelungsalternativen.length > 0) {
      setShowForm(true);
    } else {
      setShowForm(false);
    }
    props.setIsFormDirty?.(false);
  }, []);

  const saveDraft = () => {
    const values = form.getFieldsValue(true) as BewertungValues;
    values?.regelungsalternativen?.regelungsalternativeList?.forEach((alternative, i) => {
      if (alternative?.rang && alternative?.rang === 0) {
        delete values.regelungsalternativen.regelungsalternativeList[i].rang;
      }
    });
    saveDraftContent({
      id: currentDraftId,
      pruefkriterien: values.pruefkriterien,
      bewertung: getBewertungWithCleanKriterienReferences(values.pruefkriterien, values.bewertung),
      regelungsalternativen: values.regelungsalternativen,
    });
  };

  const regelungsalternativen: Regelungsalternative[] =
    props?.currentDraft?.regelungsalternativen?.regelungsalternativeList || [];

  return (
    <div>
      <FormWrapper
        projectName="eVoR"
        title={
          <div className="heading-holder">
            <Title level={1}>{t('bewertung.title')} &nbsp;</Title>
          </div>
        }
        previousPage={`/evor/${currentDraftId}/${ROUTES.bewertung.bewertungAuswirkungenNachAlternative}`}
        nextPage={`/evor/${currentDraftId}/${ROUTES.konsultation.konsultationVorbereitung}`}
        saveDraft={saveDraft}
        isDirty={() => form.isFieldsTouched() || dirty}
        form={form}
        formInitialValue={formInitialValue}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          props.setIsFormDirty?.(true);
        }}
      >
        <Title level={2}>{t('bewertung.subtitle1')}</Title>
        <p>{t('bewertung.infoText1')}</p>
        <Title level={3}>{t('bewertung.subsubTitle1')}</Title>
        {showForm === true && (
          <>
            <p>{t('bewertung.infoText2')}</p>
            <AlternativenCheckboxList
              regelungsalternativen={regelungsalternativen}
              isRankDisplayed={false}
              setSelectedAlternativen={setSelectedAlternativen}
              preIndexName={['regelungsalternativen', 'regelungsalternativeList']}
              postIndexName={['bewertungVornehmen']}
            />
            <Title level={3}>{t('bewertung.subsubTitle2')}</Title>
            <BewertungsTabelle
              selectedAlternativen={selectedAlternativen.alternativen}
              currentDraft={props.currentDraft}
              form={form}
              formInitialValue={formInitialValue}
              setDirtyFlag={setDirtyFlag}
            />
            <Title level={3}>{t('bewertung.subsubTitle3')}</Title>
            <p>{t('bewertung.infoText3')}</p>
            <Form.Item name={['bewertung', 'gesamtbewertung']} label={<span>{t('bewertung.subsubTitle4')}</span>}>
              <TextArea rows={8}></TextArea>
            </Form.Item>
          </>
        )}
        {showForm === false && (
          <p className="ant-typography p-no-style">{t('bewertung.noRegelungsalternativenText')}</p>
        )}
      </FormWrapper>
    </div>
  );
}
