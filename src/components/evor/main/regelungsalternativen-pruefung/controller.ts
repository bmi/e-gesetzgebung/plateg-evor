// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';

import { Politikbereich, Politikunterbereich, Regelungsalternativen } from '@plateg/rest-api';

import { IdeenValues } from '../entwicklung-alternativen/entwicklung-ideen/component.react';
import { extractPolitikbereiche } from './bewertung/controller';
/**
 * Preparing content for info box. It can have one or more sentences.
 * To prefil it dynamically we just check if message key exist in translation files.
 * @param messageKey can be as categoryAlias like 'baustadtwohnen' if category has only one element
 * or can be in format categoryAlias.itemAlias 'finance.finanzmarktpolitik' if category has areas
 * @return string[]
 */
export function prepareInfoText(messageKey: string): string[] {
  let index = 1;
  const result: string[] = [];
  while (i18n.exists(`politikbereichen.${messageKey}.infoText${index}`)) {
    result.push(`politikbereichen.${messageKey}.infoText${index}`);
    index++;
  }
  return result;
}

export interface PolitikbereichenValues {
  [key: string]: string[];
}

export interface CategoryItem {
  alias: string;
  unterbereiche: string[];
}

export interface CategoryFormItem {
  name: string;
  items: CheckboxItem[];
  hasNested: boolean;
}
export interface CheckboxItem {
  label: React.ReactElement;
  value: string;
}
/**
 * Convert stored data to Form format data
 * @param data
 */
export function prepareInitialFormPolitikbereichenValues(data: Politikbereich[]): PolitikbereichenValues {
  const formatedData: PolitikbereichenValues = {};
  data.forEach((category: Politikbereich) => {
    if (!category.unterbereiche.length) {
      // if it has no subcategories use category alias as array with one item
      formatedData[category.alias] = [category.alias];
    } else {
      // if it has subcategories put subcategories alias in array
      formatedData[category.alias] = category.unterbereiche.map((subArea) => subArea.alias);
    }
  });

  return formatedData;
}

/**
 * Get array of only activated categories aliases
 * @param values
 */
export function getActivatedCategories(values: PolitikbereichenValues): CategoryItem[] {
  return Object.keys(values).reduce((list: CategoryItem[], category: string) => {
    if (values[category] && values[category].length) {
      list.push({
        alias: category,
        unterbereiche: values[category],
      });
    }
    return list;
  }, []);
}

/**
 * Reduce initial structure to get only activated categories and subareas
 * @param activatedCategories
 * @param initialStructure
 */
export function prepareResonseData(
  activatedCategories: CategoryItem[],
  initialStructure: Politikbereich[],
): Politikbereich[] {
  const preparedResponse: Politikbereich[] = [];

  activatedCategories.forEach((activeCategory: CategoryItem) => {
    initialStructure.forEach((category: Politikbereich) => {
      // Category without subareas
      if (activeCategory.alias === category.alias && !category.unterbereiche.length) {
        preparedResponse.push(category);
      }
      // Categories with subareas
      if (activeCategory.alias === category.alias && category.unterbereiche.length) {
        // Clone initial category for assigning to it reduced list of active subcategories
        const reducedCategory = { ...category };
        const activeSubAreas: Politikbereich[] = [];
        activeCategory.unterbereiche.forEach((activeSubArea) => {
          // take only active subcategories
          category.unterbereiche.forEach((subCategory) => {
            if (subCategory.alias === activeSubArea) {
              activeSubAreas.push(subCategory);
            }
          });
        });

        reducedCategory.unterbereiche = activeSubAreas;
        preparedResponse.push(reducedCategory);
      }
    });
  });

  return preparedResponse;
}

/**
 * Get flat list with selected categories.
 * @param politikbereichenList
 */
export function getSelectedPolitikbereichen(politikbereichenList: Politikbereich[]): Politikunterbereich[] {
  return politikbereichenList.reduce((selectedItemsList: Politikunterbereich[], category: Politikbereich) => {
    // Get catgories without subcategories
    if (!category.unterbereiche.length) {
      selectedItemsList.push(category);
    } else {
      // Get catgories that are subcategories
      category.unterbereiche.forEach((subCategory: Politikunterbereich) => {
        const clonedSubCategory: Politikunterbereich = {
          ...subCategory,
          alias: `${category.alias as string}.${subCategory.alias as string}`,
        };
        selectedItemsList.push(clonedSubCategory);
      });
    }
    return selectedItemsList;
  }, []);
}

export function getRegelungsalternativenWithCleanOptionReferences(
  values: IdeenValues,
  regelungsalternativen: Regelungsalternativen,
): Regelungsalternativen {
  const ids = values.teilbereichList.flatMap((teilbereich) => teilbereich.optionen).map((option) => option.id);
  regelungsalternativen?.regelungsalternativeList?.forEach((alternative, i) => {
    alternative?.teilbereichList.forEach((teilbereich, j) => {
      if (teilbereich?.teilbereichsoptionId !== undefined && !ids.includes(teilbereich?.teilbereichsoptionId)) {
        regelungsalternativen.regelungsalternativeList[i].teilbereichList[j].teilbereichsoptionId = undefined;
      }
    });
  });
  return regelungsalternativen;
}

export function getRegelungsalternativenWithCleanTeilbereichReferences(
  values: IdeenValues,
  regelungsalternativen: Regelungsalternativen,
): Regelungsalternativen {
  const ids = values.teilbereichList.map((option) => option.id);
  regelungsalternativen?.regelungsalternativeList?.forEach((alternative, i) => {
    alternative?.teilbereichList.forEach((teilbereich, j) => {
      if (teilbereich?.teilbereichId !== undefined && !ids.includes(teilbereich?.teilbereichId)) {
        regelungsalternativen?.regelungsalternativeList?.[i].teilbereichList?.splice(j, 1);
      }
    });
  });
  return regelungsalternativen;
}

export function getRegelungsalternativenWithCleanPolitikbereicheReferences(
  values: Politikbereich[],
  regelungsalternativen: Regelungsalternativen,
): Regelungsalternativen {
  const flattenedPolitikbereiche = extractPolitikbereiche(values);
  const ids = flattenedPolitikbereiche.map((unterbereich) => unterbereich.id);
  regelungsalternativen?.regelungsalternativeList?.forEach((alternative) => {
    alternative.auswirkungenPolitikbereiche = alternative.auswirkungenPolitikbereiche?.filter((id) => ids.includes(id));
  });
  return regelungsalternativen;
}
