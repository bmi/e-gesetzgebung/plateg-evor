// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './politikbereichen.less';

import { Form, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Politikbereich, Regelungsalternative } from '@plateg/rest-api';
import { CheckboxGroupWithInfo, FormWrapper, InfoComponent } from '@plateg/theme';
import { CheckboxItem } from '@plateg/theme/src/components/checkbox-group-with-info/component.react';

import { getPolitikbereichen } from '../../../../../data/politikbereiche';
import { saveDraftContent } from '../../../../../shares/localStorage';
import { ROUTES } from '../../../../../shares/routes';
import { StandardEvorProps } from '../../component.react';
import {
  getBewertungWithCleanPolitikbereiche,
  getKonsultationenWithCleanRessortReferences,
} from '../../konsultation/vorbereitung/controller';
import {
  getActivatedCategories,
  getRegelungsalternativenWithCleanPolitikbereicheReferences,
  PolitikbereichenValues,
  prepareInfoText,
  prepareInitialFormPolitikbereichenValues,
  prepareResonseData,
} from '../controller';

interface CategoryFormItem {
  name: string;
  label: string;
  items: CheckboxItem[];
  hasNested: boolean;
}

export function PolitikbereichenComponent(props: StandardEvorProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const [form] = Form.useForm();
  const initialStructure = getPolitikbereichen();
  const currentDraftId = props.currentDraft.id;
  const [showForm, setShowForm] = useState(false);

  useEffect(() => {
    if (
      props.currentDraft.regelungsalternativen?.regelungsalternativeList !== undefined &&
      props.currentDraft.regelungsalternativen?.regelungsalternativeList?.length > 0
    ) {
      setShowForm(true);
    } else {
      setShowForm(false);
    }
  });

  useEffect(() => {
    props.setIsFormDirty?.(false);
  }, []);

  /**
   * Createing checkbox item
   * @param categoryAlias
   * @param itemAlias
   */
  const createCheckboxItem = (categoryAlias: string, itemAlias?: string) => {
    // use different messageKey structure depending on has category subareas or not
    const messageKey: string = itemAlias ? `${categoryAlias}.${itemAlias}` : categoryAlias;
    const infoTextList: string[] = prepareInfoText(messageKey);
    return {
      label: (
        <span className="nonBreakingArea">
          <span className="normalBreakingArea">{t(`politikbereichen.${messageKey}.label`)}</span>&nbsp;
          <InfoComponent title={t(`politikbereichen.${messageKey}.infoTitle`)}>
            {infoTextList.map((text, i) => {
              return (
                <p className="ant-typography p-no-style" key={`${messageKey}${i}`}>
                  {t(text)}
                </p>
              );
            })}
          </InfoComponent>
        </span>
      ),
      // use different value depending on has category subareas or not
      value: itemAlias ? itemAlias : categoryAlias,
      title: t(`politikbereichen.${messageKey}.infoTitle`),
    };
  };

  /**
   * Preparing data for creating form.
   */
  const preparedFormData: CategoryFormItem[] = initialStructure.reduce((accumulator: CategoryFormItem[], category) => {
    const checkboxItems: CheckboxItem[] = [];
    const categoryAlias = category.alias as string;
    const categoryLabel = category.name as string;

    if (!category.unterbereiche.length) {
      const formCheckboxItem = createCheckboxItem(categoryAlias);
      checkboxItems.push(formCheckboxItem);
    } else {
      category.unterbereiche.forEach((item) => {
        const formCheckboxItem = createCheckboxItem(categoryAlias, item.alias);
        checkboxItems.push(formCheckboxItem);
      });
    }

    const categoryItem: CategoryFormItem = {
      name: categoryAlias,
      label: categoryLabel,
      items: checkboxItems,
      hasNested: category.unterbereiche.length > 0,
    };

    accumulator.push(categoryItem);
    return accumulator;
  }, []);

  const formInitialValue: PolitikbereichenValues = prepareInitialFormPolitikbereichenValues(
    props.currentDraft.politikbereiche?.politikbereichList || [],
  );

  const regelungsalternativenList: Regelungsalternative[] =
    props.currentDraft.regelungsalternativen?.regelungsalternativeList || [];

  const saveDraft = () => {
    const values = form.getFieldsValue(true) as PolitikbereichenValues;
    // Filter for form values and get only activated categories aliases
    const activatedCategories = getActivatedCategories(values);
    // Get only activated categories and subareas from intial data
    const preparedResponse: Politikbereich[] = prepareResonseData(activatedCategories, initialStructure);
    let cleanedKonsultationen = props?.currentDraft?.konsultationen;
    if (props?.currentDraft?.konsultationen) {
      cleanedKonsultationen = getKonsultationenWithCleanRessortReferences(
        preparedResponse,
        props?.currentDraft?.konsultationen,
        t,
      );
    }
    let cleanedBewertung = props.currentDraft.bewertung?.bewertungPolitikbereiche || [];
    if (cleanedBewertung) {
      cleanedBewertung = getBewertungWithCleanPolitikbereiche(cleanedBewertung, preparedResponse);
    }
    // Save categories to draft
    saveDraftContent({
      id: currentDraftId,
      bewertung: {
        ...props.currentDraft.bewertung,
        bewertungPolitikbereiche: cleanedBewertung,
      },
      konsultationen: cleanedKonsultationen,
      politikbereiche: {
        politikbereichList: preparedResponse,
      },
      regelungsalternativen: getRegelungsalternativenWithCleanPolitikbereicheReferences(
        preparedResponse,
        props.currentDraft.regelungsalternativen,
      ),
    });
  };

  return (
    <div className="politikbereichen-form">
      <FormWrapper
        projectName="eVoR"
        title={
          <div className="heading-holder">
            <Title level={1}>{t('regelungsalternativen.title')}</Title>
          </div>
        }
        previousPage={`/evor/${currentDraftId}/${ROUTES.entwicklung.entwicklungAlternativen}`}
        nextPage={`/evor/${currentDraftId}/${ROUTES.bewertung.bewertungAuswirkungenNachAlternative}`}
        saveDraft={saveDraft}
        isDirty={() => form.isFieldsTouched()}
        form={form}
        formInitialValue={formInitialValue}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          props.setIsFormDirty?.(true);
        }}
      >
        <Title level={2}>{t('politikbereichen.title')}</Title>
        {showForm === true && (
          <>
            <p className="ant-typography p-no-style">{t('politikbereichen.intro')}</p>

            <ul className="regelungsalternativen-list">
              {regelungsalternativenList.map((item: Regelungsalternative, i) => {
                return <li key={i}>{item.name}</li>;
              })}
            </ul>

            <div className="heading-holder">
              <span style={{ whiteSpace: 'nowrap' }}>
                <Title level={2}>{t('politikbereichen.title2')}</Title>
                <InfoComponent title={t('politikbereichen.infoAreasTitle')}>
                  <p className="ant-typography p-no-style">{t('politikbereichen.infoAreasText1')}</p>
                </InfoComponent>
              </span>
            </div>

            <p className="ant-typography p-no-style">{t('politikbereichen.text2')}</p>

            {preparedFormData.map((category: CategoryFormItem, i) => {
              return (
                <fieldset key={`${category.label}-${i}`}>
                  <legend>{category.label}</legend>
                  <Form.Item key={i} name={category.name} className={!category.hasNested ? 'single-item' : ''}>
                    <CheckboxGroupWithInfo items={category.items} id={category.name} />
                  </Form.Item>
                </fieldset>
              );
            })}
          </>
        )}
        {showForm === false && (
          <p className="ant-typography p-no-style">{t('politikbereichen.noRegelungsalternativenText')}</p>
        )}
      </FormWrapper>
    </div>
  );
}
