// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './auswirkungenNachRegelungsalternativen.less';

import { Form, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Politikunterbereich, Regelungsalternative } from '@plateg/rest-api';
import { FormWrapper, InfoComponent } from '@plateg/theme';
import { CheckOutlined } from '@plateg/theme/src/components/icons/CheckOutlined';

import { saveDraftContent } from '../../../../../shares/localStorage';
import { ROUTES } from '../../../../../shares/routes';
import { StandardEvorProps } from '../../component.react';
import { getBewertungWithDisabledPolitikbereiche } from '../../konsultation/vorbereitung/controller';
import { getSelectedPolitikbereichen, prepareInfoText } from '../controller';
import { RegelungsalternativenSelectComponent } from './regelungsalternativen-select-component/component.react';

export interface AuswirkungenNachRegelungsalternativeValues {
  regelungsDataList: RegelungsData[];
}
export interface RegelungsData {
  auswirkungenPolitikbereiche: string[];
  auswirkungenPruefen: boolean;
  auswirkungen: string;
  id: string;
}

export function AuswirkungenNachRegelungsalternativenComponent(props: StandardEvorProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const [activeAlternatives, setActiveAlternatives] = useState<RegelungsData>({
    auswirkungenPolitikbereiche: [],
    auswirkungenPruefen: false,
    auswirkungen: '',
    id: '',
  });
  const [form] = Form.useForm();
  const currentDraftId: string = props.currentDraft.id;
  const [showForm, setShowForm] = useState(false);

  useEffect(() => {
    if (
      props.currentDraft.politikbereiche?.politikbereichList !== undefined &&
      props.currentDraft.politikbereiche?.politikbereichList?.length > 0
    ) {
      setShowForm(true);
    } else {
      setShowForm(false);
    }
  });

  useEffect(() => {
    props.setIsFormDirty?.(false);
  }, []);

  const selectedPolitikbereichen: Politikunterbereich[] = getSelectedPolitikbereichen(
    props.currentDraft.politikbereiche?.politikbereichList || [],
  );

  const alternativenList: Regelungsalternative[] =
    props.currentDraft.regelungsalternativen?.regelungsalternativeList || [];

  // First call current draft is not yet filled, so we give defaults
  const formInitialValue: AuswirkungenNachRegelungsalternativeValues = {
    regelungsDataList: alternativenList.reduce((acc: RegelungsData[], item: Regelungsalternative) => {
      const regelungItem: RegelungsData = {
        auswirkungenPruefen: item.auswirkungenPruefen || false,
        auswirkungen: item.auswirkungen || '',
        id: item.id || '',
        auswirkungenPolitikbereiche: item.auswirkungenPolitikbereiche || [],
      };
      acc.push(regelungItem);
      return acc;
    }, []),
  };

  useEffect(() => {
    const defaultState: RegelungsData = {} as RegelungsData;
    formInitialValue.regelungsDataList.forEach((item: RegelungsData) => {
      defaultState[item.id] = item.auswirkungenPruefen;
    });
    setActiveAlternatives(defaultState);
  }, []);

  const onCheckboxChange = (index: number, itemId: string, checked: boolean) => {
    const formValues: AuswirkungenNachRegelungsalternativeValues = form.getFieldsValue(
      true,
    ) as AuswirkungenNachRegelungsalternativeValues;
    formValues.regelungsDataList[index]['auswirkungenPruefen'] = checked;
    form.setFieldsValue(formValues);
    const newActiveAlternatives = { ...activeAlternatives };
    newActiveAlternatives[itemId] = checked;
    setActiveAlternatives(newActiveAlternatives);
  };

  const handlePolitikbereicheCheckboxChange = (index: number, indexPolitikbereich: number, checked: boolean) => {
    const formValues: AuswirkungenNachRegelungsalternativeValues = form.getFieldsValue(
      true,
    ) as AuswirkungenNachRegelungsalternativeValues;
    const politikbereich = selectedPolitikbereichen[indexPolitikbereich].id;
    if (politikbereich) {
      if (checked) {
        formValues.regelungsDataList[index].auswirkungenPolitikbereiche.push(politikbereich);
      } else {
        formValues.regelungsDataList[index].auswirkungenPolitikbereiche = formValues.regelungsDataList[
          index
        ].auswirkungenPolitikbereiche.filter((a) => a !== politikbereich);
      }
    }
    form.setFieldsValue(formValues);
  };

  const getCheckedValue = (index: number, indexPolitikbereich: number) => {
    const initialValues = formInitialValue;
    const politikbereich = selectedPolitikbereichen[indexPolitikbereich].id;
    if (politikbereich) {
      return initialValues.regelungsDataList[index].auswirkungenPolitikbereiche.includes(politikbereich);
    } else {
      return false;
    }
  };

  const saveDraft = () => {
    const values: AuswirkungenNachRegelungsalternativeValues = form.getFieldsValue(
      true,
    ) as AuswirkungenNachRegelungsalternativeValues;
    const updatedList: Regelungsalternative[] = [...alternativenList];
    const preparedResponse: Regelungsalternative[] = updatedList.map((item) => {
      const updatedItem: Regelungsalternative = { ...item };
      values.regelungsDataList.forEach((formItem) => {
        if (formItem.id === updatedItem.id) {
          updatedItem.auswirkungen = formItem.auswirkungen;
          updatedItem.auswirkungenPruefen = formItem.auswirkungenPruefen;
          updatedItem.auswirkungenPolitikbereiche = formItem.auswirkungenPolitikbereiche;
        }
      });

      return updatedItem;
    });
    const cleanedBewertungPolitikbereiche = props.currentDraft.bewertung?.bewertungPolitikbereiche || [];
    getBewertungWithDisabledPolitikbereiche(cleanedBewertungPolitikbereiche, preparedResponse);
    // Save categories to draft
    saveDraftContent({
      id: currentDraftId,
      bewertung: {
        ...props.currentDraft.bewertung,
        bewertungPolitikbereiche: cleanedBewertungPolitikbereiche,
      },
      regelungsalternativen: {
        regelungsalternativeList: preparedResponse,
        teilbereiche: props.currentDraft.regelungsalternativen?.teilbereiche,
      },
    });
  };

  return (
    <div className="auswirkungen-nach-regelungsalternativen-form">
      <FormWrapper
        projectName="eVoR"
        title={
          <div className="heading-holder">
            <Title level={1}>{t('regelungsalternativen.title')} &nbsp;</Title>
          </div>
        }
        previousPage={`/evor/${currentDraftId}/${ROUTES.bewertung.bewertungAuswirkungenPolitik}`}
        nextPage={`/evor/${currentDraftId}/${ROUTES.bewertung.bewertung}`}
        saveDraft={saveDraft}
        isDirty={() => form.isFieldsTouched()}
        form={form}
        formInitialValue={formInitialValue}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          props.setIsFormDirty?.(true);
        }}
      >
        <Title level={2}>{t('auswirkungennachregelungsalternativen.title')}</Title>
        {showForm === true && (
          <>
            <p className="ant-typography p-no-style">{t('auswirkungennachregelungsalternativen.intro')}</p>

            {/* List of selected Politikbereichen */}
            <ul className="politikBereichenList">
              {selectedPolitikbereichen.map((category: Politikunterbereich, i) => {
                const categoryAlias: string = category.alias as string;
                const infoTextList: string[] = prepareInfoText(categoryAlias);
                return (
                  <li key={i}>
                    <CheckOutlined />
                    <span className="nonBreakingArea">
                      &nbsp;<span className="normalBreakingArea">{category.name}</span>&nbsp;
                      <InfoComponent
                        id={`evor-politikbereiche-wesentliche-auswirkungen-${categoryAlias}`}
                        title={t(`politikbereichen.${categoryAlias}.infoTitle`)}
                      >
                        {infoTextList.map((text, j) => {
                          return (
                            <p className="ant-typography p-no-style" key={`${categoryAlias}${j}`}>
                              {t(text)}
                            </p>
                          );
                        })}
                      </InfoComponent>
                    </span>
                  </li>
                );
              })}
            </ul>
            <Title level={2}>{t('auswirkungennachregelungsalternativen.subtitle1')}</Title>
            <RegelungsalternativenSelectComponent
              alternativenList={alternativenList}
              activeAlternatives={activeAlternatives}
              onCheckboxChange={onCheckboxChange}
              selectedPolitikbereichen={selectedPolitikbereichen}
              getCheckedValue={getCheckedValue}
              handlePolitikbereicheCheckboxChange={handlePolitikbereicheCheckboxChange}
            />
          </>
        )}
        {showForm === false && (
          <p className="ant-typography p-no-style">{t('auswirkungennachregelungsalternativen.noOptionenText')}</p>
        )}
      </FormWrapper>
    </div>
  );
}
