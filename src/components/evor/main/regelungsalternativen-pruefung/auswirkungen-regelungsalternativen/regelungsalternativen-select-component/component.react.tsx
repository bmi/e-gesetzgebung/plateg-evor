// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Checkbox, Form, Input } from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { BASE_PATH, Politikunterbereich, Regelungsalternative } from '@plateg/rest-api';
import { CheckboxWithInfo, FormItemWithInfo, InfoComponent } from '@plateg/theme';

import { prepareInfoText } from '../../controller';
import { RegelungsData } from '../component.react';
interface RegelungsalternativenSelectProps {
  alternativenList: Regelungsalternative[];
  activeAlternatives: RegelungsData;
  onCheckboxChange: (index: number, itemId: string, checked: boolean) => void;
  selectedPolitikbereichen: Politikunterbereich[];
  getCheckedValue: (index: number, indexPolitikbereich: number) => boolean;
  handlePolitikbereicheCheckboxChange: (index: number, indexPolitikbereich: number, checked: boolean) => void;
}

export function RegelungsalternativenSelectComponent(props: RegelungsalternativenSelectProps): React.ReactElement {
  const { t } = useTranslation();
  const { TextArea } = Input;

  const GGO_LINK = BASE_PATH + '/arbeitshilfen/download/34#page=';

  return (
    <ul>
      {props.alternativenList.map((item: Regelungsalternative, i) => {
        return (
          <li key={i}>
            <Form.Item
              name={['regelungsDataList', i, 'auswirkungenPruefen']}
              className="title-holder"
              valuePropName={item.auswirkungenPruefen ? 'checked' : ''}
            >
              <Checkbox
                id={`evor-auswirkungenNachregelungsAlternativen-${item?.id}-chk`}
                onChange={(e) => props.onCheckboxChange(i, item.id, e.target.checked)}
                aria-label={t('auswirkungennachregelungsalternativen.checkboxAriaLabel')}
                aria-expanded={props.activeAlternatives[item.id] ? true : false}
              >
                {item.name}
              </Checkbox>
            </Form.Item>
            <div style={{ display: props.activeAlternatives[item.id] ? 'block' : 'none' }}>
              <fieldset>
                <legend>{t('auswirkungennachregelungsalternativen.subtitle2')}</legend>
                {props.selectedPolitikbereichen.map((category: Politikunterbereich, ii) => {
                  const categoryAlias: string = category.alias as string;
                  const infoTextList: string[] = prepareInfoText(categoryAlias);

                  return (
                    <div id={`checkbox-wrapper-${ii}`} className="checkbox-wrapper" key={ii}>
                      <Form.Item name={`isDirtyIndicator + ${i}`} noStyle>
                        <CheckboxWithInfo
                          wrapperId={`checkbox-wrapper-${ii}`}
                          title={category.name || ''}
                          id={`evor-politikbereiche-${categoryAlias}-chk`}
                          defaultChecked={props.getCheckedValue(i, ii)}
                          onChange={(e: CheckboxChangeEvent) =>
                            props.handlePolitikbereicheCheckboxChange(i, ii, e.target.checked)
                          }
                        >
                          <span className="nonBreakingArea">
                            <span className="normalBreakingArea">{category.name}</span>&nbsp;
                            <InfoComponent title={t(`politikbereichen.${categoryAlias}.infoTitle`)}>
                              {infoTextList.map((text, j) => {
                                return <p key={`${categoryAlias}${j}`}>{t(text)}</p>;
                              })}
                            </InfoComponent>
                          </span>
                        </CheckboxWithInfo>
                      </Form.Item>
                    </div>
                  );
                })}
              </fieldset>
              <FormItemWithInfo
                name={['regelungsDataList', i, 'auswirkungen']}
                className="auswirkungen-holder"
                label={
                  <span>
                    {`Auswirkungen für "${item.name || ''}"`}
                    <InfoComponent title={t('auswirkungennachregelungsalternativen.drawerHeadingTitle1')}>
                      <Title level={3} style={{ marginTop: 0 }}>
                        {t('auswirkungennachregelungsalternativen.drawerHeadingSubtitle')}
                      </Title>
                      <p
                        dangerouslySetInnerHTML={{
                          __html: t('auswirkungennachregelungsalternativen.drawerHeadingContent', {
                            interpolation: { escapeValue: false },
                            link: GGO_LINK + '34',
                          }),
                        }}
                      />
                    </InfoComponent>
                  </span>
                }
              >
                <TextArea rows={4} />
              </FormItemWithInfo>
            </div>
          </li>
        );
      })}
    </ul>
  );
}
