// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Affix, Layout, Spin } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, useRouteMatch } from 'react-router-dom';

import { EvorData } from '@plateg/rest-api';
import {
  HeaderComponent,
  HeaderController,
  LoadingStatusController,
  MenuCollapseController,
  NavbarComponent,
  SiderWrapper,
} from '@plateg/theme';
import { Loading } from '@plateg/theme/src/components/icons/Loading';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { DraftCollection } from '../../types/DraftCollection';
import { MainComponent } from './main/component.react';

const { Header, Content } = Layout;

export function EVOR(): React.ReactElement {
  const { t } = useTranslation();

  const [isCollapsed, setIsCollapsed] = useState<boolean>(false);
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const menuCollapseController = new MenuCollapseController(isCollapsed, setIsCollapsed);
  const headerController = GlobalDI.get<HeaderController>('headerController');
  const [loadingStatus, setLoadingStatus] = useState<boolean>(false);
  const draftCollectionString: string = localStorage.getItem('draftCollection') || '{}';
  const draftCollection: DraftCollection = JSON.parse(draftCollectionString) as DraftCollection;
  const routeMatcherVorbereitung = useRouteMatch<{ id: string }>('/evor/:id');
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [menuOffsetTop, setMenuOffsetTop] = useState<number>(0);
  let currentDraft: EvorData | undefined;

  useEffect(() => {
    headerController.setHeaderProps({
      setMenuOffset: setMenuOffsetTop,
    });
    const subs = loadingStatusController.subscribeLoadingStatus().subscribe({
      next: (state: boolean) => {
        setLoadingStatus(state);
      },
      error: (error) => {
        console.log('Error sub', error);
      },
    });
    loadingStatusController.initLoadingStatus();

    return () => subs.unsubscribe();
  }, []);

  const customTranslation = (name: string, dynamicName?: string) => {
    return dynamicName ? dynamicName : t(`evor.nav.${name}`);
  };

  useEffect(() => {
    menuCollapseController.registerListener();
    return () => menuCollapseController.removeListener();
  }, []);

  useEffect(() => {
    menuCollapseController.updateButtonPosition();
  });

  useEffect(() => {
    menuCollapseController.configureCollapseButton();
  }, [isCollapsed]);

  if (routeMatcherVorbereitung?.params?.id != null) {
    const draftId: string = routeMatcherVorbereitung?.params.id;
    currentDraft = draftCollection[draftId];
  }
  if (!currentDraft) {
    return <Redirect to="/evor" />;
  }
  return (
    <Spin
      {...{ role: loadingStatus ? 'progressbar' : undefined }}
      aria-busy={loadingStatus}
      spinning={loadingStatus}
      size="large"
      tip="Laden..."
      wrapperClassName="loading-screen"
      indicator={<Loading />}
    >
      <Layout
        style={{ height: '100%', display: loadingStatus === true ? 'none' : 'flex' }}
        className="site-layout-background"
      >
        <Header className="header-component site-layout-background">
          <HeaderComponent ctrl={headerController} />
        </Header>

        <Layout id="plateg-content-section" className="has-drawer">
          <SiderWrapper
            breakpoint="lg"
            collapsedWidth="30"
            onCollapse={() => setIsCollapsed(!isCollapsed)}
            collapsible
            width={280}
            collapsed={isCollapsed}
            className="navbar-component"
          >
            <Affix offsetTop={menuOffsetTop}>
              <span>
                {!isCollapsed && (
                  <NavbarComponent
                    form={formInstance}
                    navBarItems={t('evor.navItems', { returnObjects: true })}
                    trans={customTranslation}
                  />
                )}
              </span>
            </Affix>
          </SiderWrapper>
          <Layout className="main-content-area site-layout-background">
            <Content style={{ padding: '48px 0' }}>
              <MainComponent
                currentDraft={currentDraft}
                formInstance={formInstance}
                setFormInstance={setFormInstance}
              />
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </Spin>
  );
}
