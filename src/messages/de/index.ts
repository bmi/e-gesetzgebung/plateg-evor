// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const de = {
  startseite: {
    mainTitle: 'Vorbereitung',
    subTitle: 'Die Anwendung zur Vorbereitung von Regelungsentwürfen',
    newDraftTitle: 'Neues Vorhaben beginnen',
    lastModifiedTitle: 'Ihre Vorhaben',
    newDraftSubtitle: 'Name des Vorhabens',
    btnNewProject: 'Neues Vorhaben beginnen',
    btnLoadProject: 'Vorhaben laden',
    btnStartProject: 'Vorhaben beginnen',
    btnContinue: 'Fortsetzen',
    btnSaveLocally: 'Lokal speichern',
    lastModified: 'Zuletzt bearbeitet:',
    btnAbort: 'Abbrechen',
    tipTitle: 'Hinweis',
    tooltipText:
      'Hier können Sie ihr Vorhaben lokal auf ihrem Computer speichern und ' +
      'später über „Vorhaben laden“ weiter bearbeiten. Nur ein Vorhaben wird automatisch ' +
      'im Browser gespeichert.',
    tipText:
      'Sie können immer nur ein Vorhaben im Browser bearbeiten. Wenn Sie Ihr zuletzt ' +
      'bearbeitetes Vorhaben auf ihrem Computer speichern möchten, um es später weiter zu ' +
      'bearbeiten, nutzen Sie die Funktion „Lokal speichern“.',
    infoText:
      'Diese Anwendung unterstützt Sie als federführendes Ressort bei der ' +
      'inhaltlichen Vorbereitung eines neuen Regelungsentwurfs. Die Anwendung begleitet Sie ' +
      'bei der Analyse des Regelungsfeldes, bei der Operationalisierung von Zielen, der ' +
      'Entwicklung von Regelungsalternativen und der Abschätzung ihrer Vor- und Nachteile. Sie ' +
      'basiert auf der vom Bundesministerium des Innern und für Heimat erstellten ' +
      '<a href="{{basePath}}/arbeitshilfen/download/11" target="_blank" style="white-space: normal">Arbeitshilfe zur Gesetzesfolgenabschätzung</a>.',
    projectNamePlaceholder: 'Namen für neuen Regelungsentwurf eingeben',
    iconBtnDelete: 'Regelungsentwurf löschen',
    deleteConfirmation: {
      title: 'Möchten Sie das Vorhaben löschen?',
      warningText: 'Achtung, wenn Sie das Vorhaben löschen, ist es nicht mehr verfügbar.',
      btnDelete: 'Löschen',
      btnCancel: 'Abbrechen',
    },
    lastEdited: 'Zuletzt bearbeitet: {{date}} Uhr',
  },
  evor: {
    header: {
      linkPDF: 'PDF-Export',
      linkBack: 'Zurück',
      linkExport: 'Exportieren',
      linkSave: 'Lokal speichern',
      linkLoad: 'Laden',
      linkHelp: 'Hilfe',
      linkHome: 'Vorbereitung',
    },
    breadcrumbs: {
      analyse: 'Angaben für Analyse des Regelungsfeldes',
      zielAnalyse: 'Angaben für Zielanalyse',
      entwicklungIdeen: 'Angaben für Entwicklung von Regelungsalternativen',
      entwicklungAlternativen: 'Angaben für Entwicklung von Regelungsalternativen',
      bewertungAuswirkungenPolitik: 'Angaben für Prüfung und Bewertung von Regelungsalternativen',
      bewertungAuswirkungenNachAlternative: 'Angaben für Prüfung und Bewertung von Regelungsalternativen',
      bewertungBewertung: 'Angaben für Prüfung und Bewertung von Regelungsalternativen',
      konsultationVorbereitung: 'Angaben für Konsultation',
      konsultationErgebnisse: 'Angaben für Konsultation',
      fazit: 'Angaben für Fazit',
      ergebnisdokumentation: 'Vorschau Ergebnisdokumentation',
    },
    main: {
      title: 'Hello World',
    },
    navItems: [
      {
        menuKeys: [{ key: 'analyse' }, { key: 'zielAnalyse' }],
      },
      {
        submenuEntry: {
          key: 'entwicklung',
          isLink: false,
        },
        menuKeys: [
          { key: 'entwicklung.entwicklungIdeen', dynamicName: 'Ideen sammeln' },
          { key: 'entwicklung.entwicklungAlternativen', dynamicName: 'Alternativen zusammenstellen' },
        ],
      },
      {
        submenuEntry: {
          key: 'bewertung',
          isLink: false,
        },
        menuKeys: [
          { key: 'bewertung.bewertungAuswirkungenPolitik', dynamicName: 'Auswirkungen in Politikbereichen' },
          {
            key: 'bewertung.bewertungAuswirkungenNachAlternative',
            dynamicName: 'Auswirkungen nach Regelungsalternativen',
          },
          { key: 'bewertung.bewertungBewertung', dynamicName: 'Bewertung' },
        ],
      },
      {
        submenuEntry: {
          key: 'konsultation',
          isLink: false,
        },
        menuKeys: [
          { key: 'konsultation.konsultationVorbereitung', dynamicName: 'Vorbereitung und Durchführung' },
          { key: 'konsultation.konsultationErgebnisse', dynamicName: 'Ergebnisse' },
        ],
      },
      {
        menuKeys: [{ key: 'fazit' }, { key: 'ergebnisdokumentation' }],
      },
    ],
    nav: {
      analyse: 'Analyse des Regelungsfeldes',
      zielAnalyse: 'Zielanalyse',
      entwicklung: 'Entwicklung von Regelungsalternativen',
      bewertung: 'Prüfung und Bewertung von Regelungsentwürfen',
      konsultation: 'Konsultation',
      fazit: 'Fazit',
      ergebnisdokumentation: 'Ergebnisdokumentation',
    },
    continueLater: {
      title: 'Bearbeitung später fortsetzen?',
      btnText: 'Zwischenspeichern',
      successMsg: 'Ihre Vorhaben wurde erfolgreich gespeichert.',
    },
  },
  form: {
    btnNext: 'Nächster Schritt',
    btnPrev: 'Vorheriger Schritt',
  },
  analyseRegelungsfeldes: {
    title: 'Analyse des Regelungsfeldes',
    info: 'Analysieren Sie in einer Problemanalyse und Akteurs- und Systemanalyse das Regelungsfeld, um die Grundlagen der Problembewältigung zu legen und die Regelungsnotwendigkeit zu begründen.',
    drawerAnalyseTitle: 'Analyse des Regelungsfeldes',
    drawerAnalyseIntro: 'Die Analyse des Regelungsfeldes beinhaltet:',
    drawerAnalyseProblem1: '1. Problemanalyse und Problemabgrenzung (inklusive Ursachen)',
    drawerAnalyseProblem2: '2. Systemanalyse (inklusive Akteurskonstellation)',
    drawerAnalyseText:
      'Es ist zu bedenken, dass mit diesen Analysen das Regelungsvorhaben gerechtfertigt, ' +
      'die Regelungsnotwendigkeit begründet und die Grundlagen der Problembewältigung gelegt werden.',
    fromProblemanalyseLable: 'Problemanalyse',
    formSystemanalyseLable: 'Akteurs- und Systemanalyse',
    drawerProblemAnalyseTitle: 'Problemanalyse',
    drawerProblemAnalyseText:
      'Zum einen ist das Problem, welches dem Regelungsvorhaben zugrunde liegt, genau zu ' +
      'analysieren. Je gewissenhafter dies erfolgt, umso eher lässt sich das Erfordernis ' +
      'einer Regelung rechtfertigen beziehungsweise die Notwendigkeit einer Regelung begründen.',
    drawerProblemAnalyseText2:
      'Die im Regelungsfeld auftretenden Probleme sollten soweit als möglich eingegrenzt ' +
      'und bestehende Missstände, welche ohne das Regelungsvorhaben nicht beseitigt werden ' +
      'können, analysiert und dokumentiert werden. Dabei sollten möglichst auch die ' +
      'Problemursachen herausgearbeitet werden.',
    drawerSystemAnalyseTitle: 'Akteurs- und Systemanalyse',
    drawerSystemAnalyseText:
      'Das Regelungsfeld sollte zum anderen in seinen systemischen Zusammenhängen analysiert ' +
      'werden. So sollten die Zusammenhänge zwischen verschiedenen Teilen des Regelungsfeldes ' +
      'fokussiert, analysiert und beschrieben werden. Dies umfasst die Analyse von Bereichen ' +
      'wie Wirtschaft, Soziales und Umwelt sowie der beteiligten Akteurinnen und Akteure.',
    drawerSystemAnalyseText2:
      'Auf der Basis dieser Analysen sollte sich – als Grundlage für das weitere Vorgehen – ein ' +
      'umfassendes Bild der Regelungsnotwendigkeit ergeben.',
  },
  zielanalyse: {
    title: 'Zielanalyse und Zieldefinition',
    info: 'Beschreiben und operationalisieren Sie die Ziele der Regelung.',
    subtitle1: 'Zielanalyse',
    goalRegelungsvorhaben: 'Ziele der Regelungsvorhaben',
    subtitle2: 'Zieldefinition',
    goalDefinitionInfo:
      'Listen Sie die Ziele des Regelungsvorhabens stichpunktartig auf, sodass Sie mit diesen ' +
      'im weiteren Verlauf der Alternativenfindung und -bewertung weiterarbeiten können:',
    drawerZielanalyseTitle: 'Zielanalyse',
    drawerZielanalyseContent1:
      'Das angestrebte Ziel sollte so genau wie möglich beschrieben werden. Dies umfasst neben ' +
      'dem eigentlichen Ziel des Regelungsvorhabens auch die angestrebten Verhaltensänderungen ' +
      'der Normzielgruppe sowie die daraus resultierenden beabsichtigten Wirkungen.',
    drawerZielanalyseContent2:
      'In der Regel werden Ziele eines Regelungsvorhabens von der Leitung eines jeweiligen ' +
      'Ressorts vorgegeben oder gebilligt. Im nächsten Schritt, der Zieldefinition, sollten ' +
      'diese Zielvorgaben operationalisiert, also messbar formuliert werden. Es stellt sich ' +
      'also die Frage, wie die Ziele so formuliert werden können, dass im weiteren Verlauf ' +
      'der Folgenabschätzung eine Aussage zum Zielerreichungsgrad unterschiedlicher ' +
      'Regelungsalternativen getroffen werden kann.',
    drawerZieleTitle: 'Beschreiben Sie die Ziele des Regelungsvorhabens',
    drawerZieleContent1: 'Was ist das Ziel der Regelung?',
    drawerZieleContent2: 'Welche Verhaltensänderungen sollen bei den Normzielgruppen herbeigeführt werden?',
    drawerZieleContent3:
      'Welche weiterführenden Wirkungen (zum Beispiel Auswirkungen auf Wirtschaft, Soziales und Umwelt) werden angestrebt?',
    drawerZieleContent4: 'Können eindeutige Zielvorgaben gemacht werden?',
    drawerZieleContent5:
      'Müssen die Ziele „weicher“, unspezifischer formuliert werden? Wie können Toleranzen konkretisiert werden?',
    drawerZieldefinitionTitle: 'Zieldefinition',
    drawerZieldefinitionContent1:
      'Es empfiehlt sich, bereits in dieser Phase abzuwägen, in welcher Form die Zielvorgabe ' +
      'in den späteren Regelungsentwurf aufgenommen werden kann. In Frage kommen zum Beispiel:',
    drawerZieldefinitionContent2: 'eindeutige Zielvorgaben',
    drawerZieldefinitionContent3:
      '„weiche“, unspezifische Zielvorgaben (zum Beispiel spürbare Entlastungen der Bürgerinnen ' +
      'und Bürger, deutliche Verbesserung der Vermittlungstätigkeit). Mit diesen unspezifischen ' +
      'Zielvorgaben sind Toleranzen und „Spielräume“ in der Zielerreichung verbunden, welche in ' +
      'der Zielbeschreibung möglichst konkretisiert werden sollten.',
    validationMessage: 'Bitte geben Sie einen Titel für das Ziel {{index}} ein oder entfernen Sie dieses Ziel.',
  },
  regelungsalternativen: {
    title: 'Prüfung und Bewertung von Regelungsalternativen',
  },
  politikbereichen: {
    title: 'Auswirkungen in Politikbereichen',
    intro: 'Ihre Regelungsalternativen:',
    title2: 'In welchen Bereichen sind wesentliche Auswirkungen des Regelungsvorhabens zu erwarten?',
    text2:
      'Wählen Sie nun die Bereiche aus, die durch das Regelungsvorhaben wesentlich betroffen sind. ' +
      'Dazu gehören auch solche, die nicht von allen Regelungsalternativen, aber mindestens von einer ' +
      'betroffen sind. Überall dort, wo gravierende Auswirkungen zu erwarten sind, sollten diese ' +
      'eingehend geprüft werden. Für die Ermittlung und Prüfung der gravierenden Auswirkungen wurden ' +
      'gemeinsam mit verschiedenen Bundesressorts Prüffragen entwickelt, die bei den Politikbereichen ' +
      'jeweils durch Klick auf den Info-Button eingeblendet werden können. Im nächsten Schritt haben ' +
      'Sie die Möglichkeit, die Auswirkungen der einzelnen Alternativen genauer zu beschreiben.',
    infoAreasTitle: 'In welchen Bereichen sind wesentliche Auswirkungen des Regelungsvorhabens zu erwarten?',
    infoAreasText1:
      'Die Prüffragen für jeden der Politikbereiche dienen dazu festzustellen, ob das Regelungsvorhaben ' +
      'Auswirkungen auf einen Politikbereich hat. In der Regel wird nur ein Teil der genannten Themenfelder ' +
      'relevant sein, dennoch helfen die Prüffragen, nicht sofort offensichtliche Folgen (zum Beispiel ' +
      'Nebenwirkungen) zu erkennen.',
    noRegelungsalternativenText:
      'Es wurden keine Regelungsalternativen angelegt. Dies kann im Schritt „Alternativen zusammenstellen“ vorgenommen werden.',

    finance: {
      categoryTitle: 'Finanzen, Steuern und Zölle',
      finanzmarktpolitik: {
        label: 'Finanzmarktpolitik',
        infoTitle: 'Finanzmarktpolitik',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Finanzmarktpolitik auswählen.',
        infoText2:
          'Tangiert das Vorhaben die Wettbewerbsfähigkeit, Märkte, Handel und Investitionsströme (u.a. Vorteile oder Nachteile für den Wirtschaftsstandort Deutschland, Chancen bestimmter Produkte am Markt, etc.)?',
        infoText3:
          'Sind schwerwiegende volkswirtschaftliche Vor- oder Nachteile zu erwarten (Makroökomische Betrachtung eventueller Folgen und Risiken)?',
      },
      steuern: {
        label: 'Steuern (ohne Energie- und Verbrauchsteuern)',
        infoTitle: 'Steuern (ohne Energie- und Verbrauchsteuern)',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Steuern (ohne Energie- und Verbrauchsteuern) auswählen.',
        infoText2:
          'Hat der Entwurf Auswirkungen auf Steuern und andere Abgaben (u.a. Steuererhöhungen oder -senkungen, Sozialversicherungsbeiträge, sonstige Abgaben, etc.)?',
        infoText3:
          'Sind schwerwiegende volkswirtschaftliche Vor- oder Nachteile zu erwarten (Makroökomische Betrachtung eventueller Folgen und Risiken)?',
      },
      versicherungenundfinanzen: {
        label: 'Versicherungen und Finanzen',
        infoTitle: 'Versicherungen und Finanzen',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Versicherungen und Finanzen auswählen.',
        infoText2:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. Arbeitsplatzabbau, Unternehmensinsolvenzen, Kapitalmarktrisiken, volkswirtschaftliche Risiken, Fachkräftemangel, etc.), die zu einer Gefährdung der Wirtschaft bei Eintritt des Risikos führen?',
      },
      zolleenergieverbrauchsteuern: {
        label: 'Zölle, Energie- und Verbrauchsteuern',
        infoTitle: 'Zölle, Energie- und Verbrauchsteuern',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Zölle, Energie- und Verbrauchsteuern auswählen.',
        infoText2:
          'Hat der Entwurf Auswirkungen auf Steuern und andere Abgaben (u.a. Steuererhöhungen oder -senkungen, Sozialversicherungsbeiträge, sonstige Abgaben, etc.)?',
      },
      finanzengrundsatz: {
        label: 'Finanzen (Grundsatz)',
        infoTitle: 'Finanzen (Grundsatz)',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Finanzen (Grundsatz) auswählen.',
        infoText2:
          'Sind schwerwiegende volkswirtschaftliche Vor- oder Nachteile zu erwarten (Makroökomische Betrachtung eventueller Folgen und Risiken)?',
      },
    },

    innereskategorie: {
      categoryTitle: 'Inneres',
      inneres: {
        label: 'Inneres (Gesellschaft, Verfassung, Verwaltung, Öffentlicher Dienst)',
        infoTitle: 'Inneres (Gesellschaft, Verfassung, Verwaltung, Öffentlicher Dienst)',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Inneres (Gesellschaft, Verfassung, Verwaltung, Öffentlicher Dienst) auswählen.',
        infoText2:
          'Sind Auswirkungen auf die soziale Einbeziehung bzw. den Schutz bestimmter gesellschaftlicher Gruppen zu erwarten (z.B. Minderheitenschutz, Antidiskriminierung, etc.)?',
        infoText3:
          'Werden Belange des Datenschutzes berührt (u.a. verbesserter oder verschlechterter Schutz personenbezogener Daten vor unberechtigter bzw. unsachgemäßer Nutzung, etc.)?',
        infoText4:
          'Werden Belange des Sports berührt (z.B. Sportausübung, Förderung des Spitzensports, Sportanlagenbau, etc.)?',
        infoText5:
          'Werden Angelegenheiten der Kirchen oder  Religionsgemeinschaften berührt (u.a. bei der freien Religionsausübung, bei ihrer caritativen und gesellschaftspolitischen Tätigkeit in ihrer Eigenschaft als Trägerin, Träger, Arbeitgeberin oder Arbeitgeber bestimmter Einrichtungen wie z.B. Schulen, Pflegeeinrichtungen, Kindergärten, Krankenhäuser sowie bei der Ausübung dieser Tätigkeiten)?',
        infoText6:
          'Hat der Entwurf Auswirkungen auf den Zugang zum Bildungssektor (z.B. freier Zugang zu Bildungseinrichtungen und Fortbildungsangeboten.)?',
        infoText7:
          'Hat der Entwurf Auswirkungen auf Behandlungs- und Chancengleichheit (Nicht-Diskriminierung z.B. wegen des Geschlechts oder wegen einer körperlichen oder geistigen Beeinträchtigung)?',
        infoText8:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. sozialer Auf- oder Abstieg, Verbesserung oder Verminderung von sozialer Teilhabe oder Bildungsmöglichkeiten), die zu einer verstärkten oder verminderten sozialen Qualität bei Eintritt des Risikos führen?',
        infoText9: 'Sind durch das Vorhaben Aspekte der ehrenamtlichen Tätigkeit berührt?',
      },

      migrationundintegration: {
        label: 'Migration und Integration',
        infoTitle: 'Migration und Integration',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Migration und Integration auswählen.',
        infoText2: 'Hat das Vorhaben Auswirkungen auf das Zusammenleben mit Migrantinnen und Migranten?',
        infoText3:
          'Sind Auswirkungen auf die soziale Einbeziehung bzw. den Schutz bestimmter gesellschaftlicher Gruppen zu erwarten (z.B. Minderheitenschutz, Antidiskriminierung, etc.)?',
      },

      statistik: {
        label: 'Statistik',
        infoTitle: 'Statistik',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Statistik auswählen.',
        infoText2: 'Für diesen Bereich liegen keine Prüffragen vor.',
      },

      sicherheit: {
        label: 'Sicherheit',
        infoTitle: 'Sicherheit',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Sicherheit auswählen.',
        infoText2: 'Werden Aspekte der inneren Sicherheit berührt?',
        infoText3: 'Werden Aspekte der Abwehr von (organisierter) Kriminalität berührt?',
        infoText4: 'Werden Belange der Terrorabwehr berührt?',
        infoText5: 'Hat das Vorhaben Auswirkung auf Aspekte der elektronischen Identifizierung und der IT Sicherheit?',
        infoText6: 'Hat das Vorhaben Auswirkungen auf den Zivil- und Bevölkerungsschutz im Krisenfalle?',
        infoText7:
          'Werden Belange des Datenschutzes berührt (u.a. verbesserter oder verschlechterter Schutz personenbezogener Daten vor unberechtigter bzw. unsachgemäßer Nutzung, etc.)?',
      },
    },

    internationales: {
      categoryTitle: 'Internationales',
      auswartigerdienst: {
        label: 'Auswärtiger Dienst',
        infoTitle: 'Auswärtiger Dienst',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Auswärtiger Dienst auswählen.',
        infoText2:
          'Hat der Entwurf wirtschaftliche Folgen, die sich auch auf andere Staaten auswirken (internationale bzw. globale Dimension)?',
        infoText3:
          'Hat der Entwurf Folgen im Umweltbereich, die sich auch auf andere Staaten (internationale bzw. globale Dimension) auswirken (z.B. Verknappung der Lebensmittelverfügbarkeit im Ausland wegen des Anbaus von Energiepflanzen für den Export)?',
        infoText4:
          'Hat der Entwurf soziale Folgen, die sich auch auf andere Staaten auswirken (internationale bzw. globale Dimension)?',
        infoText5:
          'Werden Aspekte der Verteidigung oder der militärischen Einsatzbereitschaft der Bundeswehr im In- und Ausland berührt (z.B. Rüstungsprojekte, finanzielle und materielle Ausstattung der Bundeswehr, Kompetenzen der Bundeswehr für Einsätze im Ausland bzw. in Krisengebieten, etc.)?',
      },
      wirtschaftlichezusammenarbeitundentwicklung: {
        label: 'Wirtschaftliche Zusammenarbeit und Entwicklung',
        infoTitle: 'Wirtschaftliche Zusammenarbeit und Entwicklung',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Wirtschaftliche Zusammenarbeit und Entwicklung auswählen.',
        infoText2:
          'Hat der Entwurf wirtschaftliche Folgen, die sich auch auf andere Staaten auswirken (internationale bzw. globale Dimension)?',
        infoText3:
          'Sind durch das Vorhaben Beiträge zu Veränderungen des globalen Klimas zu erwarten (z.B. Verstärkung oder Minderung des Treibhauseffektes, Ozonausstoß, etc.)?',
        infoText4:
          'Hat der Entwurf Folgen im Umweltbereich, die sich auch auf andere Staaten (internationale bzw. globale Dimension) auswirken (z.B. Verknappung der Lebensmittelverfügbarkeit im Ausland wegen des Anbaus von Energiepflanzen für den Export)?',
        infoText5:
          'Hat der Entwurf soziale Folgen, die sich auch auf andere Staaten auswirken (internationale bzw. globale Dimension)?',
        infoText6: 'Sind Belange von entwicklungspolitischer Bedeutung tangiert?',
      },
    },

    verteidigungkategorie: {
      categoryTitle: 'Verteidigung',
      verteidigung: {
        label: 'Verteidigung',
        infoTitle: 'Verteidigung',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Verteidigung auswählen.',
        infoText2:
          'Werden Aspekte der Verteidigung oder der militärischen Einsatzbereitschaft der Bundeswehr im In- und Ausland berührt (z.B. Rüstungsprojekte, finanzielle und materielle Ausstattung der Bundeswehr, Kompetenzen der Bundeswehr für Einsätze im Ausland bzw. in Krisengebieten, etc.)?',
      },
    },

    wirtschaftundverbraucherschutz: {
      categoryTitle: 'Wirtschaft und Verbraucherschutz',
      wirtschaft: {
        label: 'Wirtschaft',
        infoTitle: 'Wirtschaft',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Wirtschaft auswählen.',
        infoText2:
          'Hat die vorgesehene Norm Auswirkungen auf Eigentumsrechte von juristischen Personen bzw. Wirtschaftssubjekten (u.a. Wirtschaftsunternehmen, Vereine, etc.)?',
        infoText3:
          'Tangiert das Vorhaben die Wettbewerbsfähigkeit, Märkte, Handel und Investitionsströme (u.a. Vorteile oder Nachteile für den Wirtschaftsstandort Deutschland, Chancen bestimmter Produkte am Markt, etc.)?',
        infoText4:
          'Werden durch den Entwurf Belange von wirtschafts- und technologiepolitischer Bedeutung berührt (z.B. Entwicklung neuer Technologien bis zur Marktreife, wirtschaftsrelevante Innovationen)?',
        infoText5:
          'Hat der Entwurf Auswirkungen auf die Kosten für Wirtschaftsunternehmen und das Verhalten von Unternehmen am Markt (z.B. Auswirkungen auf fixe und variable Kosten für Unternehmen, Förderung von bestimmten Unternehmen am deutschen Markt)?',
        infoText6: 'Hat der Entwurf Auswirkungen auf administrative Kosten der Unternehmen (Verwaltungskosten)?',
        infoText7:
          'Sind Vor- oder Nachteile für bestimmte (Wirtschafts-) Regionen oder Sektoren zu erwarten (u.a. Vor- und Nachteile für bestimmte industrielle Ballungszentren oder bestimmte Industriesektoren wie Schwerindustrie, chemische Industrie, Bergbau, etc.)?',
        infoText8:
          'Sind schwerwiegende volkswirtschaftliche Vor- oder Nachteile zu erwarten (Makroökomische Betrachtung eventueller Folgen und Risiken)?',
        infoText9:
          'Werden Aspekte der Beschäftigung und der Arbeitsmärkte berührt (z.B. Kommt es möglicherweise zur Schaffung oder zum Abbau von Arbeitsplätzen? Kommt es zu veränderten rechtlichen oder wirtschaftlichen Rahmenbedingungen für Beschäftigungsverhältnisse?)?',
        infoText10:
          'Sind Auswirkungen auf das Innovationsverhalten der Unternehmen zu erwarten (z.B. Mehr- oder Minderausgaben von Wirtschaftsunternehmen, Produktinnovationen)?',
        infoText11:
          'Hat der Entwurf wirtschaftliche Folgen, die sich auch auf andere Staaten auswirken (internationale bzw. globale Dimension)?',
        infoText12:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. Arbeitsplatzabbau, Unternehmensinsolvenzen, Kapitalmarktrisiken, volkswirtschaftliche Risiken, etc.), die zu einer verstärkten oder verminderten Gefährdung der Wirtschaft bei Eintritt des Risikos führen?',
        infoText13: 'Wird durch das Vorhaben die Effizienz des Einsatzes von Ressourcen gesteigert oder vermindert?',
        infoText14:
          'Werden Aspekte der Arbeitnehmerinnen und der Arbeitnehmer, Beschäftigung und der Arbeitsmärkte berührt (z.B. Sicherung oder Abbau von Arbeitsplätzen, positive oder negative Veränderung der Bedingungen am Arbeitsmarkt aus Sicht der Arbeitnehmerinnen und der Arbeitnehmer, Förderung neuer Beschäftigungsverhältnisse, etc.)?',
        infoText15:
          'Sind Vor- oder Nachteile für bestimmte Regionen oder Räume zu erwarten (u.a. Raumnutzung, soziale Infrastruktur (Kindertagesstätten, Jugendeinrichtungen, etc.), Gentrification, Segregation, sozialer Auf- oder Abstieg bestimmter Regionen und Quartiere, etc.)?',
      },
      verbraucherschutz: {
        label: 'Verbraucherschutz',
        infoTitle: 'Verbraucherschutz',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Verbraucherschutz auswählen.',
        infoText2:
          'Führt das Vorhaben zu Veränderungen der Sicherheit von Nahrungs- und Futtermitteln (Nahrungskette, Nahrungspflanzen, tierische Erzeugnisse, Wild- und Futterpflanzen, Beutetieren)?',
        infoText3:
          'Werden Belange des Datenschutzes berührt (u.a. verbesserter oder verschlechterter Schutz personenbezogener Daten vor unberechtigter bzw. unsachgemäßer Nutzung, etc.)?',
      },
    },

    energie: {
      categoryTitle: 'Energie',
      strahlenschutzatomenergie: {
        label: 'Strahlenschutz, Atomenergie',
        infoTitle: 'Strahlenschutz, Atomenergie',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Strahlenschutz, Atomenergie auswählen.',
        infoText2: 'Hat der Entwurf Auswirkungen auf Energieverbrauch (z.B. Mehrverbrauch von Energieressourcen)?',
        infoText3:
          'Hat der Entwurf Auswirkungen auf erneuerbare und nichterneuerbare Energien (u.a. Förderung von erneuerbaren Energien, Mehrverbrauch nichterneuerbarer Energien, etc.)?',
        infoText4:
          'Führt das Vorhaben zu einem erhöhten oder verminderten Einsatz von nicht erneuerbaren Ressourcen (z.B. Erdöl, Mineralien, Erze)?',
        infoText5:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. unbeabsichtigte Freisetzungen von Schadstoffen, Strahlung, Lärm, Wärme; Störfälle; Havarien; Anfälligkeit für Anschläge bei umweltrelevanten Anlagen), die zu einer Gefährdung der Umwelt bei Eintritt des Risikos führen?',
      },
      energiegrundsatz: {
        label: 'Energie (Grundsatz)',
        infoTitle: 'Energie (Grundsatz)',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Energie (Grundsatz) auswählen.',
        infoText2: 'Hat der Entwurf Auswirkungen auf Energieverbrauch (z.B. Mehrverbrauch von Energieressourcen)?',
        infoText3:
          'Hat der Entwurf Auswirkungen auf erneuerbare und nichterneuerbare Energien (u.a. Förderung von erneuerbaren Energien, Mehrverbrauch nichterneuerbarer Energien, etc.)?',
        infoText4:
          'Wird durch das Vorhaben die Effizienz des Einsatzes von Energie gesteigert oder vermindert (z.B. Energiesparprojekte)?',
      },
    },
    klimakategorie: {
      categoryTitle: 'Klima',
      klima: {
        label: 'Klima',
        infoTitle: 'Klima',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Klima auswählen.',
        infoText2: 'Hat der Entwurf Auswirkungen auf Energieverbrauch (z.B. Mehrverbrauch von Energieressourcen)?',
        infoText3:
          'Hat der Entwurf Auswirkungen auf erneuerbare und nichterneuerbare Energien (u.a. Förderung von erneuerbaren Energien, Mehrverbrauch nichterneuerbarer Energien, etc.)?',
        infoText4:
          'Sind durch das Vorhaben Beiträge zu Veränderungen des globalen Klimas zu erwarten (z.B. Verstärkung oder Minderung des Treibhauseffektes, Ozonausstoß, etc.)?',
        infoText5:
          'Führt das Vorhaben zu einem erhöhten oder verminderten Einsatz von erneuerbaren Ressourcen, deren Bestandsentwicklung einen Abwärtstrend aufweist (z.B. Fischbestände)?',
        infoText6:
          'Führt das Vorhaben zu einem erhöhten oder verminderten Einsatz von nicht erneuerbaren Ressourcen (z.B. Erdöl, Mineralien, Erze)?',
        infoText7:
          'Wird durch das Vorhaben die Effizienz des Einsatzes von Energie gesteigert oder vermindert (z.B. Energiesparprojekte)?',
        infoText8: 'Wird durch das Vorhaben die Effizienz des Einsatzes von Ressourcen gesteigert oder vermindert?',
      },
    },
    digitalisierungundtechnologie: {
      categoryTitle: 'Digitalisierung und Technologie',
      digitales: {
        label: 'Digitales',
        infoTitle: 'Digitales',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Digitales auswählen.',
        infoText2: 'Für diesen Bereich liegen keine Prüffragen vor.',
      },
      technologie: {
        label: 'Technologie',
        infoTitle: 'Technologie',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Technologie auswählen.',
        infoText2:
          'Werden durch den Entwurf Belange von wirtschafts- und technologiepolitischer Bedeutung berührt (z.B. Entwicklung neuer Technologien bis zur Marktreife, wirtschaftsrelevante Innovationen)?',
        infoText3:
          'Sind Auswirkungen auf das Innovationsverhalten der Unternehmen zu erwarten (z.B. Mehr- oder Minderausgaben von Wirtschaftsunternehmen, Produktinnovationen)?',
        infoText4:
          'Sind Auswirkungen auf die Nutzung innovativer Lösungen/neuer Technologien zu erwarten (z.B. werden explizit Innovationen und/oder neue Technologien gefördert oder in ihrer Entwicklung gehemmt)?',
      },
      digitalegesellschaft: {
        label: 'Digitale Gesellschaft',
        infoTitle: 'Digitale Gesellschaft',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Digitale Gesellschaft auswählen.',
        infoText2:
          'Werden Belange des Datenschutzes berührt (u.a. verbesserter oder verschlechterter Schutz personenbezogener Daten vor unberechtigter bzw. unsachgemäßer Nutzung, etc.)?',
        infoText3: 'Hat das Vorhaben Auswirkung auf Aspekte der elektronischen Identifizierung und der IT Sicherheit?',
        infoText4:
          'Hat das Vorhaben Auswirkungen auf die Nutzung moderner IT-Techniken und des E-Government (elektronische Verwaltungs- und Beteiligungsprozesse)?',
      },
    },

    justizkategorie: {
      categoryTitle: 'Justiz',
      justiz: {
        label: 'Justiz',
        infoTitle: 'Justiz',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Justiz auswählen.',
        infoText2:
          'Hat die vorgesehene Norm Auswirkungen auf Eigentumsrechte von juristischen Personen bzw. Wirtschaftssubjekten (u.a. Wirtschaftsunternehmen, Vereine, etc.)?',
        infoText3:
          'Hat die vorgesehene Norm Auswirkungen auf Eigentumsrechte von Privatpersonen (u.a. Erbrecht, Grundrecht, etc.)?',
        infoText4:
          'Hat der Entwurf Auswirkungen auf Behandlungs- und Chancengleichheit (Nicht-Diskriminierung z.B. wegen des Geschlechts oder wegen einer körperlichen oder geistigen Beeinträchtigung)?',
        infoText5: 'Werden Aspekte der inneren Sicherheit berührt?',
        infoText6: 'Werden Aspekte der Abwehr von (organisierter) Kriminalität berührt?',
      },
    },

    arbeitundsoziales: {
      categoryTitle: 'Arbeit und Soziales',
      arbeitsmarkt: {
        label: 'Arbeitsmarkt',
        infoTitle: 'Arbeitsmarkt',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Arbeitsmarkt auswählen.',
        infoText2:
          'Werden Aspekte der Beschäftigung und der Arbeitsmärkte berührt (z.B. Kommt es möglicherweise zur Schaffung oder zum Abbau von Arbeitsplätzen? Kommt es zu veränderten rechtlichen oder wirtschaftlichen Rahmenbedingungen für Beschäftigungsverhältnisse?)?',
        infoText3:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. Arbeitsplatzabbau, Unternehmensinsolvenzen, Kapitalmarktrisiken, volkswirtschaftliche Risiken, etc.), die zu einer verstärkten oder verminderten Gefährdung der Wirtschaft bei Eintritt des Risikos führen?',
        infoText4:
          'Werden Aspekte der Arbeitnehmerinnen und der Arbeitnehmer, Beschäftigung und der Arbeitsmärkte berührt (z.B. Sicherung oder Abbau von Arbeitsplätzen, positive oder negative Veränderung der Bedingungen am Arbeitsmarkt aus Sicht der Arbeitnehmerinnen und der Arbeitnehmer, Förderung neuer Beschäftigungsverhältnisse, etc.)?',
      },
      belangebehindertermenschen: {
        label: 'Belange behinderter Menschen',
        infoTitle: 'Belange behinderter Menschen',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Belange behinderter Menschen auswählen.',
        infoText2: 'Sind Belange von behinderten Personen berührt (z.B. Antidiskriminierung, Barrierefreiheit, etc.)?',
        infoText3:
          'Hat der Entwurf Auswirkungen auf den Zugang zum Gesundheitssektor (z.B. uneingeschränkter Zugang zu Ärztinnen und Ärzten, zu Krankenhäusern, zu Gesundheitsprävention, etc.)?',
        infoText4:
          'Hat der Entwurf Auswirkungen auf Behandlungs- und Chancengleichheit (Nicht-Diskriminierung z.B. wegen des Geschlechts oder wegen einer körperlichen oder geistigen Beeinträchtigung)?',
      },
      rehabilitation: {
        label: 'Rehabilitation',
        infoTitle: 'Rehabilitation',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Rehabilitation auswählen.',
        infoText2: 'Für diesen Bereich liegen keine Prüffragen vor.',
      },
      sozialhilfe: {
        label: 'Sozialhilfe',
        infoTitle: 'Sozialhilfe',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Sozialhilfe auswählen.',
        infoText2:
          'Hat das Vorhaben Auswirkungen auf die Gewährung, Art und Umfang sozialer staatlicher Transferleistungen (z.B. ALG I und II, Sozialhilfe, etc.)?',
      },
      sozialversicherung: {
        label: 'Sozialversicherung (Arbeit und Soziales)',
        infoTitle: 'Sozialversicherung (Arbeit und Soziales)',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Sozialversicherung (Arbeit und Soziales) auswählen.',
        infoText2:
          'Hat der Entwurf Auswirkungen auf Steuern und andere Abgaben (u.a. Steuererhöhungen oder -senkungen, Sozialversicherungsbeiträge, sonstige Abgaben, etc.)?',
      },
      arbeitundsozialesgrundsatz: {
        label: 'Arbeit und Soziales (Grundsatz)',
        infoTitle: 'Arbeit und Soziales (Grundsatz)',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Arbeit und Soziales (Grundsatz) auswählen.',
        infoText2:
          'Sind Vor- oder Nachteile für bestimmte (Wirtschafts-) Regionen oder Sektoren zu erwarten (u.a. Vor- und Nachteile für bestimmte industrielle Ballungszentren oder bestimmte Industriesektoren wie Schwerindustrie, chemische Industrie, Bergbau, etc.)?',
        infoText3:
          'Sind schwerwiegende volkswirtschaftliche Vor- oder Nachteile zu erwarten (Makroökomische Betrachtung eventueller Folgen und Risiken)?',
        infoText4:
          'Werden Aspekte der Arbeitnehmerinnen und der Arbeitnehmer, Beschäftigung und der Arbeitsmärkte berührt (z.B. Sicherung oder Abbau von Arbeitsplätzen, positive oder negative Veränderung der Bedingungen am Arbeitsmarkt aus Sicht der Arbeitnehmerinnen und der Arbeitnehmer, Förderung neuer Beschäftigungsverhältnisse, etc.)?',
        infoText5:
          'Sind Auswirkungen auf das Arbeitsrecht, den Arbeitsschutz zu erwarten (z.B. Veränderung der rechtlichen Position von Arbeitnehmerinnen und Arbeitnehmern in Arbeitsverhältnissen, Unfallschutz, etc.)?',
        infoText6:
          'Sind Auswirkungen auf die soziale Einbeziehung bzw. den Schutz bestimmter gesellschaftlicher Gruppen zu erwarten (z.B. Minderheitenschutz, Antidiskriminierung, etc.)?',
        infoText7: 'Hat das Vorhaben Auswirkungen auf das Zusammenleben mit Migrantinnen und Migranten?',
        infoText8:
          'Sind Auswirkungen auf den Schutz der Privatsphäre und das Familienleben zu erwarten (u.a. Schutz der Familie, Familienförderung, Schutz des privaten Lebensumfeldes, etc.)?',
        infoText9:
          'Hat der Entwurf Auswirkungen auf den Zugang zum Gesundheitssektor (z.B. uneingeschränkter Zugang zu Ärztinnen und Ärzten, zu Krankenhäusern, zu Gesundheitsprävention, etc.)?',
        infoText10:
          'Hat der Entwurf Auswirkungen auf den Zugang zum Bildungssektor (z.B. freier Zugang zu Bildungseinrichtungen und Fortbildungsangeboten.)?',
        infoText11:
          'Sind Vor- oder Nachteile für bestimmte Regionen oder Räume zu erwarten (u.a. Raumnutzung, soziale Infrastruktur (Kindertagesstätten, Jugendeinrichtungen, etc.), Gentrification, Segregation, sozialer Auf- oder Abstieg bestimmter Regionen und Quartiere, etc.)?',
        infoText12:
          'Sind Belange der Frauen-, Seniorinnen-/Senioren- und Gleichstellungspolitik berührt (z.B. Stellung von Frauen in der Gesellschaft, Rentenpolitik für Seniorinnen und Senioren, Auswirkungen für schwangere oder stillende Frauen, Gleichberechtigung von Frauen und/oder Seniorinnen und Senioren, etc.)?',
        infoText13:
          'Hat der Entwurf Auswirkungen auf Behandlungs- und Chancengleichheit (Nicht-Diskriminierung z.B. wegen des Geschlechts oder wegen einer körperlichen oder geistigen Beeinträchtigung)?',
        infoText14:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. sozialer Auf- oder Abstieg, Verbesserung oder Verminderung von sozialer Teilhabe oder Bildungsmöglichkeiten), die zu einer verstärkten oder verminderten sozialen Qualität bei Eintritt des Risikos führen?',
      },
    },

    ernahrungundlandwirtschaft: {
      categoryTitle: 'Ernährung und Landwirtschaft',
      ernahrungundlebensmittelsicherheit: {
        label: 'Ernährung und Lebensmittelsicherheit',
        infoTitle: 'Ernährung und Lebensmittelsicherheit',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Ernährung und Lebensmittelsicherheit auswählen.',
        infoText2:
          'Sind Auswirkungen auf die ökologische Landnutzung durch die Landwirtschaft zu erwarten (z.B. Monokulturen, Bio-Landwirtschaft, etc.)?',
        infoText3:
          'Hat der Entwurf Auswirkungen auf den Zustand von Nutztieren und -pflanzen (u.a. Gesundheit von Nutztieren und -pflanzen, Lebens- und Futtermittel)?',
        infoText4:
          'Führt das Vorhaben zu Veränderungen der Sicherheit von Nahrungs- und Futtermitteln (Nahrungskette, Nahrungspflanzen, tierische Erzeugnisse, Wild- und Futterpflanzen, Beutetieren)?',
        infoText5:
          'Wirkt sich das Vorhaben auf die Trinkwasserressourcen bzw. das Trinkwasser aus (z.B. Verunreinigungen, Verknappung, etc.)?',
        infoText6:
          'Führt das Vorhaben zu Veränderungen des Schadstoffgehaltes oder anderer für den Mensch unverträglicher Eigenschaften (z.B. infolge biotechnologischer Modifikationen) in Nahrungspflanzen oder tierischen Erzeugnissen?',
        infoText7:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der Gesundheit der Bevölkerung (z.B. Steigerung oder Minderung der Wahrscheinlichkeit von Epidemien/Pandemien, etc.)?',
        infoText8:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der Gesundheit der Bevölkerung (z.B. infolge des veränderten Zustandes von Luft, Wasser, Boden, Klima und/oder anderen Bestandteilen der Umwelt oder einer Veränderung der Lärmsituation, Steigerung oder Minderung der Wahrscheinlichkeit von Epidemien/Pandemien, veränderte Emissionsbelastung, etc.)?',
      },
      landundforstwirtschaftfischerei: {
        label: 'Land- und Forstwirtschaft, Fischerei',
        infoTitle: 'Land- und Forstwirtschaft, Fischerei',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Land- und Forstwirtschaft, Fischerei auswählen.',
        infoText2:
          'Sind Auswirkungen auf die ökologische Landnutzung durch die Landwirtschaft zu erwarten (z.B. Monokulturen, Bio-Landwirtschaft, etc.)?',
        infoText3:
          'Hat der Entwurf Auswirkungen auf den Zustand von Nutztieren und -pflanzen (u.a. Gesundheit von Nutztieren und -pflanzen, Lebens- und Futtermittel)?',
        infoText4:
          'Führt das Vorhaben zu Veränderungen der Sicherheit von Nahrungs- und Futtermitteln (Nahrungskette, Nahrungspflanzen, tierische Erzeugnisse, Wild- und Futterpflanzen, Beutetieren)?',
        infoText5:
          'Führt das Vorhaben zu Veränderungen der Luftqualität, die sich auf die Gesundheit der Bevölkerung und den Zustand anderer Bestandteile der Umwelt (u.a. Pflanzen [Waldschäden]) auswirken?',
        infoText6:
          'Führt das Vorhaben zu einer Verminderung oder Erhöhung der (globalen) biologischen Vielfalt (Vielfalt der Lebensräume, Vielfalt der Arten, genetische Vielfalt innerhalb der Arten)?',
        infoText7:
          'Führt das Vorhaben zu einer Veränderung des chemischen (z.B. Versauerung, Schadstoffanreicherung, Überdüngung), physikalischen (z.B. Verdichtung) oder biologischen Zustands (Bodenorganismen) von Böden einschließlich der Bodenerosion?',
        infoText8:
          'Führt das Vorhaben zu Veränderungen des Schadstoffgehaltes oder anderer für Tiere unverträglicher Eigenschaften in Wild- und Futterpflanzen oder Beutetieren?',
        infoText9:
          'Führt das Vorhaben zu einem erhöhten oder verminderten Einsatz von erneuerbaren Ressourcen, deren Bestandsentwicklung einen Abwärtstrend aufweist (z.B. Fischbestände)?',
      },
      landlicheregionraumeagrarmarkte: {
        label: 'Ländliche Region Räume, Agrarmärkte',
        infoTitle: 'Ländliche Region Räume, Agrarmärkte',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Ländliche Region Räume, Agrarmärkte auswählen.',
        infoText2:
          'Sind Auswirkungen auf die ökologische Landnutzung durch die Landwirtschaft zu erwarten (z.B. Monokulturen, Bio-Landwirtschaft, etc.)?',
        infoText3:
          'Führt das Vorhaben zu einer Verminderung oder Erhöhung der (globalen) biologischen Vielfalt (Vielfalt der Lebensräume, Vielfalt der Arten, genetische Vielfalt innerhalb der Arten)?',
        infoText4:
          'Wirkt sich das Vorhaben auf terrestrische oder aquatische Ökosysteme, Tiere oder Pflanzen insgesamt, insbesondere auf geschützte oder gefährdete Arten oder deren Lebensräume oder empfindliche Lebensräume (z.B. Natura-2000-Gebiete oder andere Schutzgebiete) aus?',
        infoText5: 'Führt das Vorhaben zu einer verbesserten oder verminderten Qualität des Landschaftsbildes?',
        infoText6:
          'Führt das Vorhaben zu einer Verminderung oder Erhöhung des Freiraumanteils an der Gesamtfläche (d.h. der Fläche, die nicht als Siedlungs- und Verkehrsfläche genutzt wird)?',
        infoText7:
          'Sind Vor- oder Nachteile für bestimmte Regionen oder Räume zu erwarten (u.a. Raumnutzung, soziale Infrastruktur (Kindertagesstätten, Jugendeinrichtungen, etc.), Gentrification, Segregation, sozialer Auf- oder Abstieg bestimmter Regionen und Quartiere, etc.)?',
      },
      ernahrungundlandwirtschaftgrundsatz: {
        label: 'Ernährung und Landwirtschaft (Grundsatz)',
        infoTitle: 'Ernährung und Landwirtschaft (Grundsatz)',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Ernährung und Landwirtschaft (Grundsatz) auswählen.',
        infoText2:
          'Sind Auswirkungen auf die ökologische Landnutzung durch die Landwirtschaft zu erwarten (z.B. Monokulturen, Bio-Landwirtschaft, etc.)?',
        infoText3:
          'Hat der Entwurf Auswirkungen auf den Zustand von Nutztieren und -pflanzen (u.a. Gesundheit von Nutztieren und -pflanzen, Lebens- und Futtermittel)?',
        infoText4:
          'Führt das Vorhaben zu Veränderungen der Luftqualität, die sich auf die Gesundheit der Bevölkerung und den Zustand anderer Bestandteile der Umwelt (u.a. Pflanzen [Waldschäden]) auswirken?',
        infoText5:
          'Führt das Vorhaben zu einer Veränderung des chemischen (z.B. Versauerung, Schadstoffanreicherung, Überdüngung), physikalischen (z.B. Verdichtung) oder biologischen Zustands (Bodenorganismen) von Böden einschließlich der Bodenerosion?',
        infoText6:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der ökologischen oder chemischen Qualität oder Quantität von Oberflächen- und Grundwasser?',
        infoText7: 'Wird durch das Vorhaben die Effizienz des Einsatzes von Ressourcen gesteigert oder vermindert?',
        infoText8: 'Hat das Vorhaben Auswirkungen auf Abfallvermeidung, -entstehung, -verwertung oder -beseitigung?',
        infoText9:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. unbeabsichtigte Freisetzungen von Schadstoffen, Strahlung, Lärm, Wärme; Störfälle; Havarien; Anfälligkeit für Anschläge bei umweltrelevanten Anlagen), die zu einer Gefährdung der Umwelt bei Eintritt des Risikos führen?',
      },
    },

    familieseniorenfrauenundjugendkategorie: {
      categoryTitle: 'Familien, Seniorinnen und Senioren, Frauen und Jugend',
      familieseniorenfrauenundjugend: {
        label: 'Familie, Seniorinnen und Senioren, Frauen und Jugend',
        infoTitle: 'Familie, Seniorinnen und Senioren, Frauen und Jugend',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Familie, Seniorinnen und Senioren, Frauen und Jugend auswählen.',
        infoText2:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der Gesundheit der Bevölkerung (z.B. infolge des veränderten Zustandes von Luft, Wasser, Boden, Klima und/oder anderen Bestandteilen der Umwelt oder einer Veränderung der Lärmsituation, Steigerung oder Minderung der Wahrscheinlichkeit von Epidemien/Pandemien, veränderte Emissionsbelastung, etc.)?',
        infoText3:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der Gesundheit der Bevölkerung (z.B. Steigerung oder Minderung der Wahrscheinlichkeit von Epidemien/Pandemien, etc.)?',
        infoText4:
          'Sind Auswirkungen auf die soziale Einbeziehung bzw. den Schutz bestimmter gesellschaftlicher Gruppen zu erwarten (z.B. Minderheitenschutz, Antidiskriminierung, etc.)?',
        infoText5: 'Hat das Vorhaben Auswirkungen auf das Zusammenleben mit Migrantinnen und Migranten?',
        infoText6:
          'Sind Auswirkungen auf den Schutz der Privatsphäre und das Familienleben zu erwarten (u.a. Schutz der Familie, Familienförderung, Schutz des privaten Lebensumfeldes, etc.)?',
        infoText7:
          'Hat der Entwurf Auswirkungen auf den Zugang zu Gesundheitssektor (z.B. uneingeschränkter Zugang zu Ärztinnen und Ärzten, zu Krankenhäusern, zu Gesundheitsprävention, etc.)?',
        infoText8:
          'Hat der Entwurf Auswirkungen auf den Zugang zum Bildungssektor (z.B. freier Zugang zu Bildungseinrichtungen und Fortbildungsangeboten.)?',
        infoText9:
          'Sind Belange der Frauen-, Seniorinnen-/Senioren- und Gleichstellungspolitik berührt (z.B. Stellung von Frauen in der Gesellschaft, Rentenpolitik für Seniorinnen und Senioren, Auswirkungen für schwangere oder stillende Frauen, Gleichberechtigung von Frauen und/oder Seniorinnen und Senioren, etc.)?',
        infoText10:
          'Sind Belange des Kinder- und Jugendschutzes bzw. der Kinder- und Jugendpolitik berührt (z.B. Schutz vor Drogen- und Alkoholmissbrauch, sexuelle Aufklärung, Schutz von Kindern und Jugendlichen vor Missbrauch, etc.)?',
        infoText11:
          'Hat der Entwurf Auswirkungen auf Behandlungs- und Chancengleichheit (Nicht-Diskriminierung z.B. wegen des Geschlechts oder wegen einer körperlichen oder geistigen Beeinträchtigung)?',
        infoText12:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. sozialer Auf- oder Abstieg, Verbesserung oder Verminderung von sozialer Teilhabe oder Bildungsmöglichkeiten), die zu einer verstärkten oder verminderten sozialen Qualität bei Eintritt des Risikos führen?',
        infoText13:
          'Hat das Vorhaben Auswirkungen auf die Gewährung, Art und Umfang sozialer staatlicher Transferleistungen (z.B. ALG I und II, Sozialhilfe, etc.)?',
      },
    },

    gesundheitkategorie: {
      categoryTitle: 'Gesundheit',
      gesundheit: {
        label: 'Gesundheit',
        infoTitle: 'Gesundheit',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Gesundheit auswählen.',
        infoText2:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der Gesundheit der Bevölkerung (z.B. infolge des veränderten Zustandes von Luft, Wasser, Boden, Klima und/oder anderen Bestandteilen der Umwelt oder einer Veränderung der Lärmsituation, Steigerung oder Minderung der Wahrscheinlichkeit von Epidemien/Pandemien, veränderte Emissionsbelastung, etc.)?',
        infoText3:
          'Hat der Entwurf Auswirkungen auf den Zugang zum Gesundheitssektor (z.B. uneingeschränkter Zugang zu Ärztinnen und Ärzten, zu Krankenhäusern, zu Gesundheitsprävention, etc.)?',
        infoText4:
          'Wirkt sich das Vorhaben auf die Trinkwasserressourcen bzw. das Trinkwasser aus (z.B. Verunreinigungen, Verknappung, etc.)?',
        infoText5:
          'Führt das Vorhaben zu Veränderungen der Luftqualität, die sich auf die Gesundheit der Bevölkerung und den Zustand anderer Bestandteile der Umwelt (u.a. Pflanzen [Waldschäden]) auswirken?',
        infoText6:
          'Führt das Vorhaben zu Veränderungen des Schadstoffgehaltes oder anderer für den Mensch unverträglicher Eigenschaften (z.B. infolge biotechnologischer Modifikationen) in Nahrungspflanzen oder tierischen Erzeugnissen?',
        infoText7:
          'Hat der Entwurf Auswirkungen auf Steuern und andere Abgaben (u.a. Steuererhöhungen oder -senkungen, Sozialversicherungsbeiträge, sonstige relevante Abgaben)?',
        infoText8:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der Gesundheit der Bevölkerung (z.B. Steigerung oder Minderung der Wahrscheinlichkeit von Epidemien/Pandemien, etc.)?',
        infoText9:
          'Sind Auswirkungen auf das Arbeitsrecht, den Arbeitsschutz zu erwarten (z.B. Veränderung der rechtlichen Position von Arbeitnehmerinnen und Arbeitnehmern in Arbeitsverhältnissen, Unfallschutz, etc.)?',
        infoText10:
          'Werden Angelegenheiten der Kirchen oder  Religionsgemeinschaften berührt (u.a. bei der freien Religionsausübung, bei ihrer caritativen und gesellschaftspolitischen Tätigkeit in ihrer Eigenschaft als Trägerin, Träger, Arbeitgeberin oder Arbeitgeber bestimmter Einrichtungen wie z.B. Schulen, Pflegeeinrichtungen, Kindergärten, Krankenhäuser sowie bei der Ausübung dieser Tätigkeiten)?',
        infoText11: 'Sind Belange von behinderten Personen berührt (z.B. Antidiskriminierung, Barrierefreiheit, etc.)?',
        infoText12:
          'Sind Auswirkungen auf die soziale Einbeziehung bzw. den Schutz bestimmter gesellschaftlicher Gruppen zu erwarten (z.B. Minderheitenschutz, Antidiskriminierung, etc.)?',
        infoText13:
          'Sind Belange der Frauen-, Seniorinnen-/Senioren- und Gleichstellungspolitik berührt (z.B. Stellung von Frauen in der Gesellschaft, Rentenpolitik für Seniorinnen und Senioren, Auswirkungen für schwangere oder stillende Frauen, Gleichberechtigung von Frauen und/oder Seniorinnen und Senioren, etc.)?',
        infoText14:
          'Hat der Entwurf Auswirkungen auf Behandlungs- und Chancengleichheit (Nicht-Diskriminierung z.B. wegen des Geschlechts oder wegen einer körperlichen oder geistigen Beeinträchtigung)?',
        infoText15:
          'Sind Belange des Kinder- und Jugendschutzes bzw. der Kinder- und Jugendpolitik berührt (z.B. Schutz vor Drogen- und Alkoholmissbrauch, sexuelle Aufklärung, Schutz von Kindern und Jugendlichen vor Missbrauch, etc.)?',
        infoText16:
          'Hat das Vorhaben Auswirkungen auf die Gewährung, Art und Umfang sozialer staatlicher Transferleistungen (z.B. ALG I und II, Sozialhilfe, etc.)?',
      },
    },

    verkehr: {
      categoryTitle: 'Verkehr, Infrastruktur und Bauwesen',
      eisenbahnen: {
        label: 'Eisenbahnen',
        infoTitle: 'Eisenbahnen',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Eisenbahnen auswählen.',
        infoText2:
          'Sind Auswirkungen auf den Verkehr und das Transportwesen zu erwarten (u.a. Verkehrsinfrastruktur, Logistik, etc.)?',
      },
      luftfahrt: {
        label: 'Luftfahrt',
        infoTitle: 'Luftfahrt',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Luftfahrt auswählen.',
        infoText2:
          'Sind Auswirkungen auf den Verkehr und das Transportwesen zu erwarten (u.a. Verkehrsinfrastruktur, Logistik, etc.)?',
      },
      strassenbau: {
        label: 'Straßenbau',
        infoTitle: 'Straßenbau',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Straßenbau auswählen.',
        infoText2:
          'Sind Auswirkungen auf den Verkehr und das Transportwesen zu erwarten (u.a. Verkehrsinfrastruktur, Logistik, etc.)?',
      },
      strassenverkehr: {
        label: 'Straßenverkehr',
        infoTitle: 'Straßenverkehr',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Straßenverkehr auswählen.',
        infoText2:
          'Sind Auswirkungen auf den Verkehr und das Transportwesen zu erwarten (u.a. Verkehrsinfrastruktur, Logistik, etc.)?',
        infoText3:
          'Sind Aspekte der städtebaulichen Planung betroffen (u.a. innerstädtische Raumnutzung, innerstädtische Infrastruktur und Verkehrswegeplanung, Gentrification, Segregation, sozialer Auf- oder Abstieg innerstädtischer Quartiere, etc.)?',
      },
      wasserstrassenschifffahrt: {
        label: 'Wasserstraßen, Schifffahrt',
        infoTitle: 'Wasserstraßen, Schifffahrt',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Wasserstraßen, Schifffahrt auswählen.',
        infoText2:
          'Sind Auswirkungen auf den Verkehr und das Transportwesen zu erwarten (u.a. Verkehrsinfrastruktur, Logistik, etc.)?',
      },
      verkehrgrundsatz: {
        label: 'Verkehr (Grundsatz)',
        infoTitle: 'Verkehr (Grundsatz)',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Verkehr (Grundsatz) auswählen.',
        infoText2:
          'Sind Auswirkungen auf den Verkehr und das Transportwesen zu erwarten (u.a. Verkehrsinfrastruktur, Logistik, etc.)?',
        infoText3:
          'Sind Aspekte der städtebaulichen Planung betroffen (u.a. innerstädtische Raumnutzung, innerstädtische Infrastruktur und Verkehrswegeplanung, Gentrification, Segregation, sozialer Auf- oder Abstieg innerstädtischer Quartiere, etc.)?',
        infoText4:
          'Führt das Vorhaben zu einer Verminderung oder Erhöhung des Freiraumanteils an der Gesamtfläche (d.h. der Fläche, die nicht als Siedlungs- und Verkehrsfläche genutzt wird)?',
      },
      baustadtwohnen: {
        label: 'Bau, Stadt, Wohnen',
        infoTitle: 'Bau, Stadt, Wohnen',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Bau, Stadt, Wohnen auswählen.',
        infoText2:
          'Sind Aspekte der städtebaulichen Planung betroffen (u.a. innerstädtische Raumnutzung, innerstädtische Infrastruktur und Verkehrswegeplanung, Gentrification, Segregation, sozialer Auf- oder Abstieg innerstädtischer Quartiere, etc.)?',
        infoText3: 'Führt das Vorhaben zu einer verbesserten oder verminderten Qualität des Landschaftsbildes?',
        infoText4:
          'Führt das Vorhaben zu einer Verminderung oder Erhöhung des Freiraumanteils an der Gesamtfläche (d.h. der Fläche, die nicht als Siedlungs- und Verkehrsfläche genutzt wird)?',
        infoText5:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. unbeabsichtigte Freisetzungen von Schadstoffen, Strahlung, Lärm, Wärme; Störfälle; Havarien; Anfälligkeit für Anschläge bei umweltrelevanten Anlagen), die zu einer Gefährdung der Umwelt bei Eintritt des Risikos führen?',
        infoText6:
          'Sind Auswirkungen auf den Schutz der Privatsphäre und das Familienleben zu erwarten (u.a. Schutz der Familie, Familienförderung, Schutz des privaten Lebensumfeldes, etc.)?',
        infoText7:
          'Sind Vor- oder Nachteile für bestimmte Regionen oder Räume zu erwarten (u.a. Raumnutzung, soziale Infrastruktur (Kindertagesstätten, Jugendeinrichtungen, etc.), Gentrification, Segregation, sozialer Auf- oder Abstieg bestimmter Regionen und Quartiere, etc.)?',
      },
    },

    umweltundnaturschutz: {
      categoryTitle: 'Umwelt und Naturschutz',
      abfallewasserboden: {
        label: 'Abfälle, Wasser, Böden',
        infoTitle: 'Abfälle, Wasser, Böden',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Abfälle, Wasser, Böden auswählen.',
        infoText2:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der Gesundheit der Bevölkerung (z.B. infolge des veränderten Zustandes von Luft, Wasser, Boden, Klima und/oder anderen Bestandteilen der Umwelt oder einer Veränderung der Lärmsituation, Steigerung oder Minderung der Wahrscheinlichkeit von Epidemien/Pandemien, veränderte Emissionsbelastung, etc.)?',
        infoText3:
          'Wirkt sich das Vorhaben auf terrestrische oder aquatische Ökosysteme, Tiere oder Pflanzen insgesamt, insbesondere auf geschützte oder gefährdete Arten oder deren Lebensräume oder empfindliche Lebensräume (z.B. Natura-2000-Gebiete oder andere Schutzgebiete) aus?',
        infoText4:
          'Führt das Vorhaben zu einer Veränderung des chemischen (z.B. Versauerung, Schadstoffanreicherung, Überdüngung), physikalischen (z.B. Verdichtung) oder biologischen Zustands (Bodenorganismen) von Böden einschließlich der Bodenerosion?',
        infoText5:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der ökologischen oder chemischen Qualität oder Quantität von Oberflächen- und Grundwasser?',
        infoText6:
          'Wirkt sich das Vorhaben auf die Trinkwasserressourcen bzw. das Trinkwasser aus (z.B. Verunreinigungen, Verknappung, etc.)?',
        infoText7:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der ökologischen oder chemischen Qualität von Küsten- oder Meeresgewässern (z.B. Verunreinigungen, etc.)?',
        infoText8: 'Hat das Vorhaben Auswirkungen auf Abfallvermeidung, -entstehung, -verwertung oder -beseitigung?',
        infoText9:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. unbeabsichtigte Freisetzungen von Schadstoffen, Strahlung, Lärm, Wärme; Störfälle; Havarien; Anfälligkeit für Anschläge bei umweltrelevanten Anlagen), die zu einer Gefährdung der Umwelt bei Eintritt des Risikos führen?',
        infoText10:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der Gesundheit der Bevölkerung (z.B. Steigerung oder Minderung der Wahrscheinlichkeit von Epidemien/Pandemien, etc.)?',
      },
      chemikalien: {
        label: 'Chemikalien',
        infoTitle: 'Chemikalien',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Chemikalien auswählen.',
        infoText2:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der Gesundheit der Bevölkerung (z.B. infolge des veränderten Zustandes von Luft, Wasser, Boden, Klima und/oder anderen Bestandteilen der Umwelt oder einer Veränderung der Lärmsituation, Steigerung oder Minderung der Wahrscheinlichkeit von Epidemien/Pandemien, veränderte Emissionsbelastung, etc.)?',
        infoText3:
          'Führt das Vorhaben zu Veränderungen der Sicherheit von Nahrungs- und Futtermitteln (Nahrungskette, Nahrungspflanzen, tierische Erzeugnisse, Wild- und Futterpflanzen, Beutetieren)?',
        infoText4:
          'Wirkt sich das Vorhaben auf terrestrische oder aquatische Ökosysteme, Tiere oder Pflanzen insgesamt, insbesondere auf geschützte oder gefährdete Arten oder deren Lebensräume oder empfindliche Lebensräume (z.B. Natura-2000-Gebiete oder andere Schutzgebiete) aus?',
        infoText5:
          'Führt das Vorhaben zu einer Veränderung des chemischen (z.B. Versauerung, Schadstoffanreicherung, Überdüngung), physikalischen (z.B. Verdichtung) oder biologischen Zustands (Bodenorganismen) von Böden einschließlich der Bodenerosion?',
        infoText6:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der ökologischen oder chemischen Qualität oder Quantität von Oberflächen- und Grundwasser?',
        infoText7:
          'Wirkt sich das Vorhaben auf die Trinkwasserressourcen bzw. das Trinkwasser aus (z.B. Verunreinigungen, Verknappung, etc.)?',
        infoText8:
          'Führt das Vorhaben zu Veränderungen des Schadstoffgehaltes oder anderer für den Mensch unverträglicher Eigenschaften (z.B. infolge biotechnologischer Modifikationen) in Nahrungspflanzen oder tierischen Erzeugnissen?',
        infoText9:
          'Führt das Vorhaben zu Veränderungen des Schadstoffgehaltes oder anderer für Tiere unverträglicher Eigenschaften in Wild- und Futterpflanzen oder Beutetieren?',
        infoText10:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. unbeabsichtigte Freisetzungen von Schadstoffen, Strahlung, Lärm, Wärme; Störfälle; Havarien; Anfälligkeit für Anschläge bei umweltrelevanten Anlagen), die zu einer Gefährdung der Umwelt bei Eintritt des Risikos führen?',
      },
      naturundartenschutz: {
        label: 'Natur- und Artenschutz',
        infoTitle: 'Natur- und Artenschutz',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Natur- und Artenschutz auswählen.',
        infoText2:
          'Hat der Entwurf Auswirkungen auf den Zustand von Nutztieren und -pflanzen (u.a. Gesundheit von Nutztieren und -pflanzen, Lebens- und Futtermittel)?',
        infoText3:
          'Führt das Vorhaben zu Veränderungen der Sicherheit von Nahrungs- und Futtermitteln (Nahrungskette, Nahrungspflanzen, tierische Erzeugnisse, Wild- und Futterpflanzen, Beutetieren)?',
        infoText4:
          'Führt das Vorhaben zu Veränderungen der Luftqualität, die sich auf die Gesundheit der Bevölkerung und den Zustand anderer Bestandteile der Umwelt (u.a. Pflanzen [Waldschäden]) auswirken?',
        infoText5:
          'Führt das Vorhaben zu einer Verminderung oder Erhöhung der (globalen) biologischen Vielfalt (Vielfalt der Lebensräume, Vielfalt der Arten, genetische Vielfalt innerhalb der Arten)?',
        infoText6:
          'Wirkt sich das Vorhaben auf terrestrische oder aquatische Ökosysteme, Tiere oder Pflanzen insgesamt, insbesondere auf geschützte oder gefährdete Arten oder deren Lebensräume oder empfindliche Lebensräume (z.B. Natura-2000-Gebiete oder andere Schutzgebiete) aus?',
        infoText7:
          'Werden durch eine erhöhte oder verminderte Zersiedelung oder Zerschneidung der Landschaft Lebensräume verkleinert oder erweitert, Vernetzungen von Lebensräumen (Lebensraumkorridore) unterbrochen oder neu geschaffen oder Pufferzonen verändert?',
        infoText8: 'Führt das Vorhaben zu einer verbesserten oder verminderten Qualität des Landschaftsbildes?',
        infoText9:
          'Führt das Vorhaben zu einer Verminderung oder Erhöhung des Freiraumanteils an der Gesamtfläche (d.h. der Fläche, die nicht als Siedlungs- und Verkehrsfläche genutzt wird)?',
        infoText10:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der ökologischen oder chemischen Qualität oder Quantität von Oberflächen- und Grundwasser?',
        infoText11:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der ökologischen oder chemischen Qualität von Küsten- oder Meeresgewässern (z.B. Verunreinigungen, etc.)?',
        infoText12:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. unbeabsichtigte Freisetzungen von Schadstoffen, Strahlung, Lärm, Wärme; Störfälle; Havarien; Anfälligkeit für Anschläge bei umweltrelevanten Anlagen), die zu einer Gefährdung der Umwelt bei Eintritt des Risikos führen?',
      },
      tierschutztiergesundheit: {
        label: 'Tierschutz, Tiergesundheit',
        infoTitle: 'Tierschutz, Tiergesundheit',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Tierschutz, Tiergesundheit auswählen.',
        infoText2:
          'Hat der Entwurf Auswirkungen auf den Zustand von Nutztieren und -pflanzen (u.a. Gesundheit von Nutztieren und -pflanzen, Lebens- und Futtermittel)?',
        infoText3:
          'Führt das Vorhaben zu Veränderungen der Sicherheit von Nahrungs- und Futtermitteln (Nahrungskette, Nahrungspflanzen, tierische Erzeugnisse, Wild- und Futterpflanzen, Beutetieren)?',
        infoText4:
          'Führt das Vorhaben zu einer Verminderung oder Erhöhung der (globalen) biologischen Vielfalt (Vielfalt der Lebensräume, Vielfalt der Arten, genetische Vielfalt innerhalb der Arten)?',
        infoText5:
          'Wirkt sich das Vorhaben auf terrestrische oder aquatische Ökosysteme, Tiere oder Pflanzen insgesamt, insbesondere auf geschützte oder gefährdete Arten oder deren Lebensräume oder empfindliche Lebensräume (z.B. Natura-2000-Gebiete oder andere Schutzgebiete) aus?',
        infoText6:
          'Führt das Vorhaben zu Veränderungen des Schadstoffgehaltes oder anderer für Tiere unverträglicher Eigenschaften in Wild- und Futterpflanzen oder Beutetieren?',
      },
      umweltgrundsatz: {
        label: 'Umwelt (Grundsatz)',
        infoTitle: 'Umwelt (Grundsatz)',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Umwelt (Grundsatz) auswählen.',
        infoText2:
          'Sind Vor- oder Nachteile für bestimmte (Wirtschafts-) Regionen oder Sektoren zu erwarten (u.a. Vor- und Nachteile für bestimmte industrielle Ballungszentren oder bestimmte Industriesektoren wie Schwerindustrie, chemische Industrie, Bergbau, etc.)?',
        infoText3:
          'Sind Auswirkungen auf die ökologische Landnutzung durch die Landwirtschaft zu erwarten (z.B. Monokulturen, Bio-Landwirtschaft, etc.)?',
        infoText4:
          'Hat der Entwurf Auswirkungen auf den Zustand von Nutztieren und -pflanzen (u.a. Gesundheit von Nutztieren und -pflanzen, Lebens- und Futtermittel)?',
        infoText5:
          'Führt das Vorhaben zu Veränderungen der Luftqualität, die sich auf die Gesundheit der Bevölkerung und den Zustand anderer Bestandteile der Umwelt (u.a. Pflanzen [Waldschäden]) auswirken?',
        infoText6:
          'Führt das Vorhaben zu einer Verminderung oder Erhöhung der (globalen) biologischen Vielfalt (Vielfalt der Lebensräume, Vielfalt der Arten, genetische Vielfalt innerhalb der Arten)?',
        infoText7: 'Führt das Vorhaben zu einer verbesserten oder verminderten Qualität des Landschaftsbildes?',
        infoText8:
          'Führt das Vorhaben zu einer Verminderung oder Erhöhung des Freiraumanteils an der Gesamtfläche (d.h. der Fläche, die nicht als Siedlungs- und Verkehrsfläche genutzt wird)?',
        infoText9:
          'Führt das Vorhaben zu Veränderungen des Schadstoffgehaltes oder anderer für den Mensch unverträglicher Eigenschaften (z.B. infolge biotechnologischer Modifikationen) in Nahrungspflanzen oder tierischen Erzeugnissen?',
        infoText10:
          'Führt das Vorhaben zu einem erhöhten oder verminderten Einsatz von nicht erneuerbaren Ressourcen (z.B. Erdöl, Mineralien, Erze)?',
        infoText11: 'Wird durch das Vorhaben die Effizienz des Einsatzes von Ressourcen gesteigert oder vermindert?',
        infoText12:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. unbeabsichtigte Freisetzungen von Schadstoffen, Strahlung, Lärm, Wärme; Störfälle; Havarien; Anfälligkeit für Anschläge bei umweltrelevanten Anlagen), die zu einer Gefährdung der Umwelt bei Eintritt des Risikos führen?',
        infoText13:
          'Hat der Entwurf Folgen im Umweltbereich, die sich auch auf andere Staaten (internationale bzw. globale Dimension) auswirken (z.B. Verknappung der Lebensmittelverfügbarkeit im Ausland wegen des Anbaus von Energiepflanzen für den Export)?',
        infoText14:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der Gesundheit der Bevölkerung (z.B. Steigerung oder Minderung der Wahrscheinlichkeit von Epidemien/Pandemien, etc.)?',
      },
    },

    bildungundforschung: {
      categoryTitle: 'Bildung und Forschung',
      berufsausbildung: {
        label: 'Berufsausbildung',
        infoTitle: 'Berufsausbildung',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Berufsausbildung auswählen.',
        infoText2:
          'Hat der Entwurf Auswirkungen auf den Zugang zum Bildungssektor (z.B. freier Zugang zu Bildungseinrichtungen und Fortbildungsangeboten.)?',
      },
      forschung: {
        label: 'Forschung',
        infoTitle: 'Forschung',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Forschung auswählen.',
        infoText2:
          'Sind Auswirkungen auf Forschung und Entwicklung zu erwarten (z.B. Förderung von Innovation und Entwicklung neuer Technologien, Förderung von (Hochschul-)Forschung als Standort- und Wirtschaftsfaktor)?',
        infoText3:
          'Sind Auswirkungen auf das Innovationsverhalten der Unternehmen zu erwarten (z.B. Mehr- oder Minderausgaben von Wirtschaftsunternehmen, Produktinnovationen)?',
        infoText4:
          'Sind Auswirkungen auf die Nutzung innovativer Lösungen/neuer Technologien zu erwarten (z.B. werden explizit Innovationen und/oder neue Technologien gefördert oder in ihrer Entwicklung gehemmt)?',
        infoText5:
          'Führt das Vorhaben zu einem erhöhten oder verminderten Einsatz von erneuerbaren Ressourcen, deren Bestandsentwicklung einen Abwärtstrend aufweist (z.B. Fischbestände)? ',
        infoText6:
          'Führt das Vorhaben zu einem erhöhten oder verminderten Einsatz von nicht erneuerbaren Ressourcen (z.B. Erdöl, Mineralien, Erze)?',
        infoText7:
          'Wird durch das Vorhaben die Effizienz des Einsatzes von Energie gesteigert oder vermindert (z.B. Energiesparprojekte)?',
        infoText8: 'Wird durch das Vorhaben die Effizienz des Einsatzes von Ressourcen gesteigert oder vermindert?',
        infoText9:
          'Führt das Vorhaben zu einer Verbesserung oder Verschlechterung der Gesundheit der Bevölkerung (z.B. Steigerung oder Minderung der Wahrscheinlichkeit von Epidemien/Pandemien, etc.)?',
      },
      fortbildung: {
        label: 'Fortbildung',
        infoTitle: 'Fortbildung',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Fortbildung auswählen.',
        infoText2:
          'Hat der Entwurf Auswirkungen auf den Zugang zum Bildungssektor (z.B. freier Zugang zu Bildungseinrichtungen und Fortbildungsangeboten.)?',
      },
      bildungundforschunggrundsatz: {
        label: 'Bildung und Forschung (Grundsatz)',
        infoTitle: 'Bildung und Forschung (Grundsatz)',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Bildung und Forschung (Grundsatz) auswählen.',
        infoText2:
          'Sind Auswirkungen auf die soziale Einbeziehung bzw. den Schutz bestimmter gesellschaftlicher Gruppen zu erwarten (z.B. Minderheitenschutz, Antidiskriminierung, etc.)?',
        infoText3: 'Hat das Vorhaben Auswirkungen auf das Zusammenleben mit Migrantinnen und Migranten?',
        infoText4:
          'Hat der Entwurf Auswirkungen auf den Zugang zum Bildungssektor (z.B. freier Zugang zu Bildungseinrichtungen und Fortbildungsangeboten.)?',
        infoText5:
          'Hat der Entwurf Auswirkungen auf Behandlungs- und Chancengleichheit (Nicht-Diskriminierung z.B. wegen des Geschlechts oder wegen einer körperlichen oder geistigen Beeinträchtigung)?',
        infoText6:
          'Steigt durch das Vorhaben die Wahrscheinlichkeit von Risiken (z.B. sozialer Auf- oder Abstieg, Verbesserung oder Verminderung von sozialer Teilhabe oder Bildungsmöglichkeiten), die zu einer verstärkten oder verminderten sozialen Qualität bei Eintritt des Risikos führen?',
      },
    },

    medienundkulturkategorie: {
      categoryTitle: 'Medien und Kultur',
      medienundkultur: {
        label: 'Medien und Kultur',
        infoTitle: 'Medien und Kultur',
        infoText1:
          'Wenn Sie eine oder mehrere dieser Fragen mit „ja“ beantworten, sollten Sie den Bereich Medien und Kultur auswählen.',
        infoText2:
          'Sind Belange der Kultur- und Medienpolitik berührt (z.B. Belange der Filmförderung, Denkmalpflege, Kunstförderung, etc.)?',
        infoText3:
          'Hat der Entwurf Auswirkungen auf den Zugang zu Medien (u.a. Zugang für Alle zu Internet, Fernsehen, Radio, Printmedien)?',
      },
    },
  },

  bundesministerium: {
    BMF: 'Bundesministerium der Finanzen',
    BMI: 'Bundesministerium des Innern und für Heimat',
    AA: 'Auswärtiges Amt',
    BMZ: 'Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung',
    BMUV: 'Bundesministerium für Umwelt, Naturschutz, nukleare Sicherheit und Verbraucherschutz',
    BMWK: 'Bundesministerium für Wirtschaft und Klimaschutz',
    BMJ: 'Bundesministerium der Justiz',
    BMAS: 'Bundesministerium für Arbeit und Soziales',
    BMVg: 'Bundesministerium der Verteidigung',
    BMEL: 'Bundesministerium für Ernährung und Landwirtschaft',
    BMFSFJ: 'Bundesministerium für Familie, Seniorinnen und Senioren, Frauen und Jugend',
    BMG: 'Bundesministerium für Gesundheit',
    BMDV: 'Bundesministerium für Digitales und Verkehr',
    BMBF: 'Bundesministerium für Bildung und Forschung',
    BKM: 'Beauftragte der Bundesregierung für Kultur und Medien',
    BMWSB: 'Bundesministerium für Wohnen, Stadtentwicklung und Bauwesen',
  },
  ideasCollection: {
    title: 'Entwicklung von Regelungsalternativen',
    subtitle1: 'Ideen sammeln',
    infoText1:
      'Hier können Sie in einem ersten Schritt Ideen für das Regelungsvorhaben sammeln. Zerlegen Sie das Regelungsvorhaben dafür zunächst in Teilbereiche. Überlegen Sie sich nun alternative Regelungsmöglichkeiten für die Teilbereiche und legen Sie diese als Optionen an - ratsam ist es, hier immer auch die Beibehaltung des Status quo als Option zu berücksichtigen. An dieser Stelle kann auch geprüft werden, ob durch die Aufnahme einer Experimentierklausel innovativen Leistungen Freiraum gegeben werden kann. Informationen dazu bietet die Arbeitshilfe zur Formulierung von <a target="_blank" href="{{basePath}}/arbeitshilfen/download/54">Experimentierklauseln</a>.<br><br>Die Zusammenstellung der einzelnen Optionen aus den Teilbereichen erfolgt im zweiten Schritt.',
    subtitle2: 'Teilbereiche des Regelungsvorhabens',
    infoText2:
      'Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch eigene Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.',
    subpartTitle: 'Titel des Teilbereichs',
    subpartOption: 'Titel der Option',
    subpartDescription: 'Beschreibung der Option',
    addOption: 'Option hinzufügen',
    removeOption: 'Option entfernen',
    removeSubpart: 'Teilbereich entfernen',
    addSubpart: 'Teilbereich hinzufügen',
    drawerTitle: 'Entwicklung von Regelungsalternativen',
    drawerContent1:
      'Die Entwicklung von Regelungsalternativen ist ein Schwerpunkt jeder Folgenabschätzung. Es ist wichtig, sich zu vergegenwärtigen, dass die frühzeitige Abschätzung in der Regel nur im Vergleich mehrerer Regelungsalternativen gelingen kann. Deshalb ist es empfehlenswert, möglichst offen nach alternativen Regelungsmöglichkeiten zu suchen.',
    drawerContent2:
      'Es ist zu beachten, dass zu jedem Teilbereich des Regelungsvorhabens immer mindestens zwei Regelungsalternativen bestehen: Eine Neuregelung und die Null-Option (Beibehaltung des Status quo). Ratsam ist es, neben der Null-Option mehrere Alternativen zu jedem bedeutsamen Aspekt der beabsichtigten Regelung zu berücksichtigen und zu untersuchen. Dabei können auch Befristungen, Experimentierklauseln etc. bei der Alternativenfindung eine Rolle spielen. Ziel ist es, voneinander abgrenzbare Regelungsalternativen zu finden, die dann vergleichend auf ihre jeweiligen Folgen hin überprüft werden sollen.',
    drawerContent3:
      'Grundsätzlich sollten alternative Instrumente und Regelungsansätze im Rahmen der Folgenabschätzung geprüft werden. Welche Ansätze und Instrumente in Frage kommen, ist selbstverständlich abhängig vom Regelungsgegenstand. In die Überlegungen zur Alternativenentwicklung können auch unterschiedliche Regulierungs- und Programmierungsformen einbezogen werden. So ist zu fragen, ob neben konditionalen Regulierungsformen (wenn x auftritt, dann wird y entschieden) auch finale Programmierungen (Zweckvorgaben; das heißt Vorgabe eines Zweckes mit freier oder begrenzter Mittelwahl) in Frage kommen; letztere werden zum Beispiel als Selbstregulierungsmöglichkeit in <a href={{linkGGO}} target="_blank"> Anlage 5 der GGO</a> behandelt.',
    validationMessageTeilbereich:
      'Bitte geben Sie einen Titel für den Teilbereich {{index}} ein oder entfernen Sie diesen Teilbereich.',
    validationMessageOption:
      'Bitte geben Sie einen Titel für die Option {{index}} ein oder entfernen Sie diese Option.',
    drawerSubpartTitle: 'Teilbereiche',
    drawerSubpartText: 'Mögliche Teilbereiche könnten sein:',
    drawerSubpartListItem1: 'Organisation (Wer macht was?)',
    drawerSubpartListItem2: 'Regelungsinhalt',
    drawerSubpartListItem3: 'Verfahren (Wie macht man etwas?)',
    drawerSubpartListItem4: 'Art der Regelung (Normhierarchie)',
  },
  alternativesCollection: {
    title: 'Entwicklung von Regelungsalternativen',
    subtitle: 'Alternativen zusammenstellen',
    infoText:
      'Hier können Sie die Optionen, die Sie für verschiedene Teilbereiche des Regelungsvorhabens gesammelt haben, zu umfassenden Regelungsalternativen zusammensetzen.',
    elementTitle: 'Regelungsalternative',
    addAlternative: 'Regelungsalternative hinzufügen',
    deleteAlternative: 'Regelungsalternative {{index}} entfernen',
    elementSubtitle1: 'Titel',
    elementSubtitle2: 'Kurzbeschreibung',
    elementSubtitle3Part1: 'Alternativen aus den Optionen der Teilbereiche',
    elementSubtitle3Part2: 'zusammenstellen',
    elementSubsubtitle: 'Titel des Teilbereichs',
    drawerHeadingTitle: 'Entwicklung von Regelungsalternativen',
    drawerHeadingContent1:
      'Die Entwicklung von Regelungsalternativen ist ein Schwerpunkt jeder Folgenabschätzung. Es ist wichtig, sich zu vergegenwärtigen, dass die frühzeitige Abschätzung in der Regel nur im Vergleich mehrerer Regelungsalternativen gelingen kann. Deshalb ist es empfehlenswert, möglichst offen nach alternativen Regelungsmöglichkeiten zu suchen.',
    drawerHeadingContent2:
      'Es ist zu beachten, dass zu jedem Teilbereich des Regelungsvorhabens immer mindestens zwei Regelungsalternativen bestehen: Eine Neuregelung und die Null-Option (Beibehaltung des Status quo). Ratsam ist es, neben der Null-Option mehrere Alternativen zu jedem bedeutsamen Aspekt der beabsichtigten Regelung zu berücksichtigen und zu untersuchen. Dabei können auch Befristungen, Experimentierklauseln etc. bei der Alternativenfindung eine Rolle spielen. Ziel ist es, voneinander abgrenzbare Regelungsalternativen zu finden, die dann vergleichend auf ihre jeweiligen Folgen hin überprüft werden sollen.',
    drawerHeadingContent3:
      'Grundsätzlich sollten alternative Instrumente und Regelungsansätze im Rahmen der Folgenabschätzung geprüft werden. Welche Ansätze und Instrumente in Frage kommen, ist selbstverständlich abhängig vom Regelungsgegenstand. In die Überlegungen zur Alternativenentwicklung können auch unterschiedliche Regulierungs- und Programmierungsformen einbezogen werden. So ist zu fragen, ob neben konditionalen Regulierungsformen (wenn x auftritt, dann wird y entschieden) auch finale Programmierungen (Zweckvorgaben; das heißt Vorgabe eines Zweckes mit freier oder begrenzter Mittelwahl) in Frage kommen; letztere werden zum Beispiel als Selbstregulierungsmöglichkeit in Anlage 5 der <a href={{linkGGO}} target="_blank">GGO</a> behandelt.',
    selectDefaultText: 'Bitte wählen Sie eine Ihrer Optionen aus',
    noTeilbereicheText:
      'Es wurden keine Teilbereiche angelegt. Dies kann im Schritt „Ideen sammeln“ vorgenommen werden.',
    noOptionenText:
      'Für diesen Teilbereich wurden keine Optionen angelegt. Dies kann im Schritt „Ideen sammeln“ vorgenommen werden.',
    validationMessage:
      'Bitte geben Sie einen Titel für die Regelungsalternative {{index}} ein oder entfernen Sie diese Regelungsalternative.',
  },

  auswirkungennachregelungsalternativen: {
    title: 'Auswirkungen nach Regelungsalternativen',
    intro:
      'Sie haben im vorherigen Schritt die folgenden Bereiche identifiziert, in denen Sie wesentliche Auswirkungen erwarten:',
    subtitle1:
      'Für welche der von Ihnen entwickelten Alternativen sollen die wesentlichen Auswirkungen geprüft und beschrieben werden?',
    subtitle2: 'Für welche Bereiche erwarten Sie Auswirkungen bei dieser Regelungsalternative?',
    infoText:
      'Sie haben im vorherigen Schritt die folgenden Bereiche identifiziert, in denen Sie wesentliche Auswirkungen erwarten:',
    drawerHeadingTitle1: 'Auswirkungen für Regelungsalternative beschreiben.',
    drawerHeadingSubtitle: 'Ziel der Alternativenprüfung und -bewertung',
    drawerHeadingContent:
      `<p>Für die Alternativenprüfung gilt: Je gewissenhafter und erschöpfender eine jede mögliche Alternative geprüft wird, umso belastbarer ist die letztlich getroffene Auswahl. Zudem können durch eine intensive Prüfung etwaige Auseinandersetzungen mit Dritten vorab erkannt werden. Die Verteidigung der eigenen Argumentation wird erleichtert und die Akzeptanz bei Politikern, Betroffenen, Verbänden und weiteren Akteuren kann erhöht werden. Überall dort, wo gravierende Auswirkungen zu erwarten sind, sollten diese eingehend geprüft werden. Dabei lohnt auch ein Austausch mit den betroffenen Fachressorts. Bei der Durchführung der Konsultation unterstützt Sie auch Schritt 5 der Anwendung – hier wird entsprechend der ausgewählten Politikbereiche auch jeweils auf die betroffenen Fachressorts und die dortige Expertise verwiesen. Anschließend sollte den Fragen in einer Konsultation Dritter nachgegangen werden.</p>` +
      `<p>Um einen GGO-konformen Regelungsentwurf zu erstellen, müssen die in <a href="{{link}}" target="_blank" style="white-space: normal">§ 44 GGO</a> genannten Prüfkriterien berücksichtigt und im Entwurf dargestellt werden. Dazu gehören unter anderem der Erfüllungsaufwand, Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte und Auswirkungen auf eine nachhaltige Entwicklung. Auch weitere wesentliche Auswirkungen – d.h. beabsichtigte Wirkungen und unbeabsichtigte Nebenwirkungen – sollen im Regelungsentwurf dargestellt werden.</p>`,
    noOptionenText:
      'Für diesen Teilbereich wurden keine Politikbereiche angelegt. Dies kann im Schritt „Auswirkungen in Politikbereichen“ vorgenommen werden.',
    checkboxAriaLabel: 'Nach Auswahl sind weitere Eingabenmöglichkeiten verfügbar',
  },
  fazit: {
    title: 'Fazit',
    goalDefinitionInfoFazit:
      'Dokumentieren Sie hier zusammenfassend die Ergebnisse der Alternativenprüfung und -bewertung und des Konsultationsverfahrens. Zum Vergleich werden Ihnen Ihre eigene vergleichende Bewertung der Regelungsalternativen und Ihre Zusammenfassung der Konsultationsergebnisse angezeigt. Sie können Ihre Bewertung mit der Bewertung der Beteiligten vergleichen und ein Fazit ziehen.',
    subtitle1: 'Ihre Ergebnisse auf einen Blick',
    goalRegelungsvorhaben: 'Ziele der Regelungsvorhaben',
    subtitle2: 'Gesamtbewertung für die Regelungsalternativen',
    subtitle3: 'Zusammenfassung der Konsultation',
    subtitle4: 'Ihr Gesamtfazit',
    subtitle5: 'Fazit',
    subtitle6: 'Für welche der Regelungsalternativen möchten Sie sich aufgrund Ihrer Ergebnisse entscheiden?',
    hintGesamtFazit: 'Sie können zur Weiterverwendung auch mehrere Regelungsalternativen wählen.',

    infoSubitle2:
      'Sie können hier an ihrem Text Änderungen vornehmen. An allen anderen Stellen wird er automatisch angepasst.',

    drawerFazitTitleSmall: 'Fazit',
    drawerFazitContentSmall:
      'Zu welchem Ergebnis kommen Sie durch den Abgleich Ihrer Bewertung mit der Bewertung der Beteiligten? Welche Auswirkungen sind bei den verschiedenen Regelungsalternativen zu erwarten? Welche Gründe sprechen für oder gegen die verschiedenen Alternativen? Lässt sich aufgrund der Ergebnisse eine Entscheidung für eine der Regelungsalternativen treffen?',
    noRegelungsalternativenText:
      'Es wurden keine Regelungsalternativen angelegt. Dies kann im Schritt „Alternativen zusammenstellen“ vorgenommen werden.',
  },

  bewertung: {
    title: 'Prüfung und Bewertung von Regelungsalternativen',
    subtitle1: 'Bewertung',
    infoText1:
      'Wie bewerten Sie die ausgewählten Regelungsalternativen in Bezug auf die Erreichung der von Ihnen beschriebenen Ziele, ihre Umsetzbarkeit, Effizienz und ihre weiteren Auswirkungen? Entsprechend Ihrer Bewertung können Sie für jede Regelungsalternative einen Rang vergeben.',
    infoText2: 'Für welche der von Ihnen entwickelten Alternativen soll eine Bewertung vorgenommen werden?',
    infoText3:
      'Wie bewerten Sie die ausgewählten Regelungsalternativen im Vergleich? Welche Regelungsalternative halten Sie nach der oben vorgenommenen Bewertung für am besten geeignet? Begründen Sie Ihre Entscheidung kurz.',
    subsubTitle1: 'Auswahl der Regelungsalternativen',
    subsubTitle2: 'Bewertung der Regelungsalternativen',
    subsubTitle3: 'Zusammenfassung',
    subsubTitle4: 'Gesamtbewertung für die Regelungsalternativen',
    rowblockHeading1: 'Zielerreichung',
    rowblockHeading2: 'Weitere Prüfkriterien',
    rowblockHeading3: 'Auswirkungen in Politikbereichen',
    rowblockHeading4: 'Bewertung qualitativ',
    rowblockHeading5: 'Gesamtrang',
    tableInfoText1: 'Bitte beschreiben Sie Ihre Bewertung der jeweiligen Alternative.',
    tableInfoText2: 'Bitte vergeben Sie einen Rang für die jeweilige Regelungsalternative.',
    addRowText: 'Bewertungskriterium hinzufügen',
    selectDefaultValue: 'Bitte auswählen',
    noRegelungsalternativenText:
      'Es wurden keine Regelungsalternativen angelegt. Dies kann im Schritt „Alternativen zusammenstellen“ vorgenommen werden.',
    deleteCriteria: 'Prüfkriterium {{index}} entfernen',
    addCriteria: 'Prüfkriterium hinzufügen',
    goalTitle: 'Zielerreichung',
    criteriaTitle: 'Weitere Prüfkriterien',
    politikbereichTitle: 'Auswirkungen in Politikbereichen',
    ratingTitle: 'Bewertung qualitativ',
    rankTitle: 'Gesamtrang',
    rankLabel: 'Rang',
    bewertungLabel: 'Bewertung',
    pruefkriteriumLabel: 'Weiteres Prüfkriterium',
    validationMessage:
      'Bitte geben Sie einen Namen für das Prüfkriterium {{index}} ein oder entfernen Sie dieses Prüfkriterium.',
  },

  ergebnisse: {
    title: 'Konsultation',
    subtitle1: 'Ergebnisse',
    subtitle2: 'Zusammenfassung der Konsultationsergebnisse',
    subsubTitle1: 'Betroffene Ressorts',
    subsubTitle2: 'Weitere Stellen',
    subsubTitle3: 'Zusammenfassung',
    infoText1:
      'Nachdem Sie die Konsultation der betreffenden Stellen durchgeführt haben, können Sie hier die zusammengetragenen Ergebnisse dokumentieren.',
    infoText2:
      'Wie bewerten die betroffenen Ressorts und andere Beteiligte die verschiedenen Regelungsalternativen? Welche Auswirkungen sind ihrer Ansicht nach bei den verschiedenen Alternativen zu erwarten?',
    noStellenText:
      'Für diesen Bereich wurden keine Stellen ausgewählt. Dieses kann auf der vorherigen Seite „Vorbereitung und Durchführung“ geändert werden.',
  },

  konsultation: {
    title: 'Konsultation',
    infoText1:
      'Das Konsultationsverfahren ist ein Prozess, in welchem die entwickelten Regelungsalternativen mit externen Fachkundigen und Normzielgruppe (unter anderem Betroffene und Sachverständige der Ressorts) erörtert werden.',
    konsultationTitleDrawer: 'Konsultation',
    konsultationDrawerText1:
      'Zur Vermeidung unnötigen Aufwands und von Irritationen durch die Behandlung nicht mehr relevanter Varianten sollte sich die externe Konsultation nur noch auf diejenigen Regelungsalternativen erstrecken, die nach erfolgter Einbeziehung der Fachressorts weiterhin als erwägenswert anzusehen sind. Ziel ist es zunächst, die herausgearbeiteten Regelungsalternativen auf ihre Verständlichkeit, Eindeutigkeit und Vollständigkeit hin zu prüfen und gegebenenfalls zu modifizieren oder zu ergänzen. Das Konsultationsverfahren dient also der Prüfung der vorher entwickelten Regelungsalternativen im Hinblick auf fachliche Richtigkeit, interne Schlüssigkeit und Vollständigkeit.',
    konsultationDrawerText2:
      'Darüber hinaus sollen die Folgen der Regelungsalternativen mit potenziell Betroffenen (Normzielgruppe) und Expertinnen und Experten möglichst in einem offenen Dialog (zum Beispiel eine Gruppendiskussion) ermittelt, erörtert und im Hinblick auf die relevanten Prüfkriterien bewertet werden. Das Ergebnis der internen Bewertung kann als Grundlage dieses Dialoges dienen. Hierbei sollten vor allem die Prüfkriterien beachtet werden, die sich bei der internen Prüfung als besonders relevant erwiesen haben.',
    konsultationDrawerText3:
      'Empfohlen wird bei der Konsultation zunächst eine qualitative Vorgehensweise, insbesondere Gruppendiskussionen. Darüber hinaus können auch quantitative Verfahren (schriftliche Befragungen etc.) in Betracht kommen. Bei einer Gruppendiskussion sollte die Zahl der Beteiligten auf maximal 15 reduziert werden. Es können aber nach Bedarf auch mehrere Gruppen gebildet werden.',
    konsultationDrawerText4: 'Mithilfe eines Konsultationsverfahrens sollte erörtert werden: ',
    konsultationDrawerText5:
      '1. das Regelungserfordernis durch Einbeziehung der Null-Alternative, das heißt keine Neuregelung',
    konsultationDrawerText6:
      '2. die vergleichende Bewertung der Folgen der Regelungsalternativen im Hinblick auf die Prüfkriterien (Effektivität, Risiko etc.)',
    konsultationDrawerText7: '3. welche Regelungsalternative die Vorteilhafteste ist.',
    subsubTitle1: 'Vorbereitung und Durchführung',
    subsubTitle2: 'Auswahl der Regelungsalternativen',
    infoText2:
      'Welche der Regelungsalternativen möchten Sie vor dem Hintergrund Ihrer Bewertung weiterverfolgen und extern konsultieren?',
    subsubTitle3: 'Konsultation betroffener Ressorts und anderer Stellen',
    subsubTitle4: 'Betroffene Ressorts',
    infoText3:
      'In dem Schritt „Prüfung und Bewertung – Auswirkungen“ haben Sie Bereiche ausgewählt, die durch das Regelungsvorhaben betroffen sind. Ausgehend von Ihren Antworten bekommen Sie unten angezeigt, welche Ressorts Sie zu welchen Themenbereichen kontaktieren sollten.',
    subsubTitle5: 'Weitere betroffene Ressorts',
    infoText4: 'Wenn Sie weitere Ressorts konsultieren möchten, können Sie diese hier hinzufügen.',
    subsubTitle6: 'Weitere zu konsultierende Stellen',
    infoText5:
      'Anschließend sollte den Fragen in einer Konsultation Dritter nachgegangen werden. Hier können Sie eintragen, welche anderen Stellen Sie konsultieren möchten (Vertreterinnen und Vertreter der betroffenen Interessenvertretungen und Verbände, direkt betroffene Normadressatinnen und –adressaten, externe Expertinnen und Experten etc.). Die Anzahl der konsultierten beziehungsweise hinzugezogenen Beteiligten ist abhängig von der Art des Konsultationsverfahrens.',
    subsubTitle7: 'Durchführung des Konsultationsverfahrens im Detail',
    infoText6:
      'Für die Durchführung eines Konsultationsverfahrens gibt es verschiedene Möglichkeiten. Die folgende exemplarische Aufzählung soll dies verdeutlichen. Für ein Konsultationsverfahren kommen unter anderem in Betracht:',
    infoText7: 'Gruppendiskussionen (Workshops)',
    infoText8: 'Interviews mit Fachkundigen und Normadressatinnen und –adressaten',
    infoText9: 'schriftliche Befragungen',
    infoText10: 'e-Partizipationen (Online-Konsultationen)',
    infoText11:
      'Eine Kombination dieser Verfahren ist möglich. Empfehlenswert ist in jedem Fall die Durchführung einer Gruppendiskussion. Welche Form der Konsultation im Einzelfall geeignet ist, ist abhängig vom Regelungsfeld, Regelungsgegenstand und von verfügbaren Ressourcen. Nach allen Erfahrungen fördert der Einsatz dieser Instrumente neue fachliche Erkenntnisse zutage; darüber hinaus kann ein Konsultationsverfahren zur Akzeptanz im Regelungsfeld beitragen.',
    titleKosultStelle: 'Zu konsultierende Stelle',
    addStelleBtn: 'Stelle hinzufügen',
    deleteStelleBtn: 'Stelle {{index}} entfernen',
    titleKosultRessort: 'Zu konsultierendes Ressort',
    addRessortBtn: 'Ressort hinzufügen',
    deleteRessortBtn: 'Ressort {{index}} entfernen',
    noAutomaticRessorts: 'Aufgrund der bisherigen Eingaben konnten leider keine Ressorts ermittelt werden.',
    validationMessage: {
      ressort:
        'Bitte wählen Sie einen Eintrag für das zu konsultierende Ressort {{index}} aus oder entfernen Sie dieses Ressort.',
      stelle:
        'Bitte geben Sie einen Namen für die zu konsultierende Stelle {{index}} ein oder entfernen Sie diese Stelle.',
    },
  },
  ergebnisdokumentation: {
    title: 'Ergebnisdokumentation Vorschau',
    loadingText: 'Vorschau wird erstellt...',
    zurStartseite: 'Zur Startseite Vorbereitung',
    zielanalyse: {
      ziel: 'Ziel',
    },
    pruefungBewertung: {
      auswirkungen: {
        title: 'Auswirkungen',
        text: 'Es sind wesentliche Auswirkungen in folgenden Bereichen zu erwarten:',
      },
      zielerreichung: {
        title: 'Zielerreichung',
        mark: 'Ziel',
      },
      pruefkriterien: {
        title: 'Weitere Prüfkriterien',
        mark: 'Kriterium',
      },
      politikbereich: {
        title: 'Auswirkungen in Politikbereichen',
        mark: 'Politikbereich',
      },
      gesamtrang: 'Gesamtrang',
      gesamtrangKeine: 'Keine Auswahl',
      gesamtrangRangLabel: 'Rang der jeweiligen Regelungsalternative',
      gesamtbewertung: 'Gesamtbewertung',
      zeilValues: {
        keineAngabe: 'keine Angabe',
        neutral: 'Neutral',
        negativ: 'Negativ',
        positiv: 'Positiv',
      },
    },
    konsultation: {
      ressort: {
        title: 'Zu konsultierendes Ressort',
        text: 'Folgende Ressorts wurden zur weiteren Konsultation der Regelungsalternativen hinzugefügt:',
      },
      stellen: {
        title: 'Weitere zu konsultierende Stellen',
        text: 'Folgende Stellen wurden zur weiteren Konsultation der Regelungsalternativen hinzugefügt:',
      },
      ergebnisseDerKonsultation: 'Ergebnisse der Konsultation',
      betroffeneRessorts: 'Betroffene Ressorts',
      weitereStellen: 'Weitere Stellen',
      zusammenfassung: 'Zusammenfassung der Konsultationen',
    },
    fazit: {
      eigeneBewertung: 'Eigene Bewertung',
      konsultation: 'Konsultation',
      gesamtfazit: 'Gesamtfazit',
      entscheidung: 'Entscheidung für Regelungsalternative',
    },
  },
};
