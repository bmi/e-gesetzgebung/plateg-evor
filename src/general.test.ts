// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import sinon from 'sinon';

import { LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { messages } from './messages';
// Diese Testklasse enthält keine Testfälle, stattdessen werden hier alle Mocks initialisiert,
// die von den anderen Testklassen benötigt werden
const loadingStatusCtrl = GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
export const setLoadingStatusStub = sinon.stub(loadingStatusCtrl, 'setLoadingStatus');

const unusedPromise = i18n.use(initReactI18next).init({
  resources: messages,
  lng: 'de',
  interpolation: {
    escapeValue: false,
  },
});
