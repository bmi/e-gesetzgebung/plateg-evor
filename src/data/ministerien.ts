// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { TFunction } from 'i18next';
export interface MinisterienMap {
  [key: string]: { abbreviation: string; name: string };
}

export function getMinisterien(t: TFunction): MinisterienMap {
  return {
    finanzmarktpolitik: { abbreviation: 'BMF', name: t(`bundesministerium.BMF`) },
    steuern: { abbreviation: 'BMF', name: t(`bundesministerium.BMF`) },
    versicherungenundfinanzen: { abbreviation: 'BMF', name: t(`bundesministerium.BMF`) },
    zolleenergieverbrauchsteuern: { abbreviation: 'BMF', name: t(`bundesministerium.BMF`) },
    finanzengrundsatz: { abbreviation: 'BMF', name: t(`bundesministerium.BMF`) },
    baustadtwohnen: { abbreviation: 'BMWSB', name: t(`bundesministerium.BMWSB`) },
    inneres: { abbreviation: 'BMI', name: t(`bundesministerium.BMI`) },
    itundnetzpolitik: { abbreviation: 'BMI', name: t(`bundesministerium.BMI`) },
    migrationundintegration: { abbreviation: 'BMI', name: t(`bundesministerium.BMI`) },
    sicherheit: { abbreviation: 'BMI', name: t(`bundesministerium.BMI`) },
    statistik: { abbreviation: 'BMI', name: t(`bundesministerium.BMI`) },
    auswartigerdienst: { abbreviation: 'AA', name: t(`bundesministerium.AA`) },
    wirtschaftlichezusammenarbeitundentwicklung: { abbreviation: 'BMZ', name: t(`bundesministerium.BMZ`) },
    strahlenschutzatomenergie: { abbreviation: 'BMUV', name: t(`bundesministerium.BMUV`) },
    abfallewasserboden: { abbreviation: 'BMUV', name: t(`bundesministerium.BMUV`) },
    chemikalien: { abbreviation: 'BMUV', name: t(`bundesministerium.BMUV`) },
    naturundartenschutz: { abbreviation: 'BMUV', name: t(`bundesministerium.BMUV`) },
    tierschutztiergesundheit: { abbreviation: 'BMUV', name: t(`bundesministerium.BMUV`) },
    umweltgrundsatz: { abbreviation: 'BMUV', name: t(`bundesministerium.BMUV`) },
    klima: { abbreviation: 'BMWK', name: t(`bundesministerium.BMWK`) },
    energiegrundsatz: { abbreviation: 'BMWK', name: t(`bundesministerium.BMWK`) },
    technologie: { abbreviation: 'BMWK', name: t(`bundesministerium.BMWK`) },
    wirtschaft: { abbreviation: 'BMWK', name: t(`bundesministerium.BMWK`) },
    justiz: { abbreviation: 'BMJ', name: t(`bundesministerium.BMJ`) },
    verbraucherschutz: { abbreviation: 'BMUV', name: t(`bundesministerium.BMUV`) },
    arbeitsmarkt: { abbreviation: 'BMAS', name: t(`bundesministerium.BMAS`) },
    belangebehindertermenschen: { abbreviation: 'BMAS', name: t(`bundesministerium.BMAS`) },
    rehabilitation: { abbreviation: 'BMAS', name: t(`bundesministerium.BMAS`) },
    sozialhilfe: { abbreviation: 'BMAS', name: t(`bundesministerium.BMAS`) },
    sozialversicherung: { abbreviation: 'BMAS', name: t(`bundesministerium.BMAS`) },
    arbeitundsozialesgrundsatz: { abbreviation: 'BMAS', name: t(`bundesministerium.BMAS`) },
    verteidigung: { abbreviation: 'BMVg', name: t(`bundesministerium.BMVg`) },
    ernahrungundlebensmittelsicherheit: { abbreviation: 'BMEL', name: t(`bundesministerium.BMEL`) },
    landundforstwirtschaftfischerei: { abbreviation: 'BMEL', name: t(`bundesministerium.BMEL`) },
    landlicheregionraumeagrarmarkte: { abbreviation: 'BMEL', name: t(`bundesministerium.BMEL`) },
    ernahrungundlandwirtschaftgrundsatz: { abbreviation: 'BMEL', name: t(`bundesministerium.BMEL`) },
    familieseniorenfrauenundjugend: { abbreviation: 'BMFSFJ', name: t(`bundesministerium.BMFSFJ`) },
    gesundheit: { abbreviation: 'BMG', name: t(`bundesministerium.BMG`) },
    digitales: { abbreviation: 'BMDV', name: t(`bundesministerium.BMDV`) },
    digitalegesellschaft: { abbreviation: 'BMI', name: t(`bundesministerium.BMI`) },
    eisenbahnen: { abbreviation: 'BMDV', name: t(`bundesministerium.BMDV`) },
    luftfahrt: { abbreviation: 'BMDV', name: t(`bundesministerium.BMDV`) },
    strassenbau: { abbreviation: 'BMDV', name: t(`bundesministerium.BMDV`) },
    strassenverkehr: { abbreviation: 'BMDV', name: t(`bundesministerium.BMDV`) },
    wasserstrassenschifffahrt: { abbreviation: 'BMDV', name: t(`bundesministerium.BMDV`) },
    verkehrgrundsatz: { abbreviation: 'BMDV', name: t(`bundesministerium.BMDV`) },
    berufsausbildung: { abbreviation: 'BMBF', name: t(`bundesministerium.BMBF`) },
    forschung: { abbreviation: 'BMBF', name: t(`bundesministerium.BMBF`) },
    fortbildung: { abbreviation: 'BMBF', name: t(`bundesministerium.BMBF`) },
    bildungundforschunggrundsatz: { abbreviation: 'BMBF', name: t(`bundesministerium.BMBF`) },
    medienundkultur: { abbreviation: 'BKM', name: t(`bundesministerium.BKM`) },
  };
}
