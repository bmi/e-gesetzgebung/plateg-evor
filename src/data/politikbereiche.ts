// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useTranslation } from 'react-i18next';

import { Politikbereich } from '@plateg/rest-api';
export function getPolitikbereichen(): Politikbereich[] {
  const { t } = useTranslation();
  return [
    {
      id: 'finance',
      name: t(`politikbereichen.finance.categoryTitle`),
      alias: 'finance',
      unterbereiche: [
        {
          id: 'finanzmarktpolitik',
          name: t(`politikbereichen.finance.finanzmarktpolitik.label`),
          alias: 'finanzmarktpolitik',
        },
        {
          id: 'steuern',
          name: t(`politikbereichen.finance.steuern.label`),
          alias: 'steuern',
        },
        {
          id: 'versicherungenundfinanzen',
          name: t(`politikbereichen.finance.versicherungenundfinanzen.label`),
          alias: 'versicherungenundfinanzen',
        },
        {
          id: 'zolleenergieverbrauchsteuern',
          name: t(`politikbereichen.finance.zolleenergieverbrauchsteuern.label`),
          alias: 'zolleenergieverbrauchsteuern',
        },
        {
          id: 'finanzengrundsatz',
          name: t(`politikbereichen.finance.finanzengrundsatz.label`),
          alias: 'finanzengrundsatz',
        },
      ],
    },
    {
      id: 'innereskategorie',
      name: t(`politikbereichen.innereskategorie.categoryTitle`),
      alias: 'innereskategorie',
      unterbereiche: [
        {
          id: 'inneres',
          name: t(`politikbereichen.innereskategorie.inneres.label`),
          alias: 'inneres',
        },
        {
          id: 'migrationundintegration',
          name: t(`politikbereichen.innereskategorie.migrationundintegration.label`),
          alias: 'migrationundintegration',
        },
        {
          id: 'statistik',
          name: t(`politikbereichen.innereskategorie.statistik.label`),
          alias: 'statistik',
        },
        {
          id: 'sicherheit',
          name: t(`politikbereichen.innereskategorie.sicherheit.label`),
          alias: 'sicherheit',
        },
      ],
    },
    {
      id: 'internationales',
      name: t(`politikbereichen.internationales.categoryTitle`),
      alias: 'internationales',
      unterbereiche: [
        {
          id: 'auswartigerdienst',
          name: t(`politikbereichen.internationales.auswartigerdienst.label`),
          alias: 'auswartigerdienst',
        },
        {
          id: 'wirtschaftlichezusammenarbeitundentwicklung',
          name: t(`politikbereichen.internationales.wirtschaftlichezusammenarbeitundentwicklung.label`),
          alias: 'wirtschaftlichezusammenarbeitundentwicklung',
        },
      ],
    },
    {
      id: 'verteidigungkategorie',
      name: t(`politikbereichen.verteidigungkategorie.categoryTitle`),
      alias: 'verteidigungkategorie',
      unterbereiche: [
        {
          id: 'verteidigung',
          name: t(`politikbereichen.verteidigungkategorie.verteidigung.label`),
          alias: 'verteidigung',
        },
      ],
    },
    {
      id: 'wirtschaftundverbraucherschutz',
      name: t(`politikbereichen.wirtschaftundverbraucherschutz.categoryTitle`),
      alias: 'wirtschaftundverbraucherschutz',
      unterbereiche: [
        {
          id: 'wirtschaft',
          name: t(`politikbereichen.wirtschaftundverbraucherschutz.wirtschaft.label`),
          alias: 'wirtschaft',
        },
        {
          id: 'verbraucherschutz',
          name: t(`politikbereichen.wirtschaftundverbraucherschutz.verbraucherschutz.label`),
          alias: 'verbraucherschutz',
        },
      ],
    },

    {
      id: 'energie',
      name: t(`politikbereichen.energie.categoryTitle`),
      alias: 'energie',
      unterbereiche: [
        {
          id: 'strahlenschutzatomenergie',
          name: t(`politikbereichen.energie.strahlenschutzatomenergie.label`),
          alias: 'strahlenschutzatomenergie',
        },
        {
          id: 'energiegrundsatz',
          name: t(`politikbereichen.energie.energiegrundsatz.label`),
          alias: 'energiegrundsatz',
        },
      ],
    },
    {
      id: 'klimakategorie',
      name: t(`politikbereichen.klimakategorie.categoryTitle`),
      alias: 'klimakategorie',
      unterbereiche: [
        {
          id: 'klima',
          name: t(`politikbereichen.klimakategorie.klima.label`),
          alias: 'klima',
        },
      ],
    },

    {
      id: 'digitalisierungundtechnologie',
      name: t(`politikbereichen.digitalisierungundtechnologie.categoryTitle`),
      alias: 'digitalisierungundtechnologie',
      unterbereiche: [
        {
          id: 'digitales',
          name: t(`politikbereichen.digitalisierungundtechnologie.digitales.label`),
          alias: 'digitales',
        },
        {
          id: 'technologie',
          name: t(`politikbereichen.digitalisierungundtechnologie.technologie.label`),
          alias: 'technologie',
        },
        {
          id: 'digitalegesellschaft',
          name: t(`politikbereichen.digitalisierungundtechnologie.digitalegesellschaft.label`),
          alias: 'digitalegesellschaft',
        },
      ],
    },

    {
      id: 'justizkategorie',
      name: t(`politikbereichen.justizkategorie.categoryTitle`),
      alias: 'justizkategorie',
      unterbereiche: [
        {
          id: 'justiz',
          name: t(`politikbereichen.justizkategorie.justiz.label`),
          alias: 'justiz',
        },
      ],
    },

    {
      id: 'arbeitundsoziales',
      name: t(`politikbereichen.arbeitundsoziales.categoryTitle`),
      alias: 'arbeitundsoziales',
      unterbereiche: [
        {
          id: 'arbeitsmarkt',
          name: t(`politikbereichen.arbeitundsoziales.arbeitsmarkt.label`),
          alias: 'arbeitsmarkt',
        },
        {
          id: 'belangebehindertermenschen',
          name: t(`politikbereichen.arbeitundsoziales.belangebehindertermenschen.label`),
          alias: 'belangebehindertermenschen',
        },
        {
          id: 'rehabilitation',
          name: t(`politikbereichen.arbeitundsoziales.rehabilitation.label`),
          alias: 'rehabilitation',
        },
        {
          id: 'sozialhilfe',
          name: t(`politikbereichen.arbeitundsoziales.sozialhilfe.label`),
          alias: 'sozialhilfe',
        },
        {
          id: 'sozialversicherung',
          name: t(`politikbereichen.arbeitundsoziales.sozialversicherung.label`),
          alias: 'sozialversicherung',
        },
        {
          id: 'arbeitundsozialesgrundsatz',
          name: t(`politikbereichen.arbeitundsoziales.arbeitundsozialesgrundsatz.label`),
          alias: 'arbeitundsozialesgrundsatz',
        },
      ],
    },
    {
      id: 'ernahrungundlandwirtschaft',
      name: t(`politikbereichen.ernahrungundlandwirtschaft.categoryTitle`),
      alias: 'ernahrungundlandwirtschaft',
      unterbereiche: [
        {
          id: 'ernahrungundlebensmittelsicherheit',
          name: t(`politikbereichen.ernahrungundlandwirtschaft.ernahrungundlebensmittelsicherheit.label`),
          alias: 'ernahrungundlebensmittelsicherheit',
        },
        {
          id: 'landundforstwirtschaftfischerei',
          name: t(`politikbereichen.ernahrungundlandwirtschaft.landundforstwirtschaftfischerei.label`),
          alias: 'landundforstwirtschaftfischerei',
        },
        {
          id: 'landlicheregionraumeagrarmarkte',
          name: t(`politikbereichen.ernahrungundlandwirtschaft.landlicheregionraumeagrarmarkte.label`),
          alias: 'landlicheregionraumeagrarmarkte',
        },
        {
          id: 'ernahrungundlandwirtschaftgrundsatz',
          name: t(`politikbereichen.ernahrungundlandwirtschaft.ernahrungundlandwirtschaftgrundsatz.label`),
          alias: 'ernahrungundlandwirtschaftgrundsatz',
        },
      ],
    },
    {
      id: 'familieseniorenfrauenundjugendkategorie',
      name: t(`politikbereichen.familieseniorenfrauenundjugendkategorie.categoryTitle`),
      alias: 'familieseniorenfrauenundjugendkategorie',
      unterbereiche: [
        {
          id: 'familieseniorenfrauenundjugend',
          name: t(`politikbereichen.familieseniorenfrauenundjugendkategorie.familieseniorenfrauenundjugend.label`),
          alias: 'familieseniorenfrauenundjugend',
        },
      ],
    },
    {
      id: 'gesundheitkategorie',
      name: t(`politikbereichen.gesundheitkategorie.categoryTitle`),
      alias: 'gesundheitkategorie',
      unterbereiche: [
        {
          id: 'gesundheit',
          name: t(`politikbereichen.gesundheitkategorie.gesundheit.label`),
          alias: 'gesundheit',
        },
      ],
    },
    {
      id: 'verkehr',
      name: t(`politikbereichen.verkehr.categoryTitle`),
      alias: 'verkehr',
      unterbereiche: [
        {
          id: 'eisenbahnen',
          name: t(`politikbereichen.verkehr.eisenbahnen.label`),
          alias: 'eisenbahnen',
        },
        {
          id: 'luftfahrt',
          name: t(`politikbereichen.verkehr.luftfahrt.label`),
          alias: 'luftfahrt',
        },
        {
          id: 'strassenbau',
          name: t(`politikbereichen.verkehr.strassenbau.label`),
          alias: 'strassenbau',
        },
        {
          id: 'strassenverkehr',
          name: t(`politikbereichen.verkehr.strassenverkehr.label`),
          alias: 'strassenverkehr',
        },
        {
          id: 'wasserstrassenschifffahrt',
          name: t(`politikbereichen.verkehr.wasserstrassenschifffahrt.label`),
          alias: 'wasserstrassenschifffahrt',
        },
        {
          id: 'verkehrgrundsatz',
          name: t(`politikbereichen.verkehr.verkehrgrundsatz.label`),
          alias: 'verkehrgrundsatz',
        },
        {
          id: 'baustadtwohnen',
          name: t(`politikbereichen.verkehr.baustadtwohnen.label`),
          alias: 'baustadtwohnen',
        },
      ],
    },
    {
      id: 'umweltundnaturschutz',
      name: t(`politikbereichen.umweltundnaturschutz.categoryTitle`),
      alias: 'umweltundnaturschutz',
      unterbereiche: [
        {
          id: 'abfallewasserboden',
          name: t(`politikbereichen.umweltundnaturschutz.abfallewasserboden.label`),
          alias: 'abfallewasserboden',
        },
        {
          id: 'chemikalien',
          name: t(`politikbereichen.umweltundnaturschutz.chemikalien.label`),
          alias: 'chemikalien',
        },
        {
          id: 'naturundartenschutz',
          name: t(`politikbereichen.umweltundnaturschutz.naturundartenschutz.label`),
          alias: 'naturundartenschutz',
        },
        {
          id: 'tierschutztiergesundheit',
          name: t(`politikbereichen.umweltundnaturschutz.tierschutztiergesundheit.label`),
          alias: 'tierschutztiergesundheit',
        },
        {
          id: 'umweltgrundsatz',
          name: t(`politikbereichen.umweltundnaturschutz.umweltgrundsatz.label`),
          alias: 'umweltgrundsatz',
        },
      ],
    },
    {
      id: 'bildungundforschung',
      name: t(`politikbereichen.bildungundforschung.categoryTitle`),
      alias: 'bildungundforschung',
      unterbereiche: [
        {
          id: 'berufsausbildung',
          name: t(`politikbereichen.bildungundforschung.berufsausbildung.label`),
          alias: 'berufsausbildung',
        },
        {
          id: 'forschung',
          name: t(`politikbereichen.bildungundforschung.forschung.label`),
          alias: 'forschung',
        },
        {
          id: 'fortbildung',
          name: t(`politikbereichen.bildungundforschung.fortbildung.label`),
          alias: 'fortbildung',
        },
        {
          id: 'bildungundforschunggrundsatz',
          name: t(`politikbereichen.bildungundforschung.bildungundforschunggrundsatz.label`),
          alias: 'bildungundforschunggrundsatz',
        },
      ],
    },
    {
      id: 'medienundkulturkategorie',
      name: t(`politikbereichen.medienundkulturkategorie.categoryTitle`),
      alias: 'medienundkulturkategorie',
      unterbereiche: [
        {
          id: 'medienundkultur',
          name: t(`politikbereichen.medienundkulturkategorie.medienundkultur.label`),
          alias: 'medienundkultur',
        },
      ],
    },
  ];
}
